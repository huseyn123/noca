<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Config;
use App\Models\MainProduct;
use App\Models\Page;


class DatabaseSeeder extends Seeder
{

    public function run()
    {
        Model::unguard();

        $this->call('UserSeeder');
        $this->call('ConfigSeeder');
        $this->call('PageSeeder');
        $this->call('SettingsSeeder');


        $path = 'app/Sql/dictionaries.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Dictionary table seeded!');

        Model::reguard();
    }

}

class UserSeeder extends Seeder
{
    public function run()
    {
        User::create([
            "first_name" => "Rəşad",
            "last_name" => "Ağayev",
            "email" => "rashad@apamark.az",
            "password" => bcrypt("password"),
            "role" => 1,
        ]);

        User::create([
            "first_name" => "Huseyn",
            "last_name" => "Huseynli",
            "email" => "huseyn.h@code.edu.az",
            "password" => bcrypt("password"),
            "role" => 1,
        ]);
    }

}

class ConfigSeeder extends Seeder
{
    public function run()
    {
        Config::create([
            "company_name" => "NOCA",
            "contact_phone" => "+(444)12 222 22 22",
            "email" => "rashad@apamark.az",
            "site_url" => "apamark.az",
            "location" => "40.4093101, 49.8945632",
            "google_api_key" => "",
            "lang_id" => "az",

        ]);
    }

}

class PageSeeder extends Seeder
{
    public function run()
    {
        Page::create([
            "template_id" => 0,
            "lang_id" => "az",
            "relation_page" => 1,
            "name" => "Ana səhifə",
            "slug" => "index",
            "visible" => 0,
            "target" => 1
        ]);

    }

}

class SettingsSeeder extends Seeder
{
    public function run()
    {
        DB::table('settings')->insert(['key' => 'host']);
        DB::table('settings')->insert(['key' => 'port']);
        DB::table('settings')->insert(['key' => 'encryption']);
        DB::table('settings')->insert(['key' => 'username']);
        DB::table('settings')->insert(['key' => 'password']);
    }

}
