<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('relation_page')->nullable();
            $table->string('title', 100);
            $table->string('slug', 120)->unique();
            $table->string('summary', 1500)->nullable();
            $table->text('content')->nullable();
            $table->boolean('status')->index()->unsigned();
            $table->boolean('featured')->default(false);
            $table->string('published_by', 40);
            $table->integer('seen_count')->unsigned();
            $table->date('published_at')->index()->nullable();
            $table->string('youtube_id', 20)->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->foreign('category_id')->references('id')->on('pages')->onDelete('set null');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
