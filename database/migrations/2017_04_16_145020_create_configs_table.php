<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration
{

    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name', 50);
            $table->string('contact_phone', 30)->nullable();
            $table->string('contact_phone2', 30)->nullable();
            $table->string('location', 500)->nullable();
            $table->string('email', 100);
            $table->string('contact_form_email', 100)->nullable();
            $table->string('hr_form_email', 100)->nullable();
            $table->string('customer_form_email', 100)->nullable();
            $table->string('site_url', 100);
            $table->string('kiv', 255)->nullable();
            $table->timestamp('sitemap')->nullable();
            $table->string('google_api_key', 255)->nullable();
            $table->string('analytic_id', 20)->nullable();
            $table->string('main_video', 255)->nullable();
            $table->unsignedInteger('experience_count')->default(0);
            $table->string('main_video_cover', 255)->nullable();
            $table->unsignedTinyInteger('blog_page')->default(0);
            $table->string('lang_id', 2)->default('az');
            $table->string('brandbook', 255)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
