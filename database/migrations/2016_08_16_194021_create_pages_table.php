<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->boolean('template_id')->unsigned()->default(1);
            $table->string('lang_id', 2);
            $table->unsignedInteger('order')->default(1);
            $table->unsignedInteger('relation_page')->nullable();
            $table->string('name', 255);
            $table->string('slug', 255);
            $table->string('description', 255)->nullable();
            $table->string('image_file', 255)->nullable();
            $table->boolean('visible')->default(0);
            $table->string('summary', 1500)->nullable();
            $table->text('content')->nullable();
            $table->string('meta_description', 255)->nullable();
            $table->string('meta_keywords', 255)->nullable();
            $table->boolean('target')->default(1);
            $table->string('forward_url', 255)->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->unique(['slug', 'lang_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
