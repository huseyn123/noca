const { mix } = require('laravel-mix');


mix.js([
    'resources/assets/js/app.js',
    'node_modules/select2/dist/js/select2.min.js',
    'resources/app/vendor/datetimepicker/build/js/bootstrap-datetimepicker.min.js', //npm-de yoxdu
    'node_modules/datatables.net/js/jquery.dataTables.js',
    'node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
    'node_modules/datatables.net-responsive-bs/js/responsive.bootstrap.min.js',
    'node_modules/datatables.net-buttons/js/dataTables.buttons.min.js',
    'node_modules/jquery-colorbox/jquery.colorbox-min.js',
    'resources/app/js/sb-admin-2.js'
], 'public/js/app.js')
    .styles([
        'node_modules/jquery-ui/themes/base/core.css',
        'node_modules/jquery-ui/themes/base/theme.css',
        'resources/app/vendor/datetimepicker/build/css/bootstrap-datetimepicker.min.css', //npm-de yoxdu
        'node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
        'node_modules/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
        'node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
        'node_modules/font-awesome/css/font-awesome.min.css',
        'node_modules/select2/dist/css/select2.min.css',
        'node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css',
        'node_modules/jquery-colorbox/example1/colorbox.css',
        'node_modules/dropzone/dist/min/dropzone.min.css',
        'resources/app/css/sb-admin-2.css'
    ], 'resources/app/css/modules.css')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .combine([
        'public/css/app.*.css',
        'resources/app/css/modules.*.css'
    ], 'public/css/app.css')
    .copy('node_modules/dropzone/dist/dropzone.js', 'public/js/dropzone.js')
    .copy('node_modules/jquery-ui/themes/base/images/', 'public/css/images')
    .copy('node_modules/jquery-colorbox/example1/images/', 'public/css/images')
    .copy('node_modules/font-awesome/fonts/', 'public/fonts')

    //************WEB*************//

    .styles([
        'resources/css/fonts.css',
        'resources/css/bootstrap.min.css',
        'resources/css/libs.css',
        'resources/css/style.css',
        'resources/css/responsive.css'
    ], 'public/css/style.css')

    .scripts([
        'resources/js/jquery.min.js',
        'resources/js/jquery-migrate-1.4.1.min.js',
        'resources/js/jquery-ui.js',
        'resources/js/jquery.equalheights.min.js',
        'resources/js/jquery.magnific-popup.min.js',
        'resources/js/bootstrap.min.js',
        'resources/js/owl.carousel.js',
        'resources/js/jquery.counterup.min.js',
        'resources/js/waypoints.min.js',
        'resources/js/validate.min.js',
        'resources/js/site.js'
    ], 'public/js/script.js')
    .version();
