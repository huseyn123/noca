<?php

namespace App\Crud;

class UserCrud extends RenderCrud
{

    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => 'Ad',
                "db" => "first_name",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Soyad',
                "db" => "last_name",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Email',
                "db" => "email",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Şifrə',
                "db" => 'password',
                "type" => 'password',
                "attr" => ['class'=>'form-control'],
                'show' => false,
                'edit' => false,
                'profile' => false
            ],
            [
                "label" => 'Vəzifə',
                "db" => 'role',
                "type" => 'select',
                "data" => [1 => "Admin", "Editor"],
                "selected" => null,
                'profile' => false,
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => 'Cədvəl',
                "db" => "dt_view",
                "type" => 'select',
                "data" => array_merge([0 => "Bütün dillər"], config('app.locales')),
                "selected" => null,
                "attr" => ['class'=>'form-control'],
                'create' => false,
            ]

        ];

        return $this->render($fields, $action, $data);
    }

}


