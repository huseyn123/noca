<?php

namespace App\Crud;

class PageCrud extends RenderCrud
{

    public function fields($lang, $action, $data = null)
    {
        $fields = [
            [
                "label" => "Modul",
                "db" => "template_id",
                "type" => "select",
                "data" => config('config.template'),
                "selected" => 0,
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Dil",
                "db" => "lang_id",
                "type" => "select",
                "data" => config('app.locales'),
                "selected" => array_first(config('app.locales')),
                "attr" => ['class'=>'form-control'],
                'edit' => false
            ],
            [
                "label" => 'Ad',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Slug (URL)",
                "db" => "slug",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'title'=>"Boş saxladığınız təqdirdə sluq avtomatik yaradılacaq."],
            ],
            [
                "label" => "Cover şəkil",
                "db" => "file",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    $group_btn =
                    '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div>';
                },
                'hide' => [1,3,4],
                'edit' => false,

            ],
            [
                "label" => "Qısa Məzmun",
                "db" => "summary",
                "type" => "textarea",
                "hide" => [1,3,4],
                "attr" => ['class'=>'form-control ', 'rows' => 5]
            ],
            [
                "label" => "Məzmun",
                "db" => "content",
                "type" => "textarea",
                "hide" => [1,3,4],
                "attr" => ['class'=>'form-control ckeditor', 'id' => 'editor-'.$lang, 'rows' => 5]
            ],
            [
                "label" => "Görünüş",
                "db" => "visible",
                "type" => "select",
                "data" => config('config.menu-visibility'),
                "selected" => 1,
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Linkin quruluşu",
                "db" => "target",
                "type" => "select",
                "data" => config('config.menu-target'),
                "selected" => 1,
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Link",
                "db" => "forward_url",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'title'=>"Səhifəni başqa ünvana yönləndirmək üçün nəzərdə tutulub.", 'placeholder' => 'Məs: http://google.az'],
            ],
            [
                "label" => "Meta description",
                "db" => "meta_description",
                "type" => "textarea",
                "attr" => ['class'=>'form-control', 'maxlength' => 160, 'rows' => 3, 'title' => 'Maksimum simvol sayı 160 olmalıdır.Sosial şəbəkələrdə paylaşılan zaman məhz bu yazı linkin altında göstəriləcək']
            ],
            [
                "label" => "Meta keywords",
                "db" => "meta_keywords[]",
                "type" => "select",
                "data" => [],
                "selected" => null,
                "attr" => ['class'=>'form-control meta_keywords', 'id'  => 'meta_keywords_'.$lang, 'multiple' => 'multiple', 'autocomplete' => 'off', 'title' => 'Keyword-ləri bir birindən ayırmaq üçün Enter düyməsini sıxın.'],
             ],
        ];
        return $this->render($fields, $action, $data);
    }


}


