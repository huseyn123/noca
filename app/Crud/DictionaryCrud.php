<?php

namespace App\Crud;


class DictionaryCrud extends RenderCrud
{

    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Açar söz",
                "db" => "keyword",
                "type" => "text",
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Editor",
                "db" => "editor",
                "type" => "checkbox",
                "value" => 1,
                "checked" => false,
                "attr" => []
            ],
        ];
        return $this->render($fields, $action, $data);
    }


}


