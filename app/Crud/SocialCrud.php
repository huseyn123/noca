<?php

namespace App\Crud;

class SocialCrud extends RenderCrud
{

    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => 'Sosial şəbəkə',
                "db" => "type",
                "type" => 'select',
                "data" => config('config.social-network'),
                "selected" => "fb",
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "URL",
                "db" => "forward_url",
                "type" => "text",
                "attr" => ['class'=>'form-control'],
                "design" => function($input, $data){
                    $group_btn =
                        '<label class="input-group-addon">
                        http://
                    </label>';

                    return '<div class="input-group">'.$group_btn.$input.'</div>';
                }
            ],

        ];
        return $this->render($fields, $action, $data);
    }

}
