<?php

namespace App\Crud;

class MainVideoCrud extends RenderCrud
{

    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Youtube ID",
                "db" => "main_video",
                "type" => "text",
                "attr" => ['class'=>'form-control'],
            ],

            [
                "label" => "Cover Şəkil",
                "db" => "main_video_cover",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data->main_video_cover != false){
                        $title = "Yenilə";
                        $img = '<div class="input-group"><img src="'.asset("storage/thumb/$data->main_video_cover").'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
        ];
        return $this->render($fields, $action, $data);
    }

}
