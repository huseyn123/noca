<?php

namespace App\Crud;

use  App\Models\Demonstrative;
use App\Models\Page;

class DemonstrativeCrud extends RenderCrud
{

    private function category()
    {
        $query = Page::whereNotIn('slug', ['index', 'search'])
            ->where('template_id', [11])
            ->where('lang_id', 'az')
            ->orderBy("id", "asc")
            ->pluck("name", "id");



        return $query;
    }



    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => "Kateqoriya",
                "db" => "category_id",
                "type" => "select",
                "data" => $this->category(),
                "selected" => 1,
                "attr" => ['class'=>'form-control','id' =>'template_id' ]
            ],
            [
                "label" => "Şəkil",
                "db" => "image",
                "type" => "file",
                "show" => true,
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false){
                        $title = "Yenilə";
                        $img = '<div class="input-group"><img src="'.asset("storage/$data->image").'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
            [
                "label" => "Ikon",
                "db" => "icon",
                "type" => "file",
                "show" => true,
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false){
                        $title = "Yenilə";
                        $img2 = '<figure style="background:#ccc; padding:10px"><img src="'.asset("storage/icon/$data->icon").'" style="max-width:200px"></figure>';
                    }
                    else{
                        $img2 = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img2;
                }
            ],
        ];
        return $this->render($fields, $action, $data);
    }


}


