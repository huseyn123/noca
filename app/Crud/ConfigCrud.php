<?php

namespace App\Crud;

class ConfigCrud extends RenderCrud
{

    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => 'Şirkət adı',
                "db" => "company_name",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Əlaqə telefonu',
                "db" => "contact_phone",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Email',
                "db" => "email",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Email2',
                "db" => "hr_form_email",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Əlaqə forması üçün e-poçt',
                "db" => "contact_form_email",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Xəritə',
                "db" => "location",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Saytın ünvanı',
                "db" => "site_url",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Google api key',
                "db" => "google_api_key",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
               "label" => 'Google Analytic ID',
               "db" => "analytic_id",
               "type" => 'text',
               "attr" => ['class'=>'form-control', 'placeholder' => 'UA-100000000-1']
            ],
            [
                "label" => 'Dil',
                "db" => "lang_id",
                "type" => 'select',
                "data" => config('app.locales'),
                "relation_is" => "array",
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
        ];
        return $this->render($fields, $action, $data);
    }

}
