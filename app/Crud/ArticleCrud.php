<?php

namespace App\Crud;

use App\Models\Page;
use App\Logic\MultiLanguageSelect;

class ArticleCrud extends RenderCrud
{

    private function category($lang)
    {
        if(is_null($lang )){
            $select = Page::where('template_id', 1)
                ->select("id", "name", 'lang_id')
                ->orderBy("id", "asc")
                ->get();

            $query = MultiLanguageSelect::multiLang($select);

        }
        else{
            $query = Page::where('template_id', 1)
                ->where('lang_id', $lang)
                ->orderBy("id", "asc")
                ->pluck("name", "id");
        }


        return $query;
    }


    public function fields($lang, $action, $data = null)
    {
        $fields = [
            [
                "label" => 'Başlıq',
                "db" => "title",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Slug (URL)",
                "db" => "slug",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'title'=>"Boş saxladığınız təqdirdə sluq avtomatik yaradılacaq."],
            ],
            [
                "label" => "Kateqoriya",
                "db" => "category_id",
                "type" => "select",
                "data" => $this->category($lang),
                "selected" => null,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Tarix",
                "db" => "published_at",
                "type" => "text",
                "attr" => ['class'=>'form-control datepicker']
            ],
            [
                "label" => "Şəkil",
                "db" => "file",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    $group_btn =
                    '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div>';
                },
                'edit' => false
            ],
            [
                "label" => "Qısa məzmun",
                "db" => "summary",
                "type" => "textarea",
                "attr" => ['class'=>'form-control', 'rows' => 5],
            ],
            [
                "label" => "Məzmun",
                "db" => "content",
                "type" => "textarea",
                "attr" => ['class'=>'form-control ckeditor' , 'id' => 'ck-editor']
            ],

            [
                "label" => "Status",
                "db" => "status",
                "type" => "select",
                "data" => config('config.article-status'),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
        ];
        return $this->render($fields, $action, $data);
    }


}


