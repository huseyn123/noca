<?php

namespace App\Crud;


class SliderCrud extends RenderCrud
{

    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => 'Başlıq',
                "db" => "title",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Qısa məzmun',
                "db" => "summary",
                "type" => 'textarea',
                "attr" => ['class'=>'form-control', 'rows' => 5]
            ],
            [
                "label" => "Şəkil",
                "db" => "file",
                "type" => "file",
                "show" => true,
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false){
                        $img = '<div class="input-group"><img src="'.asset("storage/thumb/$data->image").'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
            [
                "label" => "Dil",
                "db" => "lang_id",
                "type" => "select",
                "data" => config('app.locales'),
                "selected" => array_first(config('app.locales')),
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Link",
                "db" => "link",
                "type" => "text",
                "attr" => ['class'=>'form-control']
            ],
        ];
        return $this->render($fields, $action, $data);
    }


}


