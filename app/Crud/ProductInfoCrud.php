<?php

namespace App\Crud;

use App\Models\Page;
use App\Logic\MultiLanguageSelect;

class ProductInfoCrud extends RenderCrud
{

    private function category($lang)
    {
        if(is_null($lang)){
            $select = Page::where('template_id', 2)
                ->select("id", "name", 'lang_id')
                ->orderBy("id", "asc")
                ->get();

            $query = MultiLanguageSelect::multiLang($select);

        }
        else{
            $query = Page::where('template_id', 2)
                ->where('lang_id', $lang)
                ->select("id", "name", 'lang_id')
                ->orderBy("id", "asc")
                ->pluck("name", "id");
        }

        return $query;
    }




    public function fields($lang, $action, $data = null)
    {
        $fields = [
            [
                "label" => 'Ad',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Kateqoriya",
                "db" => "parent_id",
                "type" => "select",
                "data" => $this->category($lang),
                "selected" => null,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Məzmun",
                "db" => "content",
                "type" => "textarea",
                "attr" => ['class'=>'form-control ckeditor', 'id' => 'editor-'.$lang, 'rows' => 5]
            ],
        ];
        return $this->render($fields, $action, $data);
    }


}


