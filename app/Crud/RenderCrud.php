<?php

namespace App\Crud;

use Collective\Html\FormFacade;

abstract class RenderCrud
{

    protected function render($fields, $action, $data = null)
    {
        if($action == 'show'){
            return $this->renderToHtmlShow($fields, $action, $data);
        }
        elseif($action == 'profile'){
            return $this->renderToHtml($fields, $action, $data);
        }
        elseif(request()->ajax()) {
            return $this->renderToHtmlModal($fields, $action, $data);
        }
        else{
            return $this->renderToHtml($fields, $action, $data);
        }
    }

    private function renderForm($field, $data)
    {
        if($field['type'] == 'select'){
            $form = FormFacade::select($field['db'], $field['data'], $data == false ? $field['selected'] : old($field['db'], $data->{$field['db']}), $field['attr']);
        }
        else if($field['type'] == 'file'){
            $form = FormFacade::file($field['db'], $field['attr']);
        }
        else if($field['type'] == 'checkbox'){
            $form = FormFacade::checkbox($field['db'], $field['value'], false, $field['attr']);
        }
        else if($field['type'] == 'password'){
            $form = FormFacade::password($field['db'], $field['attr']);
        }
        else{
            $form = FormFacade::{$field['type']}($field['db'], $data == false ? null : old($field['db'], $data->{$field['db']}), $field['attr']);
        }

        if(isset($field['design'])){
            $render = call_user_func($field['design'], $form, $data);
        }
        else{
            $render = $form;
        }

        return $render;
    }


    private function renderToHtml($fields, $action, $data)
    {
        $html = '';

        foreach ($fields as $field){

            if(!isset($field[$action]) || $field[$action] == true){
                $html.='<div class="'.@$field['divClass'].' form-class form-group col-md-12 hh'.@implode(' hh', $field['hide']).'">';

                $label = FormFacade::label($field['db'], $field['label']);

                $form = $this->renderForm($field, $data);

                $html.=$label.$form;

                if(isset($field['attr']['title'])){
                    $html.='<p class="help-block">'.$field['attr']['title'].'</p>';
                }

                $html.='</div>';
            }

        }

        return $html;
    }


    private function renderToHtmlModal($fields, $action, $data)
    {

        $html = '';

        foreach ($fields as $field){

            if(!isset($field[$action]) || $field[$action] == true){
                $html.='<div class="form-group">';

                $label = FormFacade::label($field['db'], $field['label'], array('class' => 'col-md-4 control-label'));

                $form = $this->renderForm($field, $data);


                $html.=$label;

                $html.='<div class="col-md-6 ">';

                $html.=$form;

                $html.='</div></div>';
            }

        }
        return $html;
    }

    private function renderToHtmlShow($fields,$action,$data)
    {
        $html = '';

        foreach ($fields as $field){

            $html.='<tr>
                        <td><label for="">'.$field['label'].'</label> </td>';

            if(isset($field['design'])){
                $html.= '<td><div class="input-group">';
                if($data[$field['db']]){
                    $html.= '<img src="'.asset('storage/thumb/'.$data[$field['db']]).'">';
                }
                $html.= '</div></td>';
            }
            else{
                $html.='<td>'.$data[$field['db']].'</td>';
            }
            $html.='<tr>';

        }

        return $html;
    }
}