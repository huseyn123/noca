<?php

namespace App\Crud;
use App\Models\Page;
use App\Logic\MultiLanguageSelect;

class CategoryCrud extends RenderCrud
{

    private function category($lang)
    {
        if(is_null($lang)){
            $select = Page::whereNotIn('slug', ['index', 'search'])
                ->whereNotIn('template_id', [10])
                ->select("id", "name", 'lang_id')
                ->orderBy("id", "asc")
                ->get();

            $query = MultiLanguageSelect::multiLang($select);

        }
        else{
            $query = Page::whereNotIn('slug', ['index', 'search'])
                ->whereNotIn('template_id', [10])
                ->where('lang_id', $lang)
                ->orderBy("id", "asc")
                ->pluck("name", "id");
        }

        return $query;
    }


    public function fields($lang, $action, $data = null)
    {
        $fields = [
            [
                "label" => "Modul",
                "db" => "template_id",
                "type" => "select",
                "data" => config('config.template2'),
                "selected" => 0,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Ad',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Slug (URL)",
                "db" => "slug",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'title'=>"Boş saxladığınız təqdirdə sluq avtomatik yaradılacaq."],
                'hide' => [10,14]

            ],
            [
                "label" => "Kateqoriya",
                "db" => "parent_id",
                "type" => "select",
                "data" => $this->category($lang),
                "selected" => null,
                "attr" => ['class'=>'form-control  select-search']
            ],
            [
                "label" => "Cover şəkil",
                "db" => "file",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div>';
                },
                'hide' => [9,13],
                'edit' => false
            ],
            [
                "label" => "Ikon",
                "db" => "icon",
                "type" => "select",
                "data" => config('config.services-icon'),
                "selected" => null,
                "attr" => ['class'=>'form-control', 'title' => 'Icon seçilərsə Ana səhifədə yer alacaq'],
                "hide" => [0,1,4,6,8,9,10,13]
            ],
            [
                "label" => "Qısa Məzmun",
                "db" => "summary",
                "type" => "textarea",
                "hide" => [1,4,6,10,13],
                "attr" => ['class'=>'form-control ', 'rows' => 5]
            ],
            [
                "label" => "Məzmun",
                "db" => "content",
                "type" => "textarea",
                "hide" => [1,4,6,10,13],
                "attr" => ['class'=>'form-control ckeditor', 'id' => 'editor-'.$lang, 'rows' => 5]
            ],
            [
                "label" => "Görünüş",
                "db" => "visible",
                "type" => "select",
                "data" => config('config.menu-visibility-boolean'),
                "selected" => 1,
                "attr" => ['class'=>'form-control'],
                'hide' => [10]
            ],
            [
                "label" => "Linkin quruluşu",
                "db" => "target",
                "type" => "select",
                "data" => config('config.menu-target'),
                "selected" => 1,
                "attr" => ['class'=>'form-control'],
                'hide' => [10,14]
            ],
            [
                "label" => "Link",
                "db" => "forward_url",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'title'=>"Səhifəni başqa ünvana yönləndirmək üçün nəzərdə tutulub.", 'placeholder' => 'Məs: http://google.az'],
                'hide' => [10]
            ],
            [
                "label" => "Meta description",
                "db" => "meta_description",
                "type" => "textarea",
                "attr" => ['class'=>'form-control', 'maxlength' => 160, 'rows' => 3, 'title' => 'Maksimum simvol sayı 160 olmalıdır.Sosial şəbəkələrdə paylaşılan zaman məhz bu yazı linkin altında göstəriləcək'],
                'hide' => [10]

            ],
            [
                "label" => "Meta keywords",
                "db" => "meta_keywords[]",
                "type" => "select",
                "data" => [],
                "selected" => null,
                "attr" => ['class'=>'form-control meta_keywords', 'id'  => 'meta_keywords_'.$lang, 'multiple' => 'multiple', 'autocomplete' => 'off', 'title' => 'Keyword-ləri bir birindən ayırmaq üçün Enter düyməsini sıxın.'],
                "hide" => [6,10]
            ],
        ];
        return $this->render($fields, $action, $data);
    }


}


