<?php

namespace App\Crud;

use App\Models\Page;
use App\Logic\MultiLanguageSelect;

class ProductCrud extends RenderCrud
{

    private function category($lang)
    {
        if(is_null($lang)){
            $select = Page::where('template_id', 13)
                ->select("id", "name", 'lang_id')
                ->orderBy("id", "asc")
                ->get();

            $query = MultiLanguageSelect::multiLang($select);

        }
        else{
            $query = Page::where('template_id', 13)
                ->where('lang_id', $lang)
                ->select("id", "name", 'lang_id')
                ->orderBy("id", "asc")
                ->pluck("name", "id");
        }

        return $query;
    }




    public function fields($lang, $action, $data = null)
    {
        $fields = [
            [
                "label" => 'Ad',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Slug (URL)",
                "db" => "slug",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'title'=>"Boş saxladığınız təqdirdə sluq avtomatik yaradılacaq."],
                'hide' => [10]

            ],
            [
                "label" => "Kateqoriya",
                "db" => "parent_id",
                "type" => "select",
                "data" => $this->category($lang),
                "selected" => null,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Şəkil",
                "db" => "file",
                "type" => "file",
                "show" => true,
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false){
                        $title = "Yenilə";
                        $img = '<div class="input-group"><img src="'.asset("storage/{$data->image->filename}").'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
                'edit' => false
            ],
            [
                "label" => "Məhsulun məlumat kitabı",
                "db" => "image_file",
                "type" => "file",
                "show" => true,
                "attr" => ['class'=>'form-control image', 'style' => 'display:none'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false){
                        $title = "Yenilə";
                        $img = '<div class="input-group">
                                    <a href="'.asset("storage/pdf/{$data->image_file}").'" download>'.$data->image_file.'</a>
                                </div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" >';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
            [
                "label" => "Məzmun",
                "db" => "content",
                "type" => "textarea",
                "attr" => ['class'=>'form-control ckeditor', 'id' => 'editor-'.$lang, 'rows' => 5]
            ],
            [
                "label" => "Linkin quruluşu",
                "db" => "target",
                "type" => "select",
                "data" => config('config.menu-target'),
                "selected" => 1,
                "attr" => ['class'=>'form-control'],
            ],
            [
                "label" => "Link",
                "db" => "forward_url",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'title'=>"Səhifəni başqa ünvana yönləndirmək üçün nəzərdə tutulub.", 'placeholder' => 'Məs: http://google.az'],
            ],
            [
                "label" => "Meta description",
                "db" => "meta_description",
                "type" => "textarea",
                "attr" => ['class'=>'form-control', 'maxlength' => 160, 'rows' => 3, 'title' => 'Maksimum simvol sayı 160 olmalıdır.Sosial şəbəkələrdə paylaşılan zaman məhz bu yazı linkin altında göstəriləcək'],
            ],
            [
                "label" => "Meta keywords",
                "db" => "meta_keywords[]",
                "type" => "select",
                "data" => [],
                "selected" => null,
                "attr" => ['class'=>'form-control meta_keywords', 'id'  => 'meta_keywords_'.$lang, 'multiple' => 'multiple', 'autocomplete' => 'off', 'title' => 'Keyword-ləri bir birindən ayırmaq üçün Enter düyməsini sıxın.'],
            ],
        ];
        return $this->render($fields, $action, $data);
    }


}


