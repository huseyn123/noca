<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Hash;
use Auth;
use App;
use Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('current_password_match', function($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Auth::user()->password);
        });

        if(Cache::get('webConfig')){

            $config = App\Logic\WebCache::getConfig();
            App\Logic\Smtp::set($config);

            app()->setLocale($config->lang_id);
        }

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
