<?php

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use App\Models\Page;
use App\Logic\userAction;
use Yajra\DataTables\EloquentDataTable;

class CategoryDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }


    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('visible', function($post) {
                return label(config("config.menu-visibility-label.$post->visible"), config("config.menu-visibility-boolean.$post->visible"));
            })
            ->editColumn('template_id', function($post) {
                return config("config.template2.$post->template_id");
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-page', ['route' => 'category', 'page' => $row, 'deleted' => $this->trashed])->render();
            })
            ->rawColumns(['visible', 'action', 'summary']);
    }



    public function query(Page $model)
    {
        $query = $model->newQuery()
            ->leftJoin('pages as parent', 'parent.id', '=', 'pages.parent_id')
            ->whereNotNull('pages.parent_id')
            ->whereNotIn('pages.template_id', [2,3])
            ->select('pages.*', 'parent.name as parent');

        if($this->trashed == true){
            $query->onlyTrashed();
        }

        else{
            if(array_key_exists(auth()->user()->dt_view, config('app.locales'))){
                $query->where('pages.lang_id', auth()->user()->dt_view);
            }
        }

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '145px' : '120px', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'pages.id', 'title' => 'ID', 'searchable' => false],
            ['data' => 'name', 'name' => 'pages.name', 'title' => 'Ad'],
            ['data' => 'slug', 'name' => 'pages.slug', 'title' => 'Slug'],
            ['data' => 'lang_id', 'name' => 'pages.lang_id', 'title' => 'Dil', 'searchable' => false],
            ['data' => 'visible', 'name' => 'pages.visible', 'title' => 'Status', 'searchable' => false],
            ['data' => 'parent', 'name' => 'parent.name as parent', 'title' => 'Kateqoriya', 'searchable' => false],
            ['data' => 'template_id', 'name' => 'pages.template_id', 'title' => 'Modul', 'searchable' => false, 'class' => 'none'],
            ['data' => 'created_at', 'name' => 'pages.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'pages.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [25,50]
        ];
    }
}
