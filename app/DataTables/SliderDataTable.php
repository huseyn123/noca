<?php

namespace App\DataTables;

use App\Models\Slider;
use Yajra\DataTables\Services\DataTable;
use App\Logic\userAction;
use Yajra\DataTables\EloquentDataTable;

class SliderDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }


    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'widgets.action-slider')
            ->editColumn('image', function($slider) {
                return '<img src="'.asset("storage/thumb/".$slider->image).'">';
            })

            ->addColumn('action', function($row) {
                return view( 'widgets.action-datatable', ['route' => 'slider', 'id' => $row->id, 'deleted' => $this->trashed])->render();
            })
            ->rawColumns(['image', 'action']);
    }



    public function query(Slider $model)
    {
        $query = $model->newQuery();

        if($this->trashed == true){
            $query->onlyTrashed();
        }

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '90px' : '60px', 'orderable' => false, 'searchable' => false, 'title' => ' '])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'sliders.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'sliders.image', 'title' => 'Şəkil', 'searchable' => false, 'orderable' => false],
            ['data' => 'lang_id', 'name' => 'sliders.lang_id', 'title' => 'Dil', 'searchable' => false, 'orderable' => true],
            ['data' => 'created_at', 'name' => 'sliders.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'sliders.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [5,10]
        ];
    }


    protected function filename()
    {
        return 'sliderdatatable_' . time();
    }
}
