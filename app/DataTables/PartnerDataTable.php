<?php

namespace App\DataTables;

use App\Models\Partner;
use Yajra\DataTables\Services\DataTable;
use App\Logic\userAction;
use Yajra\DataTables\EloquentDataTable;

class PartnerDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }


    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('image', function($slider) {
                return '<img src="'.asset("storage/".$slider->image).'">';
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-datatable', ['route' => 'partners', 'id' => $row->id, 'deleted' => $this->trashed])->render();
            })
            ->rawColumns(['image', 'action']);
    }



    public function query(Partner $model)
    {
        $query = $model->newQuery();

        if($this->trashed == true){
            $query->onlyTrashed();
        }

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '90px' : '60px', 'orderable' => false, 'searchable' => false, 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'partners.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'partners.image','title' => 'Şəkil', 'searchable' => false, 'orderable' => false],
            ['data' => 'created_at', 'name' => 'partners.created_at','title' => 'Yaradıldı', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'partners.updated_at', 'title' => 'Yenilənib', 'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25]
        ];
    }



    protected function filename()
    {
        return 'partnerdatatable_' . time();
    }
}
