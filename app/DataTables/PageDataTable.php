<?php

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use App\Models\Page;
use App\Logic\userAction;
use Yajra\DataTables\EloquentDataTable;

class PageDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('visible', function($post) {
                return label(config("config.menu-visibility-label.$post->visible"), config("config.menu-visibility.$post->visible"));
            })

            ->editColumn('has_slider', function($post) {
                return label(config("config.label.$post->has_slider"), icon(config("config.yes-no.$post->has_slider")));
            })
            ->editColumn('has_partner', function($post) {
                return label(config("config.label.$post->has_partner"), icon(config("config.yes-no.$post->has_partner")));
            })
            ->editColumn('filename', function($post) {
                if($post->image){
                    return '<img src='.asset("storage/thumb/".$post->image->filename).'>';
                }
                else{
                    return '';
                }

            })
            ->editColumn('template_id', function($post) {
                return config("config.template.$post->template_id");
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-page', ['route' => 'page', 'page' => $row, 'deleted' => $this->trashed])->render();
            })
            ->rawColumns(['visible', 'action', 'has_slider', 'has_partner', 'template_id', 'summary', 'filename']);
    }



    public function query(Page $model)
    {
        $query = $model->newQuery()
            ->whereNull('parent_id')
            ->with(['image:id,filename,gallery_id']);


        if($this->trashed == true){
            $query->onlyTrashed();
        }

        else{
            if(array_key_exists(auth()->user()->dt_view, config('app.locales'))){
                $query->where('lang_id', auth()->user()->dt_view);
            }
        }

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'orderable' => false, 'searchable' => false, 'visible' => userAction::showAction($this->trashed), 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => 'ID', 'searchable' => false],
            ['data' => 'name', 'name' => 'name', 'title' => 'Ad'],
            ['data' => 'slug', 'name' => 'slug', 'title' => 'Slug'],
            ['data' => 'lang_id', 'name' => 'lang_id', 'title' => 'Dil', 'searchable' => false],
            ['data' => 'visible', 'name' => 'visible', 'title' => 'Status', 'searchable' => false],
            ['data' => 'filename', 'name' => 'filename', 'title' => 'Şəkil', 'orderable' => false, 'class' => 'none'],
            ['data' => 'template_id', 'name' => 'template_id','title' => 'Modul', 'searchable' => false, 'class' => 'none'],
            ['data' => 'summary', 'name' => 'summary', 'title' => 'Qısa məzmun', 'searchable' => false, 'class' => 'none'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25,50]
        ];
    }
}
