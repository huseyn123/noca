<?php

namespace App\DataTables;

use App\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
class UserDataTable extends DataTable
{
    protected $blocked;

    public function blocked($block) {
        $this->blocked = $block;
        return $this;
    }

    protected function protectDev($query) {
        if(auth()->user()->id != 1){
            $protect = $query->where('id', '<>', 1);
        }
        else{
            $protect = $query;
        }

        return $protect;
    }


    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'widgets.action-user')
            ->editColumn('role', function($user) {
                return config("config.role.$user->role");
            });
    }


    public function query(User $model)
    {
        $query = $model->newQuery();

        if($this->blocked == true){
            $query->onlyTrashed();
        }

        return $this->protectDev($query);
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '50px', 'orderable' => false, 'searchable' => false, 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'users.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'first_name', 'name' => 'users.first_name','title' => 'Ad'],
            ['data' => 'last_name', 'name' => 'users.last_name','title' => 'Soyad'],
            ['data' => 'email', 'name' => 'users.email','title' => 'Email'],
            ['data' => 'role', 'name' => 'users.role','title' => 'Vəzifə', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'users.created_at','title' => 'Yaradıldı', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'users.updated_at','title' => 'Yenilənib', 'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25,50]
        ];
    }


    protected function filename()
    {
        return 'userdatatables_' . time();
    }
}
