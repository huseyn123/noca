<?php

namespace App\Http\Controllers;

use App\Logic\WebCache;
use App\Models\Page;
use App\Models\Article;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Logic\Menu;
use DB;

abstract class BaseController extends Controller
{
    use DispatchesJobs, ValidatesRequests;

    private $dictionary;

    public $lang,$config,$menu;

    abstract protected function index();
    abstract protected function staticPage($getPage);
    abstract protected function articleSingle($getPage, $slug);
    abstract protected function productSingle($getPage,$slug);


    public function __construct()
    {

        $webCache = new WebCache;
        $menu = new Menu;


        $this->lang = app()->getLocale();

        $this->menu = $menu;

        $this->config = $webCache->getConfig();
        $this->dictionary = $webCache->getDictionary($this->lang);
        $sliders = $webCache->getSlider($this->lang);
        $partners = $webCache->getPartner();
        $social = $webCache->getSocial();

        view()->share('menu', $menu->all());
        view()->share('social', $social);
        view()->share('rel', $menu->relationPages());
        view()->share('webConfig', $this->config);
        view()->share('social', $social);
        view()->share('dictionary', $this->dictionary);
        view()->share('sliders', $sliders);
        view()->share('partners', $partners);
        view()->share('lang', $this->lang);

    }

    protected function checkRoute($slug)
    {
        $getPage = Page::where('slug', $slug)->where('lang_id', $this->lang)->firstOrFail();

        return $getPage;

    }
    
    public function showPage($slug)
    {
        $getPage = $this->checkRoute($slug);

        if (!is_null($getPage->forward_url)) {
            return redirect()->to($getPage->forward_url);
        }


        if ($getPage->slug == 'index') {
            return redirect()->route('home');
        }




        if ($getPage->template_id == 0) {
            return $this->staticPage($getPage);
        }
        if ($getPage->template_id == 1) {
            return $this->news($getPage);
        }
        elseif ($getPage->template_id == 4) {
            return $this->contact($getPage);
        }
        elseif ($getPage->template_id == 6) {
            return $this->dropdown($getPage);
        }
        elseif ($getPage->template_id == 7) {
            return $this->staticPage($getPage);
        }
        elseif ($getPage->template_id == 8) {
            return $this->staticPage($getPage);
        }
        elseif ($getPage->template_id == 10) {
            return $this->block($getPage);
        }
        elseif ($getPage->template_id == 13) {
            return $this->product($getPage,'product');
        }
        else{
            return view('errors.404');
        }
    }


    public function showArticle($category, $slug)
    {
        $getPage = $this->checkRoute($category);

        if($getPage->template_id == 1) {
            return $this->articleSingle($getPage, $slug);
        }
        elseif($getPage->template_id == 13) {
            return $this->productSingle( $getPage, $slug);
        }
        else {
            return $this->index();
        }
    }


    protected function block($parentPage)
    {
        if ($parentPage->template_id != 10) {
            return redirect()->route('showPage', $parentPage->slug);
        } else {
            return redirect()->route('home');
        }
    }


    protected function dropdown($page)
    {
        if ($page->children->count()) {
            return redirect()->route('showPage', $page->children->first()->slug);
        } else {
            return $this->staticPage($page);
        }
    }

    public function search()
    {
        $keyword = strip_tags(request()->get('keyword'));

        if(trim($keyword) == '')
        {
            return redirect()->route('home');
        }

        $cat = Page::join('pages as parent', 'pages.parent_id', '=', 'parent.id')
            ->leftJoin('galleries', function ($join) {
                $join->on('pages.relation_page', '=', 'galleries.gallery_id')->where('galleries.gallery_type', 'App\Models\Page');
            })
            ->where('pages.lang_id', $this->lang)
            ->whereNotIn('pages.template_id', [3,10])
            ->where(function ($query) use($keyword) {
                $query->orWhere('pages.slug', 'like', '%' . $keyword . '%')
                    ->orWhere('pages.name', 'like', '%' . $keyword . '%')
                    ->orWhere('pages.summary', 'like', '%' . $keyword . '%')
                    ->orWhere('pages.content', 'like', '%' . $keyword . '%');
            })
            ->groupBy('galleries.gallery_id')
            ->select('pages.id','pages.slug','pages.name', 'pages.summary', 'pages.content', 'parent.slug as category','pages.created_at as published_at','pages.template_id','galleries.filename as image', DB::raw('1 as type'));


        $results = Article::join('pages as c', 'c.id', '=', 'articles.category_id')
            ->leftJoin('galleries', function ($join) {
                $join->on('articles.relation_page', '=', 'galleries.gallery_id')->where('galleries.gallery_type', 'App\Models\Article');
            })
            ->where('articles.status', 1)
            ->where('c.lang_id', $this->lang)
            ->where(function ($query) use($keyword) {
                $query->orWhere('articles.slug', 'like', '%' . $keyword . '%')
                    ->orWhere('articles.title', 'like', '%' . $keyword . '%')
                    ->orWhere('articles.summary', 'like', '%' . $keyword . '%')
                    ->orWhere('articles.content', 'like', '%' . $keyword . '%');
            })
            ->select('articles.id', 'articles.slug', 'articles.title as name', 'articles.summary', 'articles.content', 'c.slug as category', 'articles.published_at','c.template_id','galleries.filename as image', DB::raw('2 as type'))
            ->union($cat)
            ->orderBy('type', 'asc')
            ->orderBy('id', 'asc')
            ->get();

        return view('web.search', ['results' => $results, 'keyword' => $keyword, 'menuWithoutSlug' => true]);

    }
}
