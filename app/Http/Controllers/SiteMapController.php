<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Config;
use DB;
use Carbon\Carbon;

class SiteMapController extends Controller
{
    public $title;

    public function __construct(Request $request)
    {
        $this->middleware('admin');

        $this->title = "Sitemap";
    }


    public function index()
    {
        $config = Config::findOrFail(1);

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);

        return view('app.sitemap.index', ["breadcrumb" => $breadcrumb->render(), "title" => $this->title, "config" => $config]);
    }


    public function store(Request $request)
    {
        $config = Config::findOrFail(1);

        $page = DB::table('pages as p')
            ->leftJoin('galleries', function ($join) {
                $join->on('galleries.gallery_id', '=', 'p.id')
                    ->where('galleries.gallery_type', '=', 'App\Models\Page')
                    ->where('galleries.main', 1);
            })
            ->whereNull('p.deleted_at')
            ->select('p.id', 'p.slug', 'p.lang_id', 'galleries.filename as image', 'p.name', 'p.updated_at', 'p.relation_page as category', DB::raw('1 as type'))
            ->orderBy('id', 'desc');

        $article = DB::table('articles as a')
            ->join('pages as c', 'c.id', '=', 'a.category_id')
            ->leftJoin('galleries', function ($join) {
                $join->on('galleries.gallery_id', '=', 'a.id')
                    ->where('galleries.gallery_type', '=', 'App\Models\Article')
                    ->where('galleries.main', 1);
            })
            ->whereNull('a.deleted_at')
            ->whereNull('c.deleted_at')
            ->select('a.id', 'a.slug', 'c.lang_id', 'galleries.filename as image', 'a.title as name', 'a.updated_at', 'c.slug as category', DB::raw('2 as type'))
            ->orderBy('a.id', 'desc')
            ->union($page)
            ->orderBy('type', 'asc')
            ->get();


        $sitemap = app()->make("sitemap");


        foreach ($article as $post)
        {
            $post->slug == 'index' ? $slug = '' : $slug = $post->slug;


            if(!is_null($post->image))
            {
                $images = [
                    ['url' => asset("storage/$post->image"), 'title' => $post->name, 'caption' => $post->name],
                ];
            }
            else{
                $images = 0;
            }


            if($post->type == 1){
                $sitemap->add(url("/$post->lang_id/".$slug),  Carbon::parse($post->updated_at), '1.0', 'monthly', $images);
            }
            else{
                $sitemap->add(url("/$post->lang_id/$post->category/$slug"),  Carbon::parse($post->updated_at), '1.0', 'monthly', $images);

            }

        }

        $sitemap->store('xml', 'sitemap');

        $config->sitemap = Carbon::now();
        $config->save();

        $request->session()->flash('message', trans('locale.store_success'));

        return redirect()->back();
    }
}
