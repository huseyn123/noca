<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\BaseController;
use App\Models\Config;
use App\Models\Article;
use App\Models\Page;
use DB;


class MainController extends BaseController
{
    public function __construct()
    {
        $this->middleware('web');

        parent::__construct();

        $category_article = Page::where('template_id', 3)->where('lang_id', $this->lang)->select('id','slug', 'name')->first();

        view()->share('category_article', $category_article);

    }


    public function index()
    {
      $getPage = $this->checkRoute('index');

      $services = Page::where('template_id',7)->where('lang_id',$this->lang)->whereNotNull('icon')->limit(4)->ordered()->get();

      $projects = Page::where('template_id',8)->where('lang_id', $this->lang)->limit(2)->ordered()->get();


       $products = Page::where('pages.template_id',2)
            ->leftJoin('pages as parent','parent.id','pages.parent_id')
            ->where('pages.lang_id',$this->lang)
            ->select('pages.*','parent.slug as parent_slug')
            ->limit(3)
            ->get();

        return view('web.index', ['page' => $getPage, 'title' => $getPage->name,'services' => $services,'projects' => $projects,'products' => $products,'menuWithoutSlug' => true]);
    }

    public function staticPage($getPage,$gallery = null)
    {
        $cat =  $this->menu->getData($getPage, 0);

        return view("web.static", ['page' => $getPage,'cat' => $cat,'pageTitle' => $getPage->name] );
    }


    protected function articleSingle($getPage , $slug)
    {
        $getArticle = Article::where('slug', $slug)->firstOrFail();
        $projects = Page::where('template_id',8)->where('lang_id',$this->lang)->limit(3)->get();

        $cat =  $this->menu->getData($getPage, 0);
        return view('web.article_single', ['article' => $getArticle, 'page' => $getPage,'menuWithoutSlug' => true,'pageTitle' => $getPage->title,'projects' =>$projects,'cat' => $cat]);
    }


    protected function productSingle($getPage , $slug)
    {
        $product = Page::where('slug', $slug)->firstOrFail();
        $cat =  $this->menu->getData($getPage, 0);

        return view('web.product_single', ['page' => $getPage, 'product' => $product ,'menuWithoutSlug' => true,'pageTitle' => $getPage->title,'cat' =>$cat]);
    }

    protected function news($getPage)
    {
        $cat =  $this->menu->getData($getPage, 0);
        return view("web.news", ['page' => $getPage,'pageTitle' => $getPage->name,'cat' => $cat]);
    }

    protected function product($getPage)
    {
        $cat =  $this->menu->getData($getPage, 0);
        return view("web.product", ['page' => $getPage,'pageTitle' => $getPage->name,'cat' =>$cat]);
    }

    protected function contact($getPage)
    {
        $cat =  $this->menu->getData($getPage, 0);
        return view("web.contact", ['page' => $getPage,'pageTitle' => $getPage->name,'cat' =>$cat]);
    }



}