<?php

namespace App\Http\Controllers;

use App\DataTables\ProductInfoDataTable;
use Illuminate\Support\Facades\Validator;
use App\Logic\ImageRepository;
use Illuminate\Http\Request;
use App\Models\Page;
use Illuminate\Support\Facades\Storage;
use App\Crud\ProductInfoCrud;
use DB;
use App\Logic\Slug;

class ProductInfoController extends Controller
{
    private $crud, $route, $image, $requests;
    public $title;

    public function __construct(Request $request, ImageRepository $imageUpload, ProductInfoCrud $crud)
    {
        parent::__construct();

        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->image = $imageUpload;
        $this->crud = $crud;

        $this->requests = $request->except('_token', '_method', 'file', 'return');
        $this->title = "Məlumat";
        $this->route = 'product_info';

    }



    public function index(ProductInfoDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);

        return $dataTable->render("app.$this->route.index", ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }



    public function trashed(ProductInfoDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));
        $breadcrumb->addCrumb(trans('locale.trash'));
        return $dataTable->trashed(true)->render("app.$this->route.index",  ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }



    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("product_info.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        $fields = $this->crud->fields(null, 'create');

        return view("app.page.create",  ["breadcrumb" => $breadcrumb->render(), 'fields' => $fields, 'route' => 'product_info']);
    }



    public function store(Request $request)
    {

        //check if it this page exists or not

        if ($request->has('relation_page')) {
            $existPage = Page::where('relation_page', $request->input('relation_page'))->where('lang_id', $request->input('lang_id'))->first();

            if ($existPage) {
                return $this->update($request, $existPage->id);
            }
        }

        $category = Page::find($request->input('parent_id'));


        if(!$category){
            $response = $this->responseArray(1,"Kateqoriya seçilməyib");
            return $this->validateResponse($request, $response);
        }

        $validator = Validator::make($request->all(), Page::productİnfoRules($category->lang_id), Page::$messages);

        if ($validator->fails()) {
            $response = $this->responseArray(1,$validator->errors()->first());
            return $this->validateResponse($request, $response);
        }


        $this->requests['template_id'] = 3;
        $this->requests['lang_id'] = $category->lang_id;
        $this->requests['parent_id'] = $category->id;
        $this->requests['order'] = Page::max('order') + 1;
        $this->requests['visible'] = 1;
        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('name'), 'page', $category->lang_id);


        DB::beginTransaction();

        try {

            $page = Page::create($this->requests);

        } catch (\Exception $e) {
            DB::rollback();
            $response = $this->responseArray(1, $e->getMessage());
            return $this->validateResponse($request, $response);
        }


        if (!$request->has('relation_page')) {
            try {
                $page->relation_page = $page->id;
            } catch (\Exception $e) {
                DB::rollback();
                $response = $this->responseArray(1,$e->getMessage());
                return $this->validateResponse($request, $response);
            }

            if($request->hasFile('file')){
                try{
                    $this->image->morphStore('file', $page, $this->resize());
                }
                catch(\Exception $e){
                    DB::rollback();
                    $response = $this->responseArray(1, 'danger',  $e->getMessage());
                    return $this->validateResponse($request, $response);
                }
            }
        }

        $page->save();


        DB::commit();

        if($request->has('return'))
        {
            $request->session()->flash('message', "Yeni məlumat əlavə olundu");
            return redirect()->route('product_info.index');
        }

        $response = $this->responseArray(0, "Yeni məlumat əlavə olundu", false, false, false, ['slug' => $page->slug, 'uid' => $page->id]);
        return $this->validateResponse($request, $response);
    }



    public function edit($id)
    {
        $keys = [];
        $fields = [];
        $langs = [];

        $page = Page::findOrFail($id);

        $relatedPage = Page::where('relation_page', $page->relation_page)->get();

        foreach ($relatedPage as $rel){
            $fields[$rel->lang_id] = $this->crud->fields($rel->lang_id, 'edit', $rel);  // data
            $langs[$rel->lang_id] = $rel->lang_id;
            $keys[$rel->lang_id] = array_filter(explode(",", $rel->meta_keywords));
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields($key, 'edit');  //no data
            $keys[$key] = [];
        }


        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("product_info.index"));
        $breadcrumb->addCrumb($page->name);


        return view('app.product_info.edit', ['info' => $page, 'langs' => $langs, "breadcrumb" => $breadcrumb->render(), 'fields' => $fields, 'relatedPage' => $relatedPage, 'keys' => $keys, 'route' => 'product_info']);
    }


    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);

        $category = Page::find( $request->input('parent_id'));

        $validator = Validator::make($request->all(), Page::productİnfoRules($category->lang_id, $id), Page::$messages);


        if ($validator->fails()) {
            $response = $this->responseArray(1,$validator->errors()->first());
            return $this->responseJson($response);
        }


        $this->requests['lang_id'] = $category->lang_id;
        $this->requests['parent_id'] = $category->id;
        $this->requests['visible'] = 1;
        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('name'), 'page', $category->lang_id);


        try{
            foreach($this->requests as $key => $put){
                $page->$key = $put;
            }

            $page->save();
        }
        catch(\Exception $e){
            $response = $this->responseArray(1, $e->getMessage());
            return $this->responseJson($response);
        }


        $response = $this->responseArray(0, 'success',  trans('locale.update_success'),  false, false, false, ['slug' => $page->slug]);
        return $this->responseJson($response);
    }


    public function updateImage(Request $request, $id)
    {
        $page = Page::findOrFail($id);

        if($request->hasFile('file')){

            $this->image->morphUpdate('file', $page, $this->resize());
        }

        request()->session()->flash('message', trans('locale.update_success'));
        return redirect()->back();
    }


    public function trash($id)
    {
        dd();
        $category = Page::findOrFail($id);
        $category->delete();

        request()->session()->flash('message', "Məlumat silindi. Qaytarılması mümkündür.");
        return redirect()->route('product_info.index');
    }



    protected function resize()
    {
        $resizeImage = ['resize' => ['fit' => false, 'size' => [200, 150]], 'thumb' => null ];

        return $resizeImage;
    }
}
