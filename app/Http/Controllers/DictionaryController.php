<?php

namespace App\Http\Controllers;

use App\Crud\DictionaryCrud;
use Illuminate\Http\Request;
use App\Models\Dictionary;
use App\DataTables\DictionaryDataTable;
use DB;

class DictionaryController extends Controller
{
    private $crud, $requests;
    public $title;

    public function __construct(DictionaryCrud $crud, Request $request)
    {
        $this->title = "Lüğət";
        $this->crud = $crud;

        if (in_array(strtolower($request->method()), ['put', 'patch', 'post'])) {
            clearCache('dictionary');
        }

        $this->requests = $request->except('_token', '_method');
    }


    public function index(DictionaryDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);

        return $dataTable->render('app.dictionary.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('dictionary.index'));
        $breadcrumb->addCrumb(trans('locale.create'));

        $fields = $this->crud->fields('create');

        return view('app.dictionary.create', ['fields' => $fields, 'title' => $this->title, "breadcrumb" => $breadcrumb->render() ]);
    }


    public function edit($id)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('dictionary.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        $word = Dictionary::findOrFail($id);

        $words = Dictionary::where('keyword', $word->keyword)->pluck('content', 'lang_id');


        return view('app.dictionary.edit', ['title' => $this->title, "breadcrumb" => $breadcrumb->render(), 'info' => $word, 'words' => $words]);
    }


    public function store(Request $request)
    {
        $inputs = $request->input('content');
        $keyword = $request->input('keyword');
        $editor = $request->input('editor', 0);

        $request->validate(Dictionary::$rules, Dictionary::$messages);


        DB::beginTransaction();


        foreach($inputs as $lang => $input){

            try{
                Dictionary::create(['keyword' => $keyword, 'content' => $input, 'lang_id' => $lang, 'editor' => $editor]);
            }
            catch(\Exception $e){
                DB::rollback();

                return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
            }

        }


        DB::commit();

        return redirect()->route('dictionary.index');
    }


    public function update(Request $request, $id)
    {
        $word = Dictionary::findOrFail($id);

        $inputs = $request->input('content');


        $request->validate(['content' => 'nullable']);

        $find = Dictionary::where('keyword', $word->keyword)->pluck('lang_id')->toArray();


        DB::beginTransaction();


        foreach($inputs as $lang => $input){

            if(in_array($lang, $find))
            {
                try{
                    Dictionary::where('keyword', $word->keyword)->where('lang_id', $lang)->update(['content' => $input]);
                }
                catch(\Exception $e){
                    DB::rollback();

                    $response = $this->responseDataTable(1, $e->getMessage());
                    return $this->responseJson($response);
                }
            }
            else{
                try{
                    Dictionary::create(['keyword' => $word->keyword, 'content' => $input, 'lang_id' => $lang]);
                }
                catch(\Exception $e){
                    DB::rollback();

                    $response = $this->responseDataTable(1, $e->getMessage());
                    return $this->responseJson($response);
                }
            }

        }

        DB::commit();

        $response = $this->responseDataTable(0, trans('locale.update_success'), "#dictionary", "#myModal");
        return $this->responseJson($response);
    }
}
