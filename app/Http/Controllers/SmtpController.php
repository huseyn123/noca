<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cache;
use DB;

class SmtpController extends Controller
{
    private $title;

    public function __construct(Request $request)
    {
        $this->middleware('admin', ['except' => ['index']]);

        $this->title = "SMTP";

        if (in_array(strtolower($request->method()), ['put', 'patch'])) {
            Cache::forget('settings', false);
        }
    }

    public function index()
    {
        $smtp = DB::table('settings')->get();

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb('Tənzimləmələr', route('config.show', 1));
        $breadcrumb->addCrumb($this->title);

        return view('app.settings.smtp.index', ["breadcrumb" => $breadcrumb->render(), "title" => $this->title, 'smtp' => $smtp]);
    }


    public function edit($id)
    {
        $smtp = DB::table('settings')->find($id);

        $smtp->key == 'password' ? $type = 'password' : $type = 'text';

        return view('app.settings.smtp.edit', ['smtp' => $smtp, 'type' => $type]);
    }


    public function update(Request $request, $id)
    {
        DB::table('settings')->where('id', $id)->update(['value' => $request->input('value')]);

        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->back();
    }
}