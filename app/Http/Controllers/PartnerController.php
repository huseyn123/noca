<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logic\ImageRepository;
use App\Models\Partner;
use App\Logic\Order;
use App\DataTables\PartnerDataTable;
use App\Crud\PartnerCrud;
use DB;

class PartnerController extends Controller
{
    private $crud, $image, $request,$order;
    public $title;

    public function __construct(Request $request, ImageRepository $imageUpload, PartnerCrud $crud, Order $order)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy', 'order', 'postOrder']]);

        $this->image = $imageUpload;
        $this->crud = $crud;
        $this->order = $order;
        $this->title = "Tərəfdaşlar";
        $this->requests = $request->except('_token', '_method', 'file');

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('partners', false);
        }
    }



    public function index(PartnerDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);

        return $dataTable->render('app.partners.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }



    public function trashed(PartnerDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("partners.index"));
        $breadcrumb->addCrumb(trans('locale.trash'));

        return $dataTable->trashed(true)->render('app.partners.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }



    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("partners.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        $fields = $this->crud->fields('create');

        return view('app.partners.create', ["fields" => $fields, "breadcrumb" => $breadcrumb->render()]);
    }



    public function store(Request $request)
    {
        $this->requests['order'] = Partner::max('order')+1;

        $request->validate(Partner::rules(), Partner::$messages);


        DB::beginTransaction();

        $upload = $this->image->store('file',$this->resize());    //no resize

        $this->requests['image'] = $upload[1];

        try{
            Partner::create($this->requests);
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }

        DB::commit();


        $request->session()->flash('message', "Logo əlavə olundu");

        return redirect()->route('partners.index');
    }



    public function edit($id)
    {
        $partner = Partner::findOrFail($id);

        $fields = $this->crud->fields('edit', $partner);

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("partners.index"));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view('app.partners.edit', ['info' => $partner, 'fields' => $fields, "breadcrumb" => $breadcrumb->render()]);
    }



    public function update(Request $request, $id)
    {
        $slider = Partner::findOrFail($id);

        $request->validate(Partner::rules(''), Partner::$messages);

        if($request->hasFile('file')){

            $upload = $this->image->store('file',$this->resize(),$slider->image);

            $this->requests['image'] = $upload[1];  //generated hashname with path:   2018-01-01/a5as45sa4d54sad4sadddasd.jpg
        }

        foreach($this->requests as $key => $put){
            $slider->$key = $put;
        }

        $slider->save();


        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->route('partners.index');
    }


    public function order()
    {
       return $this->order->get('partners', $this->title, 'name', false, 1);
    }


    public function postOrder(Request $request)
    {
        return $this->order->post($request, 'partners', false);
    }


    public function trash($id)
    {
        $member = Partner::findOrFail($id);
        $member->delete();

        request()->session()->flash('message', "Logo silindi. Qaytarılması mümkündür.");
        return redirect()->back();
    }


    public function restore($id)
    {
        $member = Partner::onlyTrashed()->findOrFail($id);
        $member->restore();

        request()->session()->flash('message', "Silinmiş logo qaytarıldı və aktiv olundu");
        return redirect()->route('partners.index');
    }


    public function destroy($id)
    {
        $member = Partner::onlyTrashed()->findOrFail($id);
        $member->forceDelete();

        request()->session()->flash('message', "Logo birdəfəlik silindi");
        return redirect()->back();
    }


    protected function resize()
    {
        $resizeImage = ['resize' => ['fit' => false,'size' => [200,null]], 'thumb' => null ];

        return $resizeImage;
    }

}
