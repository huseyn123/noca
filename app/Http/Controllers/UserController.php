<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\DataTables\UserDataTable;
use App\User;
use App\Crud\UserCrud;

class UserController extends Controller
{
    private $crud;
    public $title;

    public function __construct(UserCrud $crud)
    {
        $this->middleware('admin', ['except' => ['show', 'update']]);

        $this->crud = $crud;
        $this->title = "Istifadəçilər";
    }


    public function index(UserDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("user.index"));

        return $dataTable->render('app.user.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function blocked(UserDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("user.index"));
        $breadcrumb->addCrumb(trans('locale.blocked'));
        return $dataTable->blocked(true)->render('app.user.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function show()
    {
        $user = User::findOrFail(auth()->user()->id);

        $fields = $this->crud->fields('profile', $user);

        return view('app.user.show', ["fields" => $fields, "info" => $user]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('app.user.create', ['fields' => $fields]);
    }


    public function store(Request $request)
    {
        $password = $request->input('password');

        $inputs = [
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'password' => $password,
            'role' => $request->input('role'),
        ];

        $rules = array(
            'first_name' => 'required|min:3|max:15|alpha',
            'last_name' => 'required|min:3|max:20|alpha',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'role' => 'required|between:1,'.$this->roleMaxKey()
        );

        $validator = Validator::make($inputs, $rules);

        if ($validator->fails()) {
            $response = $this->responseDataTable(1,  $validator->errors()->first());
            return $this->responseJson($response);
        }

        $inputs['password'] =  bcrypt($password);

        User::create($inputs);

        $response = $this->responseDataTable(0, trans('locale.update_success'), "#users", "#myModal");
        return $this->responseJson($response);
    }


    public function edit($id)
    {
        $user = $this->protectDev($id);

        $fields = $this->crud->fields('edit', $user);

        return view('app.user.edit', ['fields' => $fields, 'info' => $user, 'title' => $user->first_name.' '.$user->last_name, 'id' => $id]);
    }


    public function editPassword()
    {
        $user = User::findOrFail(auth()->user()->id);

        return view('app.user.edit-password', ['info' => $user, 'title' => $user->first_name.' '.$user->last_name]);
    }

    public function update(Request $request, $id)
    {
        $user = $this->protectDev($id);

        $inputs = [
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'dt_view' => $request->input('dt_view'),
        ];

        $rules = [
            'first_name' => 'required|min:3|max:15|alpha',
            'last_name' => 'required|min:3|max:20|alpha',
            'email' => 'required|email|unique:users,email,'.$id,
            'dt_view' => 'required',
        ];

        if(auth()->user()->role == 1 && $request->has('role')){
            $inputs['role'] = $request->input('role');
            $rules['role'] = 'required|between:1,'.$this->roleMaxKey();
        }

        $validator = Validator::make($inputs, $rules);

        if ($validator->fails()) {
            $response = $this->responseDataTable(1, $validator->errors()->first());
            return $this->validateResponse($request, $response);
        }

        foreach($inputs as $key => $put){
            $user->$key = $put;
        }

        $user->save();

        $response = $this->responseDataTable(0, trans('locale.update_success'), "#users", "#myModal");
        return $this->validateResponse($request, $response);
    }


    public function updatePassword(Request $request)
    {
        $user = auth()->user();

        $password = $request->only([
            'current_password', 'password', 'password_confirmation'
        ]);

        $validator = Validator::make($password, [
            'current_password' => 'required|current_password_match',
            'password'     => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect()->route('show_user')->withInput($request->input())->withErrors(["errors" => $validator->errors()->first()]);
        }

        $updated = $user->update([ 'password' => bcrypt($password['password']) ]);

        if ($updated) {
            request()->session()->flash('message', "Şifrəniz yeniləndi");
            return redirect()->route('show_user');
        }
        else{
            return redirect()->route('show_user')->withInput($request->input())->withErrors(["errors" => $validator->errors()->first()]);
        }
    }


    public function trash($id)
    {
        $user = $this->protectDev($id);
        $user->delete();

        request()->session()->flash('message', "istifadəçi blok listə salındı");
        return redirect()->back();
    }


    public function restore($id)
    {
        $user = User::onlyTrashed()->findOrFail($id);
        $user->restore();

        request()->session()->flash('message', "İstifadəçi blok listdən çıxarıldı");
        return redirect()->route('user.index');
    }


    public function destroy($id)
    {
        $user = User::onlyTrashed()->findOrFail($id);
        $user->forceDelete();

        request()->session()->flash('message', "İstifadəçi silindi və qaytarılması mümkün deyil");
        return redirect()->back();
    }

}
