<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Creitive\Breadcrumbs\Breadcrumbs;

abstract class Controller extends BaseController
{

    use DispatchesJobs, ValidatesRequests;


    public function __construct()
    {
    }

    protected function breadcrumb()
    {
        $breadcrumbs = new Breadcrumbs();
        $breadcrumbs->setListElement('ul');
        //$breadcrumbs->setListItemCssClass('');
        $breadcrumbs->addCrumb(webConfig()->company_name, route('dashboard'));
        $breadcrumbs->setCssClasses("breadcrumb");
        $breadcrumbs->setDivider('');

        return $breadcrumbs;
    }


    protected function langMaxKey(){
        $lang = config('app.locales');

        return max(array_keys($lang));
    }

    protected function templateMaxKey(){
        $temp = config('config.template');

        return max(array_keys($temp));
    }


    protected function roleMaxKey(){
        $role = config('config.role');

        return max(array_keys($role));
    }


    protected function visibilityMaxKey(){
        $role = config('config.menu-visibility');

        return max(array_keys($role));
    }


    protected function protectDev($id){
        if(auth()->user()->id != 1){
            $protect = User::where('id', '<>', 1)->where('id', $id)->firstOrFail();
        }
        else{
            $protect = User::findOrFail($id);
        }

        return $protect;
    }


    protected function headers()
    {
        return array('Content-type'=> 'application/json; charset=utf-8');
    }


    protected function responseArray($code, $msg, $once=false, $reset=false, $redirect=false, $data=false)
    {
        return array("error"=> $code, "alert" => $code == 0 ? 'alert-success' : 'alert-danger', "msg" => $msg, "once" => $once, "resetForm" => $reset, "redirect" => $redirect, "data" => $data);
    }

    protected function responseDataTable($code, $msg, $draw=false, $close=false)
    {
        return array("error"=> $code, "alert" => $code == 0 ? 'alert-success' : 'alert-danger', "msg" => $msg, "draw" => $draw, "close" => $close);
    }

    protected function responseJson($response){
        return response()->json($response, 200, $this->headers(), JSON_FORCE_OBJECT, JSON_UNESCAPED_UNICODE);
    }


    protected function validateResponse($request, $response)
    {
        if($request->ajax()){
            return $this->responseJson($response);
        }
        else{
            if($response['error'] == 1){
                return redirect()->back()->withInput($request->input())->withErrors(["errors" => $response['msg']]);
            }
            else{
                $request->session()->flash('message', $response['msg']);
                return redirect()->back();
            }
        }
    }
}
