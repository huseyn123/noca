<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Config;
use App\Models\Dictionary;
use App\Mail\ContactForm;
use Mail;

class ContactController extends Controller
{

    public $lang;

    public function __construct()
    {
        $this->lang = app()->getLocale();
    }


    public function contact(Request $request)
    {
        $config = Config::findOrFail(1);

        $dictionary = Dictionary::where('keyword', 'email_sent')->where('lang_id', $this->lang)->first();

        $content = $request->except('_token', '_method');

        if(!$request->subject){
            $validator = Validator::make($content, Config::$application_form);

        }else{
            $validator = Validator::make($content, Config::$contactRule);
        }

        if ($validator->fails()) {

            $response = $this->responseArray(0, 'danger', $validator->errors()->first(), null);
            return $this->responseJson($response);
        }
        $email = $config->contact_form_email;


        try {
            Mail::to($email)->send(new ContactForm($content, 'contact', 'İstifadəçidən məktub'));

            $response = $this->responseArray(1, $dictionary->content, null, false, true);
            return $this->responseJson($response);
        } catch (\Exception $e) {
            $response = $this->responseArray(0, $e->getMessage(), null);
            return $this->responseJson($response);
        }
    }

}
