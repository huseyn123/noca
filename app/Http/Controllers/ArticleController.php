<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Crud\ArticleCrud;
use App\Logic\ImageRepository;
use Illuminate\Http\Request;
use App\DataTables\ArticleDataTable;
use App\Models\Article;
use App\Models\Page;
use App\Logic\Slug;
use DB;


class ArticleController extends Controller
{
    private $crud, $image, $requests;
    public $title;

    public function __construct(ArticleCrud $crud, ImageRepository $imageUpload, Request $request)
    {
        parent::__construct();

        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->image = $imageUpload;

        $this->crud = $crud;

        $this->requests = $request->except('_token', '_method','return', 'edit', 'file', 'image', 'lang');

        $this->title = "Xəbərlər";
    }


    public function index(ArticleDataTable $dataTable)
    {

        $cat = $this->search();

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("article.index"));
        return $dataTable->render('app.article.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()], compact('cat'));
    }

    public function trashed(ArticleDataTable $dataTable)
    {
        $cat = $this->search();

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("article.index"));
        $breadcrumb->addCrumb(trans('locale.trash'));

        return $dataTable->trashed(true)->render('app.article.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()], compact('cat'));
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("article.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        $fields = $this->crud->fields(null, 'create');

        return view('app.article.create', ["route" => "article", "breadcrumb" => $breadcrumb->render(), "fields" => $fields]);
    }


    public function store(Request $request)
    {
        //check if it this page exists or not

        if($request->has('lang')){
            $existArticle = Article::join('pages', 'pages.id', '=', 'articles.category_id')->where('pages.lang_id', $request->get('lang'))->where('articles.relation_page', $request->get('relation_page'))->select('articles.id')->first();

            if($existArticle){
                return $this->update($request, $existArticle->id);
            }
        }

        $validator = Validator::make($request->all(), Article::rules(), Article::$messages);

        if ($validator->fails()) {
            $response = $this->responseArray(1, $validator->errors()->first());
            return $this->validateResponse($request, $response);
        }

        $category = Page::findOrFail($request->input('category_id'));

        $this->requests['featured'] = $request->input('featured', 0);
        $this->requests['published_by'] = auth()->user()->first_name.' '.auth()->user()->last_name;
        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('title'), 'article', $category->lang_id );

        DB::beginTransaction();


        try{
            $article = Article::create($this->requests);
        }
        catch(\Exception $e){
            DB::rollback();
            $response = $this->responseArray(1, $e->getMessage());
            return $this->validateResponse($request, $response);
        }


        if(!$request->has('lang')){

            try{
                $this->image->morphStore('file', $article, $this->resize());
            }
            catch(\Exception $e){
                DB::rollback();
                $response = $this->responseArray(1,  $e->getMessage());
                return $this->validateResponse($request, $response);
            }

            try{
                $article->relation_page = $article->id;
                $article->save();
            }
            catch(\Exception $e){
                DB::rollback();
                $response = $this->responseArray(1, $e->getMessage());
                return $this->validateResponse($request, $response);
            }

        }

        if($request->has('return'))
        {
            $request->session()->flash('message', "Xəbər əlavə edildi");
            return redirect()->route('article.index');
        }

        $response = $this->responseArray(0, "Xəbər əlavə edildi", false, false, false, ['slug' => $article->slug, 'uid' => $article->id]);
        return $this->validateResponse($request, $response);

    }



    public function edit($id)
    {
        $fields = [];
        $langs = [];

        $article = Article::findOrFail($id);
        $lang = $article->category->lang_id;

        $relatedArticle = Article::with('category')->where('relation_page', $article->relation_page)->get();

        foreach ($relatedArticle as $rel){
            $fields[$rel->category->lang_id] = $this->crud->fields($rel->category->lang_id, 'edit', $rel);
            $langs[$rel->category->lang_id] = $rel->category->lang_id;
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields($key, 'edit', null);
        }

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("article.index"));
        $breadcrumb->addCrumb($article->title);


        return view('app.article.edit', ['info' => $article, 'lang' => $lang, 'langs' => $langs, "breadcrumb" => $breadcrumb->render(), 'fields' => $fields, 'relatedArticle' => $relatedArticle]);
    }


    public function update(Request $request, $id)
    {
        $find = Article::findOrFail($id);

        $validator = Validator::make($request->all(), Article::rules($find->id), Article::$messages);


        if ($validator->fails()) {

            $response = $this->responseArray(1,  $validator->errors()->first());
            return $this->responseJson($response);
        }

        $category = Page::findOrFail($request->input('category_id'));

        $this->requests['featured'] = $request->input('featured', 0);
        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('title'), 'article', $category->lang_id );


        DB::beginTransaction();


        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            DB::rollback();

            $response = $this->responseArray(1, trans('locale.fail', ['number' => 1]));
            return $this->responseJson($response);
        }

        DB::commit();

        $response = $this->responseArray(0, trans('locale.update_success'), false, false, false, ['slug' => $find->slug]);
        return $this->responseJson($response);

    }


    public function updateImage(Request $request, $id)
    {
        $article = Article::findOrFail($id);

        if($request->hasFile('file')){
            $this->image->morphUpdate('file', $article, $this->resize());
        }

        request()->session()->flash('message', trans('locale.update_success'));
        return redirect()->back();
    }

    public function trash($id)
    {
        $article = Article::findOrFail($id);
        $article->delete();

        request()->session()->flash('message', "Xəbər zibil qutusuna göndərildi");
        return redirect()->route('article.index');
    }


    public function restore($id)
    {
        $article = Article::onlyTrashed()->findOrFail($id);
        $article->restore();

        request()->session()->flash('message', "Xəbər zibil qutusundan qaytarıldı");
        return redirect()->route('article.index');
    }


    public function destroy($id)
    {
        $article = Article::onlyTrashed()->findOrFail($id);

        $relations = Article::where('relation_page', $article->relation_page)->withTrashed()->count();

        DB::beginTransaction();

        if($relations == 1){
            $this->image->morphDelete($article, true);  //delete all img files from directory and database
        }

        $article->forceDelete();

        DB::commit();

        request()->session()->flash('message', "Xəbər birdəfəlik silindi");
        return redirect()->back();
    }


    protected function search()
    {
        if(!request()->ajax())
        {
            $cat = Page::where('template_id', 3)->whereNotNull('parent_id')->orderBy('id')->pluck('name', 'id');
            $cat->prepend('Kateqoriya üzrə', 0);

            return $cat;
        }
    }


    protected function resize()
    {
        $resizeImage = ['resize' => ['fit' => true, 'size' => [845, 500]], 'thumb' => ['fit' => true, 'size' => [360, 280]] ];

        return $resizeImage;
    }



}
