<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Config;
use Illuminate\Support\Facades\File;
use Cache;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin', ['only' => ['store']]);
    }


    public function index()
    {
        $config = Config::findOrFail(1);

        return view('app.brand.index')->with(['title' => 'Kataloq', 'config' => $config]);
    }


    public function store(Request $request)
    {
        $config = Config::findOrFail(1);

        $file = $request->file('brandbook');


        $inputs = array(
            'brandbook' => $file,
        );


        $validator = Validator::make($inputs, Config::$catalogRule, Config::$messages);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $validator->errors()->all()]);
        }


        $name = $file->hashName();
        $directory_path = public_path('storage/catalog');
        File::deleteDirectory($directory_path);
        $path = $file->store('public/catalog');



        if ($file->isValid()) {

            $config->brandbook = $name;

            $config->save();

            Cache::forget('webConfig');

            $request->session()->flash('message', "Fayl yüklənildi");

            return redirect()->route('catalog.index');
        }
    }
}