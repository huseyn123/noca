<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Logic\Order;
use App\Crud\PageCrud;
use App\Logic\ImageRepository;
use App\Models\Page;
use App\Logic\Slug;
use Illuminate\Http\Request;
use App\DataTables\PageDataTable;
use DB;

class PageController extends Controller
{
    private $crud, $image, $requests,$order;
    public $title;

    public function __construct(PageCrud $crud, ImageRepository $coverImage, Request $request, Order $order)
    {
        parent::__construct();

        $this->middleware('admin', ['only' => ['trash', 'order', 'postOrder', 'destroy']]);

        $this->requests = $request->except('_token', '_method', 'return', 'edit', 'apply_module', 'file');
        $this->crud = $crud;
        $this->image = $coverImage;
        $this->order = $order;
        $this->title = "Menyular";

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('pages',false);
        }
    }


    public function index(PageDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);
        return $dataTable->render('app.page.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function trashed(PageDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("page.index"));
        $breadcrumb->addCrumb(trans('locale.trash'));

        return $dataTable->trashed(true)->render('app.page.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("page.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        $fields = $this->crud->fields(null, 'create');

        return view('app.page.create', ["breadcrumb" => $breadcrumb->render(), 'route' => 'page', 'fields' => $fields]);
    }


    public function store(Request $request)
    {
        //check if it this page exists or not

        if($request->has('relation_page')){
            $existPage = Page::where('relation_page', $request->input('relation_page'))->where( 'lang_id', $request->input('lang_id'))->first();

            if($existPage){
                return $this->update($request, $existPage->id);
            }
        }

        $validator = Validator::make($request->all(), Page::rules($request->input('lang_id')), Page::$messages);

        if ($validator->fails()) {
            $response = $this->responseArray(1, $validator->errors()->first());
            return $this->validateResponse($request, $response);
        }

        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('name'), 'page', $request->input('lang_id') );

        DB::beginTransaction();


        try{
            $page = Page::create($this->requests);
        }
        catch(\Exception $e){
            DB::rollback();
            $response = $this->responseArray(1, $e->getMessage());
            return $this->validateResponse($request, $response);
        }


        if(!$request->has('relation_page')){
            try{
                $page->relation_page = $page->id;
                $page->save();
            }
            catch(\Exception $e){
                DB::rollback();
                $response = $this->responseArray(1, $e->getMessage());
                return $this->validateResponse($request, $response);
            }

            if($request->hasFile('file')){
                try{
                    $this->image->morphStore('file', $page, $this->resize());
                }
                catch(\Exception $e){
                    DB::rollback();
                    $response = $this->responseArray(1, $e->getMessage());
                    return $this->validateResponse($request, $response);
                }
            }
        }


        DB::commit();



        if($request->has('return'))
        {
            $request->session()->flash('message', "Səhifə əlavə edildi");
            return redirect()->route('page.index');
        }

        $response = $this->responseArray(0, "Səhifə əlavə edildi", false, false, false, ['slug' => $page->slug, 'uid' => $page->id]);
        return $this->validateResponse($request, $response);
    }


    public function edit($id)
    {
        $fields = [];
        $langs = [];
        $keys = [];

        $page = Page::findOrFail($id);

        $relatedPage = Page::where('relation_page', $page->relation_page)->get();

        foreach ($relatedPage as $rel){
            $fields[$rel->lang_id] = $this->crud->fields($rel->lang_id, 'edit', $rel);
            $langs[$rel->lang_id] = $rel->lang_id;
            $keys[$rel->lang_id] = array_filter(explode(",", $rel->meta_keywords));
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields( $key, 'edit', null);
            $keys[$key] = [];
        }

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("page.index"));
        $breadcrumb->addCrumb($page->name);

        return view('app.page.edit', ['info' => $page, 'langs' => $langs, "breadcrumb" => $breadcrumb->render(), 'fields' => $fields, 'relatedPage' => $relatedPage, 'keys' => $keys, 'route' => 'page']);
    }


    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);


        $validator = Validator::make($request->all(), Page::rules($request->input('lang_id'), $id), Page::$messages);

        if ($validator->fails()) {

            $response = $this->responseArray(1, 'danger',  $validator->errors()->first());
            return $this->responseJson($response);
        }


        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('name'), 'page', $request->input('lang_id') );

        if(!$request->input('meta_keywords')){
            $this->requests['meta_keywords'] = null;
        }

        DB::Begintransaction();

        try{
            foreach($this->requests as $key => $put){
                $page->$key = $put;
            }

            $page->save();
        }
        catch(\Exception $e){

            DB::rollback();

            $response = $this->responseArray(1, trans('locale.fail', ['number' => 1]));
            return $this->responseJson($response);
        }


        DB::commit();

        $response = $this->responseArray(0, trans('locale.update_success'), false, false, false, ['slug' => $page->slug]);
        return $this->responseJson($response);
    }


    public function updateImage(Request $request, $id)
    {
        $page = Page::findOrFail($id);

        if($request->hasFile('file')){
            $this->image->morphUpdate('file', $page, $this->resize());
        }

        request()->session()->flash('message', trans('locale.update_success'));
        return redirect()->back();
    }


    public function delImage($id)
    {
        $page = Page::findOrFail($id);

        $this->image->morphDelete($page);

        request()->session()->flash('message', "Şəkil silindi");
        return redirect()->back();
    }


    public function trash($id)
    {
        $category = Page::findOrFail($id);
        $category->delete();

        request()->session()->flash('message', "Səhifə zibil qutusuna göndərildi");
        return redirect()->back();
    }


    public function restore($id)
    {
        $category = Page::onlyTrashed()->findOrFail($id);
        $category->restore();

        request()->session()->flash('message', "Səhifə zibil qutusundan qaytarıldı");
        return redirect()->back();
    }


    public function destroy($id)
    {
        $page = Page::onlyTrashed()->findOrFail($id);

        $relations = Page::where('relation_page', $page->relation_page)->withTrashed()->count();

        DB::beginTransaction();

        if($relations == 1){
            $this->image->morphDelete($page, true);  //delete all img files from directory and database
        }

        $page->forceDelete();

        DB::commit();


        request()->session()->flash('message', "Səhifə birdəfəlik silindi");
        return redirect()->back();
    }

    public function order()
    {
        $lang = request()->get('lang', 'az');

        return $this->order->get('page', $this->title, 'name', $lang, 6);
    }

   public function postOrder(Request $request)
    {
        return $this->order->post($request, 'page', true);

    }

    protected function resize()
    {
        $resizeImage = ['resize' => ['fit' => true, 'size' => [1920, 240]], 'thumb' => ['fit' => false, 'size' => [400, null]] ];

        return $resizeImage;
    }
}
