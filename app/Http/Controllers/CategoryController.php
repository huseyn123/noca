<?php

namespace App\Http\Controllers;
use App\Crud\CategoryCrud;
use Illuminate\Support\Facades\Validator;
use App\Logic\ImageRepository;
use App\Models\Page;
use Illuminate\Http\Request;
use App\DataTables\CategoryDataTable;
use App\Logic\Slug;
use DB;

class CategoryController extends Controller
{
    private $crud, $image, $requests;
    public $title;

    public function __construct(CategoryCrud $crud, ImageRepository $imageUpload, Request $request)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->image = $imageUpload;
        $this->crud = $crud;
        $this->title = 'Alt menyular';
        $this->requests = $request->except('_token', '_method', 'return', 'edit', 'file', 'image_file');


        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('pages',false);
        }
    }


    public function index(CategoryDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);
        return $dataTable->render('app.category.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function trashed(CategoryDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("category.index"));
        $breadcrumb->addCrumb(trans('locale.trash'));
        return $dataTable->trashed(true)->render('app.category.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("category.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        $fields = $this->crud->fields(null,'create');

        return view('app.page.create',  ["fields" => $fields,  "route" => "category", "breadcrumb" => $breadcrumb->render()]);
    }


    public function store(Request $request)
    {
        //check if it this page exists or not

        if ($request->has('relation_page')) {
            $existPage = Page::where('relation_page', $request->input('relation_page'))->where('lang_id', $request->input('lang_id'))->first();

            if ($existPage) {
                return $this->update($request, $existPage->id);
            }
        }

        $category = Page::find($request->input('parent_id'));

        if(!$category){
            $response = $this->responseArray(1, "Kateqoriya seçilməyib");
            return $this->validateResponse($request, $response);
        }

        $validator = Validator::make($request->all(), Page::categoryRules($category->lang_id), Page::$messages);

        if ($validator->fails()) {
            $response = $this->responseArray(1, $validator->errors()->first());
            return $this->validateResponse($request, $response);
        }


        $this->requests['lang_id'] = $category->lang_id;
        $this->requests['parent_id'] = $category->id;
        $this->requests['order'] = Page::max('order') + 1;
        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('name'), 'page', $category->lang_id);


        DB::beginTransaction();

        try {

            $page = Page::create($this->requests);
        } catch (\Exception $e) {
            DB::rollback();
            $response = $this->responseArray(1, $e->getMessage());
            return $this->validateResponse($request, $response);
        }


        if (!$request->has('relation_page')) {
            try {
                $page->relation_page = $page->id;
            } catch (\Exception $e) {
                DB::rollback();
                $response = $this->responseArray(1, $e->getMessage());
                return $this->validateResponse($request, $response);
            }

            if($request->hasFile('file')){
                try{
                    $this->image->morphStore('file', $page, $this->resize($page->template_id));
                }
                catch(\Exception $e){
                    DB::rollback();
                    $response = $this->responseArray(1, $e->getMessage());
                    return $this->validateResponse($request, $response);
                }
            }

            if($request->hasFile('image_file')){
                $upload = $this->image->store('image_file', null);

                $page->image_file = $upload[1];
            }
        }

        $page->save();


        DB::commit();

        if($request->has('return'))
        {
            $request->session()->flash('message', "Səhifə əlavə edildi");
            return redirect()->route('category.index');
        }

        $response = $this->responseArray(0, "Səhifə əlavə edildi", false, false, false, ['slug' => $page->slug, 'uid' => $page->id]);
        return $this->validateResponse($request, $response);

    }


    public function edit($id) //this is relation page
    {

        $fields = [];
        $langs = [];
        $keys = [];

        $page = Page::findOrFail($id);

        $relatedPage = Page::where('relation_page', $page->relation_page)->get();

        foreach ($relatedPage as $rel){
            $fields[$rel->lang_id] = $this->crud->fields($rel->lang_id, 'edit', $rel);
            $langs[$rel->lang_id] = $rel->lang_id;
            $keys[$rel->lang_id] = array_filter(explode(",", $rel->meta_keywords));
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields($key, 'edit', null);
            $keys[$key] = [];
        }


        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("category.index"));
        $breadcrumb->addCrumb($page->name);


        return view('app.page.edit', ['info' => $page, 'langs' => $langs, "breadcrumb" => $breadcrumb->render(), 'fields' => $fields, 'relatedPage' => $relatedPage, 'keys' => $keys, 'route' => 'category']);
    }


    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);

        $category = Page::find( $request->input('parent_id'));

        $validator = Validator::make($request->all(), Page::categoryRules($category->lang_id, $id), Page::$messages);


        if ($validator->fails()) {
            $response = $this->responseArray(1, $e->getMessage());
            return $this->responseJson($response);
        }


        $this->requests['lang_id'] = $category->lang_id;
        $this->requests['parent_id'] = $category->id;
        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('name'), 'page', $category->lang_id);
        if(!$request->input('meta_keywords')){
            $this->requests['meta_keywords'] = null;
        }

        try{
            if($request->hasFile('image_file')){

                $upload = $this->image->store('image_file', null, $page->image_file);

                $this->requests['image_file'] = $upload[1];  //generated hashname with path:   2018-01-01/a5as45sa4d54sad4sadddasd.jpg
            }

            foreach($this->requests as $key => $put){
                $page->$key = $put;
            }

            $page->save();
        }
        catch(\Exception $e){
            $response = $this->responseArray(1, $e->getMessage());
            return $this->responseJson($response);
        }

        $response = $this->responseArray(0, trans('locale.update_success'),  false, false, false, ['slug' => $page->slug]);
        return $this->responseJson($response);
    }


    public function updateImage(Request $request, $id)
    {
        $page = Page::findOrFail($id);

        if($request->hasFile('file')){

            $this->image->morphUpdate('file', $page, $this->resize($page->template_id));
        }

        request()->session()->flash('message', trans('locale.update_success'));
        return redirect()->back();
    }


    public function trash($id)
    {
        $category = Page::findOrFail($id);
        $category->delete();

        request()->session()->flash('message', "Səhifə silindi. Qaytarılması mümkündür.");
        return redirect()->route('category.index');
    }


    protected function resize($template_id)
    {
        if($template_id == 8){
            $resizeImage = ['resize' => ['fit' => true, 'size' => [850, 280]], 'thumb' => ['fit' => true, 'size' => [550, 300]] ];
        }
        elseif($template_id == 10){
            $resizeImage = ['resize' => ['fit' => false, 'size' => [55, null]], 'thumb' => null ];
        }
        else{
            $resizeImage = ['resize' => ['fit' => false, 'size' => [850, null]], 'thumb' => ['fit' => false, 'size' => [400, null]] ];
        }

        return $resizeImage;
    }
}
