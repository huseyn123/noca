<?php

namespace App\Http\Controllers;

use App\Crud\SocialCrud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Social;
use Cache;
use Route;
class SocialController extends Controller
{
    protected $input;
    public $title;

    public function __construct(Request $request, SocialCrud $crud)
    {
        $this->middleware('admin', ['except' => ['index']]);

        $this->crud = $crud;
        $this->title = "Sosial şəbəkələr";

        if (in_array(strtolower($request->method()), ['put', 'patch', 'post', 'delete'])) {
            Cache::forget('social', false);
        }
    }

    public function index()
    {


        $social = Social::orderBy('id', 'asc')->get();

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb('Tənzimləmələr', route('config.show', 1));
        $breadcrumb->addCrumb($this->title);

        return view('app.settings.social.index', ["breadcrumb" => $breadcrumb->render(), "title" => $this->title, 'social' => $social]);
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('social.index'));
        $breadcrumb->addCrumb(trans('locale.create'), route("social.create"));

        $fields = $this->crud->fields('create');

        return view('app.settings.social.create',['fields' =>$fields]);
    }


    public function store(Request $request)
    {

        $inputs = [
            'type' => $request->input('type'),
            'forward_url' => $request->input('forward_url'),
        ];

        $rules = array(
            'type' => "required",
            'forward_url' => "required"
        );

        $validator = Validator::make($inputs, $rules);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $validator->errors()->first()]);
        }


        Social::create($inputs);


        $request->session()->flash('message', trans('locale.store_success'));

        return redirect()->back();
    }


    public function edit($id)
    {
        $social = Social::findOrFail($id);

        $fields = $this->crud->fields('edit', $social);

        return view('app.settings.social.edit', ['info' => $social, 'title' => $social->type, 'id' => $id,'fields' => $fields ]);
    }


    public function update(Request $request, $id)
    {
        $social = Social::findOrFail($id);


        $inputs = [
            'type' => $request->input('type'),
            'forward_url' => $request->input('forward_url')
        ];

        $rules = array(
            'type' => "required",
            'forward_url' => "required"
        );

        $validator = Validator::make($inputs, $rules);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $validator->errors()->first()]);
        }

        foreach($inputs as $key => $put){
            $social->$key = $put;
        }

        $social->save();

        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->back();
    }



    public function destroy($id)
    {
        $category = Social::findOrFail($id);
        $category->delete();

        request()->session()->flash('message', trans('locale.store_success'));
        return redirect()->back();
    }
}
