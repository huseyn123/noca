<?php

namespace App\Http\Controllers;

use App\Crud\MainProductCrud;
use Illuminate\Http\Request;
use App\Models\MainProduct;
use Cache;

class MainProductController extends Controller
{
    private $crud, $requests;

    public function __construct(Request $request, MainProductCrud $crud)
    {
        $this->middleware('admin', ['only' => ['edit', 'update']]);

        $this->requests = $request->except('_token', '_method');
        $this->crud = $crud;


        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            Cache::forget('webConfig', false);
        }
    }

    public function show($id)
    {
        $main_product = MainProduct::findOrFail($id);

        $fields = $this->crud->fields('show', $main_product);


        $title = "Müəssisə";
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb('Tənzimləmələr', route('main_product.show', 1));
        $breadcrumb->addCrumb($title);

        return view('app.settings.main_product.show', ["fields" => $fields, "breadcrumb" => $breadcrumb->render(), "title" => $title, 'show' => $main_product, 'id' => $id]);
    }


    public function edit($id)
    {
        $main_product = MainProduct::findOrFail($id);

        $fields = $this->crud->fields('edit', $main_product);

        return view('app.settings.main_product.edit', ["fields" => $fields, 'info' => $main_product, 'title' => trans('locale.edit'), 'id' => $id]);
    }

    public function update(Request $request, $id)
    {
        $main_product = MainProduct::findOrFail($id);

        $request->validate(MainProduct::$rules,MainProduct::$messages);

        foreach($this->requests as $key => $put){
            $main_product->$key = $put;
        }

        $main_product->save();

        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->back();
    }
}
