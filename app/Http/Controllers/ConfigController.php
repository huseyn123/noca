<?php

namespace App\Http\Controllers;

use App\Crud\ConfigCrud;
use Illuminate\Http\Request;
use App\Models\Config;
use App\Models\Page;
use Cache;

class ConfigController extends Controller
{
    private $crud, $requests;

    public function __construct(Request $request, ConfigCrud $crud)
    {
        $this->middleware('admin', ['only' => ['edit', 'update']]);

        $this->requests = $request->except('_token', '_method');
        $this->crud = $crud;


        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            Cache::forget('webConfig', false);
        }
    }

    public function show($id)
    {
        $config = Config::findOrFail($id);

        $fields = $this->crud->fields('show', $config);

        $title = "Konfiqurasiya";
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb('Tənzimləmələr', route('config.show', 1));
        $breadcrumb->addCrumb($title);

        return view('app.settings.config.show', ["fields" => $fields, "breadcrumb" => $breadcrumb->render(), "title" => $title, 'show' => $config, 'id' => $id]);
    }


    public function edit($id)
    {
        $config = Config::findOrFail($id);

        $fields = $this->crud->fields('edit', $config);

        return view('app.settings.config.edit', ["fields" => $fields, 'info' => $config, 'title' => trans('locale.edit'), 'id' => $id]);
    }

    public function update(Request $request, $id)
    {
        $config = Config::findOrFail($id);

        $request->validate(Config::$rules);

        foreach($this->requests as $key => $put){
            $config->$key = $put;
        }

        $config->save();

        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->back();
    }
}
