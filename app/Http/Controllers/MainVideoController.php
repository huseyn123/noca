<?php

namespace App\Http\Controllers;

use App\Crud\MainVideoCrud;
use App\Logic\ImageRepository;
use Illuminate\Http\Request;
use App\Models\Config;
use Illuminate\Support\Facades\Storage;
use App\Models\Page;
use Cache;

class MainVideoController extends Controller
{
    private $crud, $image, $requests;

    public function __construct(Request $request, MainVideoCrud $crud,ImageRepository $imageUpload)
    {
        $this->middleware('admin', ['only' => ['edit', 'update']]);

        $this->requests = $request->except('_token', '_method');
        $this->crud = $crud;
        $this->image = $imageUpload;


        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            Cache::forget('webConfig', false);
        }
    }

    public function show($id)
    {
        $config = Config::findOrFail($id);

        $fields = $this->crud->fields('show', $config);

//        return $fields;

        $title = "Video";
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb('Video', route('main_video.show', 1));
        $breadcrumb->addCrumb($title);

        return view('app.settings.main_video.show', ["fields" => $fields, "breadcrumb" => $breadcrumb->render(), "title" => $title, 'show' => $config, 'id' => $id]);
    }


    public function edit($id)
    {
        $config = Config::findOrFail($id);

        $fields = $this->crud->fields('edit', $config);

        return view('app.settings.main_video.edit', ["fields" => $fields, 'info' => $config, 'title' => trans('locale.edit'), 'id' => $id]);
    }

    public function update(Request $request, $id)
    {
        $config = Config::findOrFail($id);

        $request->validate(Config::$videoRule);

        if($request->has('main_video_cover')){

            $upload = $this->image->store('main_video_cover', $this->resize());

            $this->requests['main_video_cover'] = $upload[1];
        }

        foreach($this->requests as $key => $put){
            $config->$key = $put;
        }

        $config->save();

        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->back();
    }

    protected function resize()
    {

        $resizeImage = ['resize' => ['fit' => true, 'size' => [460, 280]], 'thumb' => ['fit' => false, 'size' => [330, null]] ];

        return $resizeImage;
    }
}







