<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Logic\ImageRepository;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    private $image;

    public function __construct(Request $request, ImageRepository $imageRepository)
    {
        $this->image = $imageRepository;

    }

    public function store(Request $request)
    {
        $id = $request->input('id');
        $route = config('config.model.'.$request->input('route'));

        $model = $route::findOrFail($id);

        $response = $this->image->dropzone('file', $model, $this->resize());
        return $response;
    }


    public function show(Request $request, $id)
    {
        $route = config('config.model.'.$request->get('route'));

        $model = $route::findOrFail($id);

        $images = $model->galleries;

        $imageAnswer = [];

        foreach ($images as $image) {
            $imageAnswer[] = [
                'id' => $image->id,
                'original' => $image->original_filename,
                'server' => $image->filename,
                'size' => Storage::size('public/'.$image->filename)
            ];
        }

        return response()->json([
            'images' => $imageAnswer
        ]);
    }


    public function destroy(Request $request, $modelId)
    {
        $id = $request->input('id');

        $response = $this->image->dropzoneDelete($id);

        return $response;
    }


    public function getDownload($filename)
    {
        $file= public_path(). "/public/storage.$filename";

        $headers = array(
            'Content-Type: image/jpg',
        );

        return response()->download($file, $filename, $headers);
    }


    protected function resize()
    {
        $resizeImage = ['resize' => ['fit' => false, 'size' => [480, null]], 'thumb' => ['fit' => true, 'size' => [260, 185]] ];

        return $resizeImage;
    }
}