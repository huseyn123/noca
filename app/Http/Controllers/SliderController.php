<?php

namespace App\Http\Controllers;

use App\Logic\ImageRepository;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\DataTables\SliderDataTable;
use App\Crud\SliderCrud;
use App\Logic\Order;
use DB;

class SliderController extends Controller
{
    private $crud, $route, $image, $requests,$order;
    public $title;

    public function __construct(Request $request, ImageRepository $imageUpload, SliderCrud $crud,Order $order)
    {
        parent::__construct();

        $this->middleware('admin', ['only' => ['trash', 'destroy','order', 'postOrder']]);

        $this->image = $imageUpload;
        $this->crud = $crud;
        $this->order = $order;

        $this->requests = $request->except('_token', '_method', 'file');
        $this->title = "Slider";
        $this->route = 'slider';

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('slider');
        }
    }



    public function index(SliderDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);

        return $dataTable->render("app.$this->route.index", ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }

    public function trashed(SliderDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));
        $breadcrumb->addCrumb(trans('locale.trash'));
        return $dataTable->trashed(true)->render("app.$this->route.index",  ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }

    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("slider.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        $fields = $this->crud->fields('create');

        return view("app.$this->route.create",  ["breadcrumb" => $breadcrumb->render(), 'fields' => $fields]);
    }

    public function store(Request $request)
    {
        $request->validate(Slider::rules(), Slider::$messages);


        DB::beginTransaction();

        $upload = $this->image->store('file', $this->resize());

        $this->requests['image'] = $upload[1];  //generated hashname with path:   2018-01-01/a5as45sa4d54sad4sadddasd.jpg

        try{
            Slider::create($this->requests);
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
        }

        DB::commit();


        $request->session()->flash('message', "Slider əlavə olundu");

        return redirect()->route("$this->route.index");
    }

    public function edit($id)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("$this->route.index"));
        $breadcrumb->addCrumb(trans('locale.edit'));

        $slider = Slider::findOrFail($id);

        $fields = $this->crud->fields('edit',$slider);


        return view('app.slider.edit', ['fields' => $fields, 'info' => $slider, 'breadcrumb' => $breadcrumb->render()]);
    }

    public function update(Request $request, $id)
    {
        $slider = Slider::findOrFail($id);

        $request->validate(Slider::rules(null), Slider::$messages);


        if($request->hasFile('file')){

            $upload = $this->image->store('file', $this->resize(), $slider->image);

            $this->requests['image'] = $upload[1];  //generated hashname with path:   2018-01-01/a5as45sa4d54sad4sadddasd.jpg
        }


        foreach($this->requests as $key => $put){
            $slider->$key = $put;
        }

        $slider->save();


        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->route("$this->route.index");
    }

    public function order()
    {
        $lang = request()->get('lang', 'az');

        return $this->order->get('slider', $this->title, 'title', $lang, 1);
    }

    public function postOrder(Request $request)
    {
        return $this->order->post($request, 'slider', false);
    }

    public function trash($id)
    {
        $member = Slider::findOrFail($id);
        $member->delete();

        request()->session()->flash('message', "Slider silindi. Qaytarılması mümkündür.");
        return redirect()->back();
    }

    public function restore($id)
    {
        $member = Slider::onlyTrashed()->findOrFail($id);
        $member->restore();

        request()->session()->flash('message', "Silinmiş slider qaytarıldı və aktiv olundu");
        return redirect()->route("$this->route.index");
    }

    public function destroy($id)
    {
        $slider = Slider::onlyTrashed()->findOrFail($id);
        $slider->forceDelete();

        $this->image->deleteFile($slider->image);

        request()->session()->flash('message', "Slider birdəfəlik silindi");
        return redirect()->back();
    }


    protected function resize()
    {
        $resizeImage = ['resize' => ['fit' => true, 'size' => [1350, 500]], 'thumb' => ['fit' => true, 'size' => [300, 130]] ];

        return $resizeImage;
    }
}
