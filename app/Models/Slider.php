<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Slider extends Model
{
    use SoftDeletes;

    protected $table = 'sliders';

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public static function rules($required = 'required|'){
        return [
            'title' => 'nullable',
            'file' => $required.'mimes:jpeg,jpg,png|max:10000|dimensions:min_width=1350,min_height=500',
            'lang_id' => 'required',
            'link' => 'nullable'
        ];
    }


    public static $messages = [
        'file.required' => "Video faylı əlavə edilməyib.",
    ];

    //for reorder//
    public function buildSlider($items)
    {
        $result = null;

        foreach ($items as $item)
        {
            $result .= "
            <li class='dd-item nested-list-item' data-order='{$item->order}' data-id='{$item->id}'>
                <div class='dd-handle nested-list-handle'>
                    <span class='glyphicon glyphicon-move'></span>
                </div>
                <div class='nested-list-content'>{$item->id}) {$item->title}</div>
            </li>";
        }

        return $result ?  "\n<ol class=\"dd-list\">\n$result</ol>\n" : null;
    }



    public function getSlider($items)
    {
        return $this->buildSlider($items);
    }

}
