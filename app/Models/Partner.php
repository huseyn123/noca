<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partner extends Model
{
    use SoftDeletes;

    protected $table = 'partners';

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public static function rules($required = 'required|')
    {
        return [
            'file' => $required . '|mimes:jpeg,jpg,png|max:10000',
            'site_url' => 'nullable',
        ];
    }

    public static $messages = [
        'file.required' => "Loqotip əlavə olunmayıb."
    ];



    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }
}
