<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'configs';

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];


    public static $rules = [
            'company_name' => 'required',
            'email' => 'required|email',
            'contact_form_email' => 'nullable|email',
            'app_form_email' => 'nullable|email',
            'site_url' => 'required',
            'location' => 'nullable',
            'contact_phone' => 'nullable',
            'main_video' => 'nullable',
            'lang_id' => 'required',
            'google_api_key' => 'nullable',
            'analytic_id' => 'nullable',
            'box_first_url' => 'nullable',
            'box_second_url' => 'nullable',
            'box_third_url' => 'nullable'
    ];


    public static $videoRule = [
        'main_video' => 'required'
    ];




    public static $application_form = [
        'fullname' => 'required|min:3',
        'phone' => 'required|numeric',
        'message' => 'required',
    ];



    public static $contactRule = [
        'fullname' => 'required|min:3',
        'email' => 'nullable|email',
        'phone' => 'required',
        'subject' => 'required|min:3',
        'message' => 'required',
    ];


    public static $messages = [
        'brandbook.required' => 'Fayl seçilməyib',
        'brandbook.mimes' => 'Kataloq :values formatda yüklənməlidir',
    ];

}
