<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Dictionary extends Model
{
    protected $table = 'dictionaries';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];

    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public static $rules = [
        'keyword' => 'required|string',
        'content' => 'nullable',
    ];


    public static $messages = [
        'keyword.required' => 'Açar sözü düzgün yazın',
    ];

}
