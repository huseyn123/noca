<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use DB;
class Article extends Model
{
    use SoftDeletes;

    protected $table = 'articles';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public static function rules($id = null){

        return [
            'title' => "required",
            'slug' => 'unique:articles,slug,'.$id,
            'file' => 'required_without:lang|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=845,min_height=500',
            'category_id' => 'required|numeric',
            'published_at' => 'required|date',
            'status' => 'boolean',
            'featured' => 'boolean',
            'summary' => 'nullable',
            'content' => 'nullable',
            'youtube_id' => 'nullable',
        ];
    }

    public static $messages = [
        'file.required_without' => "Şəkil əlavə olunmayıb",
    ];


    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function page()
    {
        return $this->belongsTo('App\Models\Page', 'category_id');
    }


    public function scopeCondition($query, $keyword)
    {
        if($keyword == 1){
            return $query->where(function ($query) use ($keyword){
                $query->orWhere('articles.main_category_id', $keyword)
                    ->orWhere('articles.category_id', $keyword);
            });
        }
    }


    public function scopeSelectByYear($query, $year)
    {
        if(!is_null($year)){
            return $query->where(DB::raw('YEAR(articles.published_at)'), $year);
        }
        else{
            return $query;
        }
    }


    public function scopeSelectByMonth($query, $month)
    {
        if(!is_null($month)){
            return $query->where(DB::raw('MONTH(articles.published_at)'), $month);
        }
        else{
            return $query;
        }
    }


    public function category()
    {
        return $this->hasOne('App\Models\Page', 'id', 'category_id')->select('id','name', 'lang_id', 'relation_page', 'slug');
    }


    public function images()
    {
        return $this->morphMany('App\Models\Gallery', 'gallery', 'gallery_type', 'gallery_id', 'relation_page');
    }

    public function image()
    {
        return $this->morphOne('App\Models\Gallery', 'gallery', 'gallery_type', 'gallery_id', 'relation_page')->where('main', 1);
    }

    public function galleries()
    {
        return $this->morphMany('App\Models\Gallery', 'gallery', 'gallery_type', 'gallery_id', 'relation_page')->where('main', 0);
    }

    public function gallery()   //dropzone delete function
    {
        return $this->hasOne('App\Models\Gallery', 'relation_page');
    }

    public function relations()
    {
        return $this->morphMany('App\Models\Gallery', 'gallery');
    }

}

