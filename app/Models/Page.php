<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Page extends Model
{
    use SoftDeletes;

    protected $table = 'pages';
    protected $guarded = ['id'];
    protected $hidden =  ['_token'];
    protected $dates =   ['created_at', 'updated_at', 'deleted_at'];

    public static function rules($lang, $id = null){

        return [
            'template_id' => 'required|numeric',
            'name' => 'required|max:255',
            'slug' => 'unique:pages,slug,'.$id.',id,lang_id,'.$lang,
            'file' => 'nullable|mimes:jpeg,jpg,png|max:10000',
            'main_image' => 'nullable|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=811,min_height=446',
            'image_file' => 'nullable',
            'description' => 'nullable',
            'summary' => 'nullable',
            'has_slider' => 'boolean',
            'has_partner' => 'boolean',
            'content' => 'nullable',
            'content_extra' => 'nullable',
            'forward_url' => 'nullable',
            'order' => 'numeric',
            'target' => 'boolean',
            'visible' => 'numeric',
            'youtube_id' => 'nullable',
            'featured' => 'boolean',
            'icon' => 'nullable',
            'meta_description' => 'nullable|max:160',
        ];
    }



    public static function categoryRules($lang, $id = null){

        return [
            'name' => 'required|max:255',
            'slug' => 'unique:pages,slug,'.$id.',id,lang_id,'.$lang,
            'file' => 'nullable|mimes:jpeg,jpg,png|max:10000',
            'main_image' => 'nullable|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=811,min_height=446',
            'image_file' => 'nullable',
            'description' => 'nullable',
            'summary' => 'nullable',
            'has_slider' => 'boolean',
            'has_partner' => 'boolean',
            'content' => 'nullable',
            'content_extra' => 'nullable',
            'forward_url' => 'nullable',
            'order' => 'numeric',
            'target' => 'boolean',
            'visible' => 'numeric',
            'youtube_id' => 'nullable',
            'featured' => 'boolean',
            'icon' => 'nullable',
            'meta_description' => 'nullable|max:160',
        ];
    }


    public static function productRules($lang, $id = null){

        return [
            'name' => 'required|max:255',
            'slug' => 'unique:pages,slug,'.$id.',id,lang_id,'.$lang,
            'file' => 'required_without:lang_id|mimes:jpeg,jpg,png|max:10000|dimensions:min_width=150,min_height=150',
            'image_file' => 'nullable|mimes:pdf|max:10000',
            'description' => 'nullable',
            'summary' => 'nullable',
            'content' => 'nullable',
            'forward_url' => 'nullable',
            'order' => 'numeric',
            'target' => 'boolean',
            'visible' => 'numeric',
            'meta_description' => 'nullable|max:160',
        ];
    }


    public static function productİnfoRules($lang, $id = null){

        return [
            'name' => 'required|max:255',
            'slug' => 'unique:pages,slug,'.$id.',id,lang_id,'.$lang,
            'parent_id' => 'required|numeric',
            'content' => 'nullable',

        ];
    }

    public static $messages = [
        'file.required_without' => 'Şəkil yüklənməyib'
    ];

    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function setMetaKeywordsAttribute($value) {

        $keywords = null;

        if(!is_null($value) && is_array($value))
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }

    public function getPage($items)
    {
        return $this->buildPage($items);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc');
    }

    public function scopeVisible($query)
    {
        return $query->where('visible', '<>', 0);
    }

    public function articles($paginate = 9)
    {
        return $this->hasMany('App\Models\Article', 'category_id', 'id')
            ->join('pages', 'pages.id', '=', 'articles.category_id')
            ->with('image:id,filename,gallery_id')
            ->select('articles.id', 'articles.relation_page', 'articles.title', 'articles.slug', 'articles.summary', 'articles.content', 'articles.published_at')
            ->where('articles.status', 1)
            ->whereNull('pages.deleted_at')
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc')
            ->paginate($paginate);
    }

    public function otherArticles($id)
    {
        return $this->hasMany('App\Models\Article', 'category_id', 'id')
            ->join('pages', 'pages.id', '=', 'articles.category_id')
            ->select('articles.title', 'articles.slug as article_slug', 'articles.summary', 'articles.published_at', 'pages.slug')
            ->where('articles.id', '<>', $id)
            ->where('articles.status', 1)
            ->whereNull('pages.deleted_at')
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc')
            ->limit(4);
    }

    public function children()
    {
        return $this->hasMany('App\Models\Page', 'parent_id', 'id')
            ->whereNotNull('parent_id')
            ->select('id', 'name', 'slug', 'description','content', 'template_id', 'relation_page', 'summary')
            ->visible()
            ->ordered();
    }

    public function sortImage()
    {
        return $this->hasOne('App\Models\Sort', 'id', 'tag_id');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Gallery', 'gallery', 'gallery_type', 'gallery_id', 'relation_page');
    }

    public function image()
    {
        return $this->morphOne('App\Models\Gallery', 'gallery', 'gallery_type', 'gallery_id', 'relation_page')->where('main', 1);
    }

    public function galleries()
    {
        return $this->morphMany('App\Models\Gallery', 'gallery', 'gallery_type', 'gallery_id', 'relation_page')->where('main', 0);
    }

    public function gallery()   //dropzone delete function
    {
        return $this->hasOne('App\Models\Gallery', 'relation_page');
    }

    public function relations()
    {
        return $this->morphMany('App\Models\Gallery', 'gallery');
    }

}
