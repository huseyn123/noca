<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $guarded =    ['id'];
    protected $hidden =     ['_token'];
    protected $dates = ['created_at', 'updated_at'];


    public static $rules = [
        'file' => 'required|mimes:png,gif,jpeg,jpg,bmp|max:10000|dimensions:min_width=220,min_height=165'
    ];
    public static $messages = [
        'file.required' =>  "Şəkil əlavə olunmayıb",
        'file.mimes' => 'Şəkil :values formatda yüklənməlidir',
    ];


    public function relation()
    {
        return $this->morphTo();
    }
}
