<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainProduct extends Model
{
    protected $table = 'main_product';

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];


    public static $rules = [
            'company_count' => 'required',
            'brand_count' => 'required',
            'product_count' => 'required',
    ];



    public static $messages = [
        'company_count.required' => 'Müəssisə sayı daxil edilməyib',
        'brand_count.required' => 'Marka sayı daxil edilməyib',
        'product_count.required' => 'Məhsul sayı daxil edilməyib',
    ];

}
