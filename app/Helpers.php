<?php
/**
 * Created by PhpStorm.
 * User: Riko
 * Date: 8/16/2016
 * Time: 5:12 PM
 */

use Carbon\Carbon;
use Jenssegers\Date\Date;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Logic\WebCache;

function webConfig()
{
    $config = WebCache::getConfig();

    return $config;
}


function headers(){
    return array('Content-type'=> 'application/json; charset=utf-8');
}


function activeUrl($url, $output = 'class=active')
{
    if(request()->url() == $url){
        return $output;
    }
}


function activeQuery($url, $output = 'class=active')
{
    if(request()->url().'?'.request()->getQueryString() == $url){
        return $output;
    }
}

function icon($class, $title = null){
    return "<i class='fa fa-$class'></i> $title";
}

function g_icon($class, $title = null, $spinning = null){
    return "<i class='fa fa-circle-o-notch fa-spin'></i>";
}

function filterDate($date, $time = false)
{
    $time == true ? $timeDate = ', '.with(new Carbon($date))->format('H:i') : $timeDate = '';

    if($date != null){
        $date = with(new Carbon($date))->format('Y-m-d');

        if($date == Carbon::today()->format('Y-m-d')) {
            return 'Bu gün'.$timeDate;
        }
        else if($date == Carbon::yesterday()->format('Y-m-d')) {
            return 'Dünən'.$timeDate;
        }
        else{
            return with(new Carbon($date))->format('d/m/Y').$timeDate;
        }
    }
    else{
        return '';
    }

}

function filterWebDate($date)
{
    return Carbon::parse($date)->format('d/m/Y');
}

function blogDate($date)
{
    Date::setLocale(LaravelLocalization::getCurrentLocale());

    $date =  Date::parse($date)->format('d F Y');

    list($day, $month, $year) = explode(' ', $date);

    return $day.' '.$month.' '.$year;
}

function mediaFullDate($date)
{
    //Date::setLocale(LaravelLocalization::getCurrentLocale());

    //return Date::parse($date)->format('d F Y');
    //return Carbon::createFromTimeStamp(strtotime($date))->formatLocalized('%d %B %Y');

    return Carbon::parse($date)->format('d.m.Y');

}


function label($class, $title)
{
    return '<span class="label label-'.$class.'">'.$title.'</span>';
}


function switchLanguage($locale)
{
    $uri = request()->segments();

    $uri[0] = $locale;

    return implode($uri, '/');
}



function menuRoute($cat, $count = 0)
{
    if($cat->slug == 'index'){
        return route('home');
    }
    else{
        if($count == 0)
        {
            return route('showPage', $cat->slug);
        }
        else{
            return "javascript:void(0)";
        }
    }
}



function ucfirst_utf8($str)
{
    return mb_substr(mb_strtoupper($str, 'utf-8'), 0, 1, 'utf-8') . mb_substr($str, 1, mb_strlen($str)-1, 'utf-8');
}


function upperTitle($title, $lang)
{
    if($lang == 'az'){
        $title = str_replace("i", "İ", $title);
    }

    return mb_strtoupper($title, 'utf-8');
}


function normalTitle($title)
{
    return ucfirst_utf8(mb_strtolower($title, 'utf-8'));
}


function selectedLang($lang)
{
    return config("config.selected_lang.$lang");
}


function categoryName($name, $inline = false)
{
    $remove = explode(' ', $name)[0];

    $replace = '<strong>'.$remove.'</strong>';

    $newName = str_replace($remove, $replace, $name);

    return $newName;
}


function homePageTitle($name, $lang)
{
    $title = explode('_-_', $name)[0];

    return categoryName($title, true);
}

function clearCache($cache, $lang = true)
{

    if($lang == true){
        foreach(config('app.locales') as $key => $locale){
            Cache::forget($cache.'_'.$key);
        }
    }
    else{

        Cache::forget($cache);
    }

}

