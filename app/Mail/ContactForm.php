<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    public $content, $blade, $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content, $blade, $subject)
    {
        $this->content = $content;
        $this->blade = $blade;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        if(isset($this->content['email'])){
            $replyTo = $this->content['email'];
        }
        else{
            $replyTo = config('mail.username');
        }

        return $this->replyTo($replyTo)
            ->subject($this->subject)
            ->markdown("emails.$this->blade")
            ->with('content',$this->content);
    }
}
