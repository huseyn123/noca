<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VacancyForm extends Mailable
{
    use Queueable, SerializesModels;

    public $content, $resume, $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content, $resume, $subject)
    {
        $this->content = $content;
        $this->subject = $subject;
        $this->resume = $resume;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $replyTo = $this->content['email'];

        return $this->replyTo($replyTo)
            ->subject($this->subject)
            ->markdown("emails.vacancy")
            ->with('content',$this->content)
            ->attach($this->resume, [
                'as' => "resume_".$this->content['full_name'].'.'.$this->resume->getClientOriginalExtension(),
                'mime' => $this->resume->getMimeType()
            ]);
    }
}
