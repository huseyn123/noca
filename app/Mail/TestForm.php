<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Upload;
use Illuminate\Support\Facades\Storage;
use DB;

class TestForm extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;

    public function __construct($subject)
    {
        $this->subject = $subject;

    }

    public function build()
    {
        $send = $this->replyTo("rashad@apamark.az")
            ->subject($this->subject)
            ->markdown("emails.test");

        $file = storage_path("app/uploads/0LyTALs4h7V4KPgjbz7L9fTJQA0znmRHnh3jyBM1.jpeg");

        $send->attach($file, [
            'as' => "salam.jpeg",
            'mime' => "image/jpeg"
        ]);

        return $send;
    }
}
