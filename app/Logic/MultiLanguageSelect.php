<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

class MultiLanguageSelect
{
    public static function multiLang($query, $empty = false)
    {
        $optgroup = [];

        foreach($query as $data)
        {
            $optgroup[$data->lang_id][$data->id] = $data->name;
        }

        return $optgroup;

    }

}