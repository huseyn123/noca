<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use Config;

class Smtp
{
    public static function set($config)
    {
        $settings = WebCache::getSettings();

        foreach ($settings as $setting) {
            Config::set('mail.'.$setting->key, $setting->value);
        }

        Config::set('app.name', $config->company_name);
        Config::set('mail.from.address', config('mail.username'));
        Config::set('mail.from.name', $config->company_name);

    }
}