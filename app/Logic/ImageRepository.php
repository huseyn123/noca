<?php

namespace App\Logic;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\Storage;
use App\Models\Gallery;

class ImageRepository
{
    public function dropzone( $fileName, $model, $resize)
    {
        $validator = Validator::make(request()->all(), Gallery::$rules, Gallery::$messages);

        if ($validator->fails()) {

            $response = ['error' => true, 'message' => $validator->messages()->first(), 'code' => 400];

            return  Response::json($response, 400);
        }

        $id = $this->morphStore($fileName, $model, $resize, 0, true);

        $response = ['error' => false, 'message' => 'Success', 'code' => 200, 'id' => $id];

        return Response::json($response, 200);


    }

    public function dropzoneDelete($id)
    {
        $image = Gallery::find($id);

        $response = ['error' => true, 'message' => 'Image was not found', 'code' => 400];

        if(!$image){
            return  Response::json($response, 400);
        }

        $image->delete();

        $this->deleteFile($image->filename);

        return Response::json([
            'error' => false,
            'code'  => 200
        ], 200);

    }



    public function morphStore( $fileName, $model, $resize, $type = 1, $dz = false)
    {
        $file = $this->store($fileName, $resize);

        $gallery = new Gallery();

        $gallery->original_filename = $file[0]->getClientOriginalName();
        $gallery->filename = $file[1];
        $gallery->main = $type;
        $model->relations()->save($gallery);


        if($dz == true){
            return $gallery->id;
        }
        else{
            return $file;
        }
    }


    public function morphUpdate( $fileName, $model, $resize,$type = 1)
    {
        if($type == 2){
            $image = $model->mainImage;

        }else{
            $image = $model->image;
        }


        if(!$image){
            return $this->morphStore($fileName, $model, $resize,$type);
        }

        $file = $this->store($fileName, $resize, $image->filename);

        $image->original_filename = $file[0]->getClientOriginalName();
        $image->filename = $file[1];
        $image->main = $type;

        $image->save();

        return $file;
    }


    public function morphDelete($model)
    {
        $image = $model->image;

        if($image){
            $image->delete();

            $this->deleteFile($image->filename);
        }

    }


    public function store( $fileName, $resize, $update = false )
    {
        $manager = new ImageManager();

        $file = request()->file($fileName);
        $extension = $file->getClientOriginalExtension();
        $image = $manager->make($file);
        $imageSize = [$image->width(), $image->height()];

        $hash = md5($file->getClientOriginalName().'-'.time());
        $path = date("Y-m-d")."/{$hash}.{$extension}";


        if(!is_null($resize)){

            if($imageSize > $resize['resize']['size']){
                if($resize['resize']['fit'] == true){
                    $image->fit($resize['resize']['size'][0], $resize['resize']['size'][1]);
                }
                else{
                    $image->resize($resize['resize']['size'][0], $resize['resize']['size'][1], function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
            }

            if(!is_null($resize['thumb'])){
                $this->thumb($file, $path, $resize);
            }

        }

        Storage::put("public/$path",  $image->stream());

        if($update == true){
            $this->deleteFile($update);
        }

        return [$file, $path];
    }


    public function thumb($file, $path, $resize)
    {
        $manager = new ImageManager();

        $imageThumb = $manager->make($file);

        if($resize['thumb']['fit'] == true){
            $imageThumb->fit($resize['thumb']['size'][0], $resize['thumb']['size'][1]);
        }
        else{
            $imageThumb->resize($resize['thumb']['size'][0], $resize['thumb']['size'][1], function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $imageThumb->encode($file->getClientOriginalExtension(), 85);

        Storage::put("public/thumb/$path",  $imageThumb->stream());
    }


    public function deleteFile($file)
    {
        Storage::delete(["public/".$file, "public/thumb/".$file]);
    }
}