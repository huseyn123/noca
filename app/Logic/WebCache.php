<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use App\Models\Config;
use App\Models\Dictionary;
use App\Models\MainProduct;
use App\Models\Social;
use App\Models\Slider;
use App\Models\Partner;
use DB;
use Cache;

class WebCache
{
    public static function getConfig()
    {
        $config = Cache::rememberForever('webConfig', function () {
            return Config::findOrFail(1);
        });

        return $config;
    }

    public static function MainProduct()
    {
        $product = Cache::rememberForever('main_product', function () {
            return MainProduct::findOrFail(1);
        });

        return $product;
    }


    public function getDictionary($lang)
    {
        $collect = [];

        $dictionary = Cache::rememberForever("dictionary_$lang", function () use($collect, $lang)
        {
            $dic =  Dictionary::where('lang_id', $lang)->select('keyword', 'content')->orderBy('keyword', 'asc')->pluck('content', 'keyword')->toArray();

            return $dic;

        });

        return $dictionary;
    }


    public function getSocial()
    {
        $social = Cache::rememberForever('social', function () {
            return Social::orderBy('id','asc')->get();
        });

        return $social;
    }

    public function getSlider($lang)
    {
        $slider = Cache::rememberForever("slider_$lang", function () use($lang) {
            return Slider::where('lang_id', $lang)->orderBy('order', 'asc')->limit(10)->get();
        });

        return $slider;
    }


    public function getPartner()
    {
        $partners = Cache::rememberForever("partners", function () {
            return Partner::orderBy('order', 'desc')->limit(40)->get();
        });

        return $partners;
    }

    public static function getSettings()
    {
        $coaches = Cache::rememberForever('settings', function () {
            return DB::table('settings')->get();
        });

        return $coaches;
    }
}