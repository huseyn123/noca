<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;


class Breadcrumb extends Menu
{
    private function build($thispage)
    {
        $bc = [];

        $breadcrumb = $this->nested($thispage);

        foreach ($breadcrumb as $data){
            $bc[] = $data['breadcrumb'];
        }

        return $bc;
    }


    public function get($thispage)
    {
        $result = $this->build($thispage);

        return $result;
    }
}