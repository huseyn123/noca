<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use App\Models\Page;
use Cache;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Menu
{

    public static function all()
    {
        $select = ['id','slug','parent_id','template_id','name','relation_page','lang_id', 'visible', 'target'];

        $menu = Cache::remember('pages', 1440, function () use($select) {
            return Page::ordered()->select($select)->get();
        });

        return $menu;
    }


    protected function nested($thispage)
    {
        $pages = $this->all();

        $nested = $this->nestedBuild($pages, $thispage);

        return $nested;
    }


    private function nestedBuild($pages, $thispage)
    {
        $nested = [];

        foreach ($pages as $page){
            if($page->id == $thispage->parent_id){
                $nested = $this->nestedBuild($pages, $page);
            }
        }

        $nested[$thispage->slug] = ['breadcrumb' => ['slug' => $thispage->slug, 'name' => $thispage->name], 'data' => $thispage];

        return $nested;
    }


    public function getData($thispage, $splice)
    {
        $nested = $this->nested($thispage);

        $data = array_splice($nested, $splice);

        $get = array_first($data);

        return $get['data'];
    }

    public function relationPages()
    {
        $pages = $this->all();

        $relations = [];

        foreach ($pages as $cat) {
            foreach(config("app.locales") as $key => $locale){
                if($cat->lang_id == $key){
                    $relations[$cat->relation_page][$key] = $cat->slug;
                }
            }
        }

        return $relations;
    }
}