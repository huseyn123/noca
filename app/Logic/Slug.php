<?php

namespace App\Logic;

use Cocur\Slugify\Slugify;

class Slug
{

    public static function model($table){

        $model = config('config.model');

        return $model[$table];
    }



    public static function slugify($slug, $title, $table, $lang)
    {
        $slugify = new Slugify(['rulesets' => ['default', config('config.slug_replacement.'.$lang)]]);
        if(trim($slug) == '')
        {
            $generateSlug = $slugify->slugify($title);
            $uniqueSlug = self::uniqueSlug($generateSlug, $table);
            return $uniqueSlug;
        }
        else{
            $generateSlug = $slugify->slugify($slug);
            return $generateSlug;
        }
    }



    public static function uniqueSlug($slug, $table){

        $model = self::model($table);

        $num = -1;
        $default_slug = $slug;

        do{
            $num++;

            if($num > 0){
                $slug = $default_slug.'-'.$num;
            }

            $rand = $model::where('slug', $slug)->withTrashed()->first();
        }

        while(!empty($rand));

        return $slug;
    }

}