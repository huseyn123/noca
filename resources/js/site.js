$(window).load(function() {
	//EqualHeights
	eqHeight(".main_product article", 767);
	eqHeight(".news_list article", 480);
	eqHeight(".product_list article", 480);
	eqHeight(".page_gallery figure", 10);
	
	if(!$('body').hasClass('inside')){
		if($('body').hasClass('main')){
			if( $(window).width() < 975 ){
				$("body").stop(true, true).removeClass("main");

			}
			else{
				$("body").stop(true, true).addClass("main");
			}
		}
	}
});

$(function(){
	
	$(window).resize(function(){
		if( $(window).innerWidth() < 975 ){
			if(!$('body').hasClass('inside')){
				if($('body').hasClass('main')){
					$("body").stop(true, true).removeClass("main");
				}
			}
		}
		else{
			if(!$('body').hasClass('inside')){
				$("body").stop(true, true).addClass("main");
			}
		}
	});
	
	//MobileMenu
	$(".btn_menu").click(function(){
		if($(this).hasClass("closed")){
			$(".mobile_menu").stop(true, true).slideUp("normal");
			$(this).stop(true, true).removeClass("closed");
			$("body").stop(true, true).removeClass("noscroll");
		}
		else{
			$("body").stop(true, true).addClass("noscroll");
			$(this).stop(true, true).addClass("closed");
			$(".mobile_menu").stop(true, true).slideDown("normal");
		}
		return false
	});
	
	$(".mobile_menu li.sub>a").click(function(){
		if($(this).hasClass("show")){
			$(".mobile_menu li ul").stop(true, true).slideUp();
			$(this).stop(true, true).removeClass("show");
			$(this).children("i").stop(true, true).removeClass("fa fa-minus").addClass("fa fa-plus");
		}
		else {
			$(this).siblings("ul").stop(true, true).slideDown("normal");
			$(this).stop(true, true).addClass("show");
			$(this).children("i").stop(true, true).removeClass("fa fa-plus").addClass("fa fa-minus");
		}
		return false;
	});


    //Search
    $(".btn_search").click(function(){
        if($(this).hasClass("active")){
            $(this).stop(true, true).removeClass("active");
            $(".search form").stop(true, true).removeClass("show");
        }
        else{
            $(this).stop(true, true).addClass("active");
            $(".search form").stop(true, true).addClass("show");
        }
        return false
    });

    $(document).keyup(function(e) {
        if (e.keyCode === 27) {
            $(".btn_search").stop(true, true).removeClass("active");
            $(".search form").stop(true, true).removeClass("show");
        }
    });

    $(document).click(function(e){
        if (!$(e.target).is(".search form, .search form *")) {
            $(".btn_search").stop(true, true).removeClass("active");
            $(".search form").stop(true, true).removeClass("show");
        }
    });

	//Slider
	$("#slider").owlCarousel({
		dots: true,
		nav : false,
		touchDrag: false,
		mouseDrag: false,
		autoplay: true,
		autoplayHoverPause: true,
		autoplayTimeout: 6000,
		autoplaySpeed: 500,
		loop: true,
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		items: 1,
		margin: 0,
		smartSpeed: 50,
	});
	
	//Partners
	$("#partners").owlCarousel({
		dots: false,
		nav: false,
		autoplay: true,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		loop: true,
		margin: 0,
		smartSpeed: 450,
		responsive : {
			// breakpoint from 0 up
			0 : {
				items: 2
			},
			// breakpoint from 480 up
			480 : {
				items: 3
			},
			// breakpoint from 768 up
			768 : {
				items: 3
			},
			// breakpoint from 992 up
			992 : {
				items: 5
			},
			// breakpoint from 1200 up
			1200 : {
				items: 6
			}
		}
	});
	
	//ImgSvg
	$('img.svg').each(function() {
        var $img = jQuery(this);
        var imgURL = $img.attr('src');
        var attributes = $img.prop("attributes");

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Remove any invalid XML tags
            $svg = $svg.removeAttr('xmlns:a');

            // Loop through IMG attributes and apply on SVG
            $.each(attributes, function() {
                $svg.attr(this.name, this.value);
            });

            // Replace IMG with SVG
            $img.replaceWith($svg);
        }, 'xml');
    });
	
	//FAQAccordion
	$(".faq_list .head").on("click", function(){
		if($(this).hasClass("active")){
			$(this).stop(true, true).removeClass("active");
			$(this).siblings(".body").stop(true, true).slideUp();
		}
		else{
			$(this).parent().siblings(".block").children(".head").stop(true, true).removeClass("active");
			$(this).stop(true, true).addClass("active");
			$(this).parent().siblings(".block").children(".body").stop(true, true).slideUp();
			$(this).siblings(".body").stop(true, true).slideDown();
		}
	});

	//Gallery
	if( $(".page_gallery figure").length ){
		$(".page_gallery figure").magnificPopup({
			delegate: "a.zoom",
			type: "image",
			tLoading: "Loading image #%curr%...",
			mainClass: "mfp-img-mobile",
			gallery: {
				enabled: true,
				navigateByImgClick: false,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',				
			}
		});
	}

	//Media
		$(".main_video a.zoom").magnificPopup({
				disableOn: 700,
			type: "iframe",
			mainClass: "mfp-fade",
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false,
			gallery:{
				enabled:false
			}
	});

});

function eqHeight(param, sizes) {
	if ($(param).length){
		var resizeTimer;
		$( window ).resize(function() {
			var width = $(window).width();
			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(func, 500);
		});
		func()
	}

	function func(){
		var width = $(window).width();
		$(param).removeAttr('style');
		if (width < sizes){
			$(param).height("auto");
		}else{
			$(param).equalHeights();
		}
	}
}

// $(function(){
//	
// 	$("#form").validate({
// 		rules: {
// 			fullname: {
// 				required: true,
// 				lettersonly: true,
// 				minlength: 5,
// 				maxlength: 35
// 			},
// 			email: {
// 				required: true,
// 				email: true
// 			},
// 			phone: {
// 				required: true
// 			},
// 			subject: {
// 				required: true
// 			},
// 			message: {
// 				required: true
// 			}
// 		},
// 		messages: {
// 			fullname: {
// 				required: 'Bu sahənin doldurulması vacibdir',
// 				lettersonly: 'Yalnız hərf istifadə oluna bilər',
// 				minlength: 'Minimum simvol 5',
// 				maxlength: 'Maximum simvol 35'
// 			},
// 			email: {
// 				required: 'Bu sahənin doldurulması vacibdir',
// 				email: 'E-mail forması düzgün deyil'
// 			},
// 			phone: {
// 				required: 'Bu sahənin doldurulması vacibdir'
// 			},
// 			subject: {
// 				required: 'Bu sahənin doldurulması vacibdir'
// 			},
// 			message: {
// 				required: 'Bu sahənin doldurulması vacibdir'
// 			}
// 		},
// 		success: function(label) {
// 			label.html('').removeClass('error').addClass('ok');
// 		},
// 		errorPlacement: function(error, element) {
// 			$(element).parents('.span').append(error);
// 		},
// 		highlight: function ( element, errorClass, validClass ) {
// 			$(element).parents(".span").addClass( "error-item" ).removeClass( "valid-item" );
// 		},
// 		unhighlight: function (element, errorClass, validClass) {
// 			$(element).parents(".span").addClass( "valid-item" ).removeClass( "error-item" );
// 		}
// 	});
//	
// 	jQuery.validator.addMethod("lettersonly", function(value, element) {
// 		return this.optional(element) || /^[a-z\s]+$/i.test(value);
// 	}, "Only alphabetical characters");
//	
// });