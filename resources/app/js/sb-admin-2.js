var loading = "Please wait...<img src='../css/images/loading.gif'>";

$.fn.select2.defaults.set( "theme", "bootstrap" );


$(document).ready( function() {

    $("#template_id").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".form-class").not(".hh" + optionValue).show();
                $(".hh" + optionValue).hide();
            } else{
                $(".form-class").hide();
            }
        });
    }).change();


    $(':file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        }

    });


    $(':file').on('change',function(e){

        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);

    });
});




$(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.datepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent:false
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('[data-toggle="popover"]').popover();

    $('#side-menu').metisMenu();

    $('.select-search').select2();




});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});



$(document).ready(function(){


    $("body").on('click', '.open-modal-dialog', function (e){
        e.preventDefault();
        modal($(this).attr('data-title'), $(this).attr('data-active'), $(this).attr('data-action'), $(this).attr('second-modal'), $(this).attr('data-large'));
    });


    function modal(title,active,action,modal,large) {
        if(modal == true){
            var mBody = "#secondModal";
            $('#mySecondModal').modal('show');
        }
        else{
            var mBody = "#modalBody";
            $('#myModal').modal('show');
        }
        if(large == true){
            $( ".modal-dialog" ).addClass( "modal-lg" );
        }
        else{
            $( ".modal-dialog" ).removeClass( "modal-lg" );
        }

        $.ajax({
            url: action,
            type: "GET",
            timeout: 20000,
            data: {"title": title, "status": active},

            beforeSend: function() {
                $(mBody).html(loading);
            },
            success: function(data){
                $(mBody).html(data);
            },
            error: function() {
                $(mBody).html('<h3>Unexpected Error!</h3>');
            }
        });
    }

    $('#modal-confirm, #modal-confirm-image').on('show.bs.modal', function(e) {
        $(this).find('.warning-modal').attr('action', $(e.relatedTarget).attr('data-action'));
    });



    $("#ajaxForm").on("submit",function (event){

        event.preventDefault();
        sbmButton($(this));
    });

    function sbmButton(action){

        if (action.data('submitted') === true) {
            return false;
        }
        else
        {
            $.ajax({
                url:  action.attr('action'),
                type: action.attr('method'),
                timeout: 20000,
                headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
                data: action.serialize(),
                dataType: "json",

                beforeSend: function(response) {
                    $(".loadingButton").button('loading');
                },

                success: function(response){
                    $(".loadingButton").button('reset');

                    if(response.success == 1) {
                        if(response.once == true)
                        {
                            $('input[type=submit]').attr('disabled',true);
                            action.data('submitted', true);
                        }

                        if(response.redirect)
                        {
                            window.location = response.redirect;
                        }


                        if(response.resetForm == true){
                            $('#ajaxForm')[0].reset();
                        }
                    }

                    $(".msgBox").html('<div class="alert '+response.alert+'">'+response.msg+'</div>');
                },

                error: function(res){
                    $(".loadingButton").button('reset');
                    $(".msgBox").html('<h3>Unexpected Error!</h3>');
                }
            });
        }
    }

    $(".actionPage").on("submit",function (event){

        event.preventDefault();
        actionPage($(this));
    });

    function actionPage(action){

        for ( instance in CKEDITOR.instances ) {
            CKEDITOR.instances[instance].updateElement();
        }

        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
            //data: action.serialize(),
            data: new FormData(action[0]),
            dataType: "json",
            processData: false,
            contentType: false,

            beforeSend: function() {
                $(".loadingButton").button('loading');
            },

            success: function(response, event){

                $(".loadingButton").button('reset');


                if(response.error == 0) {

                    if(response.data.uid){
                        var del = action.find('.del');

                        var newDelAction = del.data('action').replace(0, response.data.uid);

                        del.attr('data-action',newDelAction);

                        del.show();

                        $(event.relatedTarget).attr('data-action', newDelAction);

                    }

                    if(response.data.slug){
                        action.find('#slug').val(response.data.slug);
                    }


                    if(response.redirect)
                    {
                        window.location = response.redirect;
                    }
                }

                $(".msgBox").html('<div class="alert '+response.alert+'">'+response.msg+'</div>');
            },

            error: function(){
                $(".loadingButton").button('reset');
                $(".msgBox").html('<h3>Unexpected Error!</h3>');
            }
        });
    }
});


