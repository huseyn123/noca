<?php

return [
    'delete' => 'Delete',
    'home_page' => 'Əsas səhifə',
    'site_by' => 'Site by :site',
    'error_send_message' => 'Məktubun göndərilməsi baş tutmadı',
    'search_result' => '<span>:count</span> search results for "<span>:keyword</span>"',
    'cancel' => 'Cancel',
    'whops' => 'Whoops! Something went wrong!',
    'invalid_number' => 'Invalid number',
];
