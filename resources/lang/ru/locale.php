<?php

return [
    'home_page' => 'Əsas səhifə',
    'error_send_message' => 'Məktubun göndərilməsi baş tutmadı',
    'site_by' => 'Подготовка сайта: :site',
    'search_result' => "Найдено <span>:count</span> результатов",
    'cancel' => 'Отмена',
    'whops' => 'Whoops! Something went wrong!',
    'invalid_number' => 'Номер недействителен',
];
