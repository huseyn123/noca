<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<p>Hörmətli istifadəçi,</p>
<p>Hesabınızı aktiv etmək üçün, elektron ünvanınızı təsdiqləyərək aşağıdakı linki tıklayın.
    Əgər, siz bu əməliyyatı icra etməmisinizsə, linkə əhəmiyyət verməyin.
</p>
<p><a href="{{ url("activate/$confirmation_code") }}" target="_blank">Hesabı aktivləşdir</a></p>


</body>
</html>