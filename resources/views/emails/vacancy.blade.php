@component('mail::message')

    #{{$content['full_name']}}

    - Email: {{$content['email']}}

@endcomponent