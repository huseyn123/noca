

<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<p>Hörmətli istifadəçi,</p>
<p>Şifrəni dəyişdirmək üçün, xahiş edirik, aşağıdakı göstərilən linki tıklayın.
Əgər, siz şifrəni dəyişmək üçün müraciət etməmisinizsə, bu linkə əhəmiyyət verməyin.
</p>
<p><a href="{{ url("auth/reset/$email/$token") }}" target="_blank">Şifrəni dəyiş</a></p>

</body>
</html>