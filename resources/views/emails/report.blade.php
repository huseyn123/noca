@component('mail::message')

    #{{$content['text']}}

    Aşkarlanan səhvin linki

    @component('mail::button', ['url' => $content['link']])
        {{ $content['link'] }}
    @endcomponent

@endcomponent


