@component('mail::message')

    @if(isset($content['text']))
    #{{$content['text']}}
    @endif

    İstifadəçinin məlumatları:

    - Ad, soyad: {{$content['fullname']}}

    @if(isset($content['email']))
    - Email: {{$content['email']}}
    @endif

    - Telefon: {{$content['phone']}}

    @if(isset($content['subject']))
    - Mövzu: {{$content['subject']}}
    @endif

@endcomponent