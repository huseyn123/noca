@extends ('layouts.web', [
'page_heading' => Cache::get('dictionary_'.app()->getLocale())['nothing_found_title'],
'menuWithoutSlug' => true,
'menu' => Cache::get('pages'),
'webConfig' => webConfig(),
'dictionary' => Cache::get('dictionary_'.app()->getLocale()),
'social' => Cache::get('social'),
'lang' => app()->getLocale()
])


@section ('content')
    <!-- Error404 Begin -->
    <section class="error404 text-center">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <img src="{{ asset('images/error404.png') }}" alt="">
                    <h3>{{ Cache::get('dictionary_'.app()->getLocale())['nothing_found_title'] }}</h3>
                    <p>{{ Cache::get('dictionary_'.app()->getLocale())['nothing_found_heading'] }}</p>

                    <a href="{{ route('home') }}" title="">{{ Cache::get('dictionary_'.app()->getLocale())['home_page'] }}</a>

                </div>
            </div>
        </div>
    </section>
    <!-- Error404 End -->
@endsection
