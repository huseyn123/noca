@extends ('planes.app')
@section('main-title', webConfig()->company_name)
@section ('layout')

    <div class="container">
        <div class="row">

            <div class="col-md-8 col-md-offset-2">

                @section ('login_panel_title','Daxil ol')
                @section ('login_panel_body')

                {!! Form::open(['url'=>'/login', 'method'=>'POST', "class" => "form-horizontal", "role" => "form"]) !!}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label("email", "Email", ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::email("email", old('email'), ["class" => "form-control", "autofocus"]) !!}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        {!! Form::label("password", "Şifrə", ['class' => 'col-md-4 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::password("password", ["class" => "form-control"]) !!}

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox("remember", old('remember') ? 'checked' : '') !!} Yadda saxla
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            {!! Form::button("Login", ["class" => "btn btn-primary", "type" => "submit"]) !!}

                            <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                Şifrəni unutmuşam
                            </a>
                        </div>
                    </div>

                {!! Form::close() !!}

                @endsection

                @include('widgets.panel', array('as'=>'login', 'header'=>true))
            </div>
        </div>
    </div>
@endsection