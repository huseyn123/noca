@extends('planes.web')
@section('layout')

    @include('web.elements.mobile-menu')

    @include('web.elements.header')

    @yield('content')


    @include('web.elements.footer')


@endsection