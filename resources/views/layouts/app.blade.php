@extends('planes.app')
@section('main-title', webConfig()->company_name)
@section('layout')

    <div id="wrapper">
        @if(Auth::check())
            @include('widgets.navbar')
        @endif
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <br>

                    @if (isset($errors) && $errors->any())
                        @include('widgets.alert', array('class'=>'danger', 'message'=>$errors->first()))
                    @elseif (request()->session()->has('message'))
                        @include('widgets.alert', array('class'=>'success', 'message'=>session()->get('message').'.'))
                    @endif
                    @if(isset($breadcrumb))
                        {!! $breadcrumb !!}
                    @endif

                    @if(!isset($panel))
                        <h2 class="page-header">@yield('page_heading')</h2>
                    @endif
                </div>
            </div>
            <div class="row">
                @if(isset($panel) && $panel == true)
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    @yield('page_heading')
                                    @if(isset($create) && $create == true)
                                        @if(isset($modal) && $modal == true)
                                            {!! Form::button(icon('plus').$title, ['class'=>'btn btn-primary btn-sm pull-right open-modal-dialog', 'data-action'=> route($create)]) !!}
                                        @else
                                            <a href="{{route($create)}}" class="btn btn-primary btn-sm pull-right" style="color:#fff">{!! icon('plus').$title !!}</a>
                                        @endif
                                        <div class="clearfix"></div>
                                    @endif
                                </div>
                            </div>
                            <div class="panel-body">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                @else
                    @yield('content')
                @endif
            </div>
            <!-- /#page-wrapper -->
            <div class="footer">
                <span>&copy; {{ webConfig()->company_name }} </span>
                 <span class="pull-right">
                    <a href="http://marcom.az" target="_blank">Marcom</a> | Developer: <a href="http://aghayev.net" target="_blank">Rashad Aghayev</a>
                </span>
            </div>
            <br>
        </div>
    </div>
@stop