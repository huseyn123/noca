@foreach($fields as $input)


$html = '';

foreach (array_diff_key($fields, array_combine($except, $except)) as $field){

$html.='<div class="form-group col-md-12>';

            $label = FormFacade::label($field['db'], $field['label']);

            if($field['type'] == 'select'){
                $form = FormFacade::select($field['db'], $field['data'], $data == false ? $field['selected'] : old($field['db'], $data->{$field['db']}), $field['attr']);
            }
            else if($field['type'] == 'file'){
                $form = FormFacade::file($field['db'], $field['attr']);
            }
            else if($field['type'] == 'checkbox'){
                $form = FormFacade::checkbox($field['db'], $field['value'], false, $field['attr']);
            }
            else{
                $form = FormFacade::{$field['type']}($field['db'], $data == false ? null : old($field['db'], $data->{$field['db']}), $field['attr']);
            }

            $html.=$label;

            if(isset($field['design'])){
                $html.=call_user_func($field['design'], $form, $data);
            }
            else{
                $html.=$form;
            }

            if(isset($field['attr']['title'])){
                $html.='<p class="help-block">'.$field['attr']['title'].'</p>';
}

$html.='</div>';

}

return $html;

@endforeach


