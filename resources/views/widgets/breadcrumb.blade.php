@if ($breadcrumbs)

    @foreach ($breadcrumbs as $breadcrumb)

        @if (!$breadcrumb->last)
            <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
            <span class="line">/</span>
        @else
            <span>{{ $breadcrumb->title }}</span>
        @endif
    @endforeach

@endif