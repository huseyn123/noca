<div class="modal fade" id="modal-confirm-image" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        {!! Form::open(array('method'=> 'DELETE', 'class'=>'warning-modal','accept-charset'=>'UTF-8')) !!}
        <div class="modal-content">

            <div class="modal-header">
                {!! Form::button('&times;', ['class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => "true"]) !!}
                <h4 class="modal-title" id="myModalLabel">Əməliyyatı təsdiq et</h4>
            </div>

            <div class="modal-body">
                <p>Silmək istədiyinizə əminsinizmi?</p>
            </div>

            <div class="modal-footer">
                {!! Form::button('İmtina', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) !!}
                {!! Form::button('Sil', ['class' => 'btn btn-danger', 'type' => 'submit']) !!}
            </div>

        </div>

        {!! Form::close() !!}

    </div>
</div>