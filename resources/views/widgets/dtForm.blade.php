<script>
    $("#dtForm").on("submit",function (event){

        @if(isset($editor))
        for ( instance in CKEDITOR.instances ) {
            CKEDITOR.instances[instance].updateElement();
        }
        @endif

        event.preventDefault();
        datatableForm($(this));
    });

    function datatableForm(action){



        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
            data: action.serialize(),
            dataType: "json",

            beforeSend: function(response) {
                $("#loadingButton").button('loading');
            },

            success: function(response){
                $("#loadingButton").button('reset');

                if(response.error == 0) {

                    if(typeof response.draw =='object') {
                        $(response.draw[0]).DataTable().draw( false );
                        $(response.draw[1]).DataTable().draw( false );
                    }
                    else{
                        $(response.draw).DataTable().draw( false );
                    }

                    if(response.close)
                    {
                        $(response.close).modal("toggle");
                    }
                }

                $("#msgBox").html('<div class="alert '+response.alert+'">'+response.msg+'</div>');
            },

            error: function(res){
                $("#loadingButton").button('reset');
                $("#msgBox").html('<h3>Unexpected Error!</h3>');
            }
        });
    }
</script>