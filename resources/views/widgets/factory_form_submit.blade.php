<div class="form-group col-md-12" style="border-top:1px solid #eee"><br>


    @if(isset($return))
        {!! Form::submit('Yadda saxla və bağla', ['class' => "btn btn-primary", 'name' => 'return']) !!}
    @endif

    @if(isset($info[$key]))

        <a href="#" class="btn btn-danger del" data-action="{{ $delRoute }}" data-toggle="modal" data-target="#modal-confirm-image">
            <i class="fa fa-trash"></i> Sil
        </a>

    @endif

    <a href="javascript: history.back()" class="btn btn-warning">Geri qayıt</a>

</div>

<div class="form-group col-md-12">
    <div class="msgBox"></div>
</div>
