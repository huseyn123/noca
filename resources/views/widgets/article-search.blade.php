{!! Form::open(array('method'=>'POST','id'=>'search-form', 'class'=>'form-inline')) !!}

<div class="form-group hidden-xs">

    @if(!isset($onlyByCategory))
    {!!Form::text("title", null, ["class" => "form-control", "id" => "title", "placeholder" => 'Başlıq üzrə'])!!}
    @endif

    {!!Form::select("category_id", $cat, null, ["class" => "form-control", "id" => "category_id"])!!}

    {!!Form::button(icon('search'), ["class" => "btn btn-primary", 'type' => 'submit'])!!}
</div>

<div class="visible-xs">
    <div class="input-group">
        {!!Form::text("title", null, ["class" => "form-control", "id" => "title", "placeholder" => 'Başlıq üzrə'])!!}
        <span class="input-group-btn">
            {!!Form::button(icon('search'), ["class" => "btn btn-primary", 'type' => 'submit'])!!}
        </span>
    </div>
</div>

{!! Form::close() !!}
