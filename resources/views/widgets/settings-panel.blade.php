
@if(!isset($panel) )

	<ul class="nav nav-tabs">
	    <li role="presentation" {{ activeUrl(route('config.show', 1)) }}><a href="{{route('config.show', 1)}}">Konfiqurasiya</a></li>
	    <li role="presentation" {{ activeUrl(route('main_video.show', 1)) }}><a href="{{route('main_video.show', 1)}}">Video</a></li>
		<li role="presentation" {{ activeUrl(route('smtp.index')) }}><a href="{{route('smtp.index')}}">SMTP</a></li>
{{--	    <li role="presentation" {{ activeUrl(route('social.index')) }}><a href="{{route('social.index')}}">Sosial şəbəkə</a></li>--}}
	</ul>
@endif

<div class="tab-content">
    <div class="tab-pane active"><br>
        @yield('settings')
    </div>
</div>