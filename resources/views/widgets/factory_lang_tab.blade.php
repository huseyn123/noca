<div class="form-group col-md-12">
    <div class="row">
        <ul class="nav nav-tabs">

            @foreach(config("app.locales") as $key => $locale)
                <li role="presentation" @if($loop->first) class="active" @endif>
                    <a href="#{{$name.'_'.$key}}" data-toggle="tab">{{$locale}}</a>
                </li>
            @endforeach

        </ul>

        <div class="tab-content row">

            @foreach(config("app.locales") as $key => $locale)

                    <div id="{{$name.'_'.$key}}" class="tab-pane fade @if($loop->first) in active @endif">

                        {!! Form::open(['url'=>route('scope.store'), 'method'=>'POST', 'files'=>true ]) !!}

                        <div class="form-group">

                            <div class="col-md-12">

                                @if(isset( $info[$key] ))
                                    <img src="{{ asset("storage/".$info[$key]) }}" class="center-block img-responsive">
                                @endif

                                <br>

                                <div class="input-group">
                                    <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    <i class="fa fa-cloud-upload"></i>@if($info[$key] ) Şəkli dəyiş @else Əlavə et @endif
                                    {!! Form::file('scope_image_'.$key , ['id' => 'file_'.$key, 'class' => 'form-control image', 'style' => 'display:none']) !!}

                                    {!! Form::file('' , ['id' => 'file_'.$key, 'class' => 'form-control image', 'style' => 'display:none']) !!}
                                </span>
                                    </label>
                                    <input type="text" class="form-control" readonly="" >
                                </div>
                            </div>

                            <div class="col-md-12">
                                <input type="text" class="form-control"  name="lang" style="display: none" value="{{ $key }}">
                            </div>


                        </div>

                            @include('widgets.factory_form_submit', ['id' => $info['id'],'text' => trans('locale.next'), 'class' => 'success', 'icon' => 'arrow-right', 'return' => true,'delRoute' => route('del_factory_image', 'scope_image_'.$key )])

                        {!! Form::close() !!}

                    </div>

            @endforeach

        </div>
    </div>

</div>
