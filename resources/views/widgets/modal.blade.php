<div class="modal fade" id="modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        @if(isset($button))
            {!! Form::open(['url'=>route($route), 'method'=>$method, 'class'=>'form-horizontal' ]) !!}
        @endif
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{$title}}</h4>
            </div>

            <div class="modal-body">
                @include($include)
            </div>

            <div class="modal-footer">
                @if(isset($button))
                    {!! Form::submit($button, ['class'=>'btn btn-success']) !!}
                @endif
                {!! Form::button("Close", ['class'=>'btn btn-default', "data-dismiss"=>"modal"]) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
