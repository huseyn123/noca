<div class="form-group col-md-12">
    <div class="row">
        @if(isset($tab_title))
            {!! Form::label($name, $tab_title) !!}
        @endif

        <ul class="nav nav-tabs">

            @foreach(config("app.locales") as $key => $locale)
                <li role="presentation" @if($loop->first) class="active" @endif>
                    <a href="#{{$key.$name}}" data-toggle="tab">{{$locale}}</a>
                </li>
            @endforeach

        </ul>

        <div class="tab-content row">

            @foreach(config("app.locales") as $key => $locale)

                <div id="{{$key.$name}}" class="tab-pane fade @if($loop->first) in active @endif">

                    <div class="form-group">
                        <div class="col-md-12">


                            @if(!isset($multiRow))

                                @if(isset($editor))
                                    {!! Form::$input($name.'_'.$key, isset($info) ? $info[$name.'_'.$key] : null, ['class' => 'form-control', 'id' => $editor == 1 ? "editor-$key" : 'no-edit']) !!}
                                @else
                                    {!! Form::$input($name.'_'.$key, isset($info) ? $info[$name.'_'.$key] : null, ['class' => 'form-control']) !!}
                                @endif
                            @else
                                @if(isset($editor))

                                    {!! Form::$input("content[$key]", isset($info[$key]) ? $info[$key] : null, ['class' => 'form-control' , 'id' => $editor == 1 ? "editor-$key" : 'no-edit']) !!}
                                @else
                                    {!! Form::$input("content[$key]", isset($info[$key]) ? $info[$key] : null, ['class' => 'form-control']) !!}
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>

</div>
