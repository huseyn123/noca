<div class="form-group col-md-12" style="border-top:1px solid #eee"><br>
    {!! Form::button(icon($icon).$text, ['class' => "loadingButton btn btn-$class", 'type' => 'submit', 'data-loading-text' => g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off']) !!}

    @if(isset($return))
        {!! Form::submit('Yadda saxla və bağla', ['class' => "btn btn-primary", 'name' => 'return']) !!}
    @endif

    @if(auth()->user()->role == 1 && isset($del))
        <a href="#" class="btn btn-danger del" data-action="{{ route("trash_$route", $id) }}" data-toggle="modal" data-target="#modal-confirm" style="@if($id == 0) display:none @endif">
            <i class="fa fa-trash"></i> Sil
        </a>

    @elseif(isset($delRoute) && isset($info->image) )
        <a href="#" class="btn btn-danger del" data-action="{{ $delRoute }}" data-toggle="modal" data-target="#modal-confirm-image">
            <i class="fa fa-trash"></i> Sil
        </a>
    @endif

    <a href="javascript: history.back()" class="btn btn-warning">Geri qayıt</a>

</div>

<div class="form-group col-md-12">
    <div class="msgBox"></div>
</div>
