<a href="#" data-action="{{route('user.edit', $id)}}" class="btn btn-xs btn-primary open-modal-dialog"><i class="fa fa-edit"></i></a>
@if(is_null($deleted_at))
    <a href="#" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('soft-delete-{{ $id }}').submit();">
        <i class="fa fa-trash"></i>
    </a>

    {!! Form::open(['method' => 'PATCH', 'url' => route('trash_user', $id), 'id' => "soft-delete-$id", 'style' => 'display:none']) !!}
    {!! Form::close() !!}
@else
    <a href="#" class="btn btn-xs btn-default" onclick="event.preventDefault(); document.getElementById('restore-delete-{{ $id }}').submit();">
        <i class="fa fa-repeat"></i>
    </a>

    {!! Form::open(['method' => 'PATCH', 'url' => route('restore_user', $id), 'id' => "restore-delete-$id", 'style' => 'display:none']) !!}
    {!! Form::close() !!}
    <a href="#" class="btn btn-xs btn-danger" data-action="{{route('user.destroy', $id)}}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-remove"></i></a>
@endif