<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="{{ route ('dashboard') }}" style="padding-top:5px !important;">
      <img src="{{ asset('images/new_logo.png') }}" height="40px" alt="{{ webConfig()->company_name }}">
    </a>
  </div>
  <!-- /.navbar-header -->

  <ul class="nav navbar-top-links navbar-right">
    <li><a href="{{ url ('/') }}" target="_blank">Sayt</a></li>
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-user fa-fw"></i>  {{auth()->user()->first_name}} <i class="fa fa-caret-down"></i>
      </a>
      <ul class="dropdown-menu dropdown-user">
        <li><a href="{{ route ('show_user') }}"><i class="fa fa-user fa-fw"></i> Profil</a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out fa-fw"></i> Çıx
          </a>

          <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>
      </ul>
      <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
  </ul>
  <!-- /.navbar-top-links -->

  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">

        {{--<li {{ activeUrl(route('catalog.index')) }}>--}}
          {{--<a href="{{ route ('catalog.index') }}"><i class="fa fa-image fa-fw"></i> E-Kataloq</a>--}}
        {{--</li>--}}

        <li {{ activeUrl(route('slider.index')) }}>
          <a href="{{ route ('slider.index') }}"><i class="fa fa-image fa-fw"></i> Slider</a>
        </li>

        <li>
          <a href="#"><i class="fa fa-file fa-fw"></i> Səhifələr<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li {{ activeUrl(route('page.index')) }}><a href="{{ route ('page.index') }}"><i class="fa fa-list fa-fw"></i> Menyular</a></li>
            <li {{ activeUrl(route('category.index')) }}><a href="{{ route ('category.index') }}"><i class="fa fa-list-ul fa-fw"></i> Alt menyular</a></li>
            @if(auth()->user()->role == 1)
              <li {{ activeUrl(route('order_page')) }}><a href="{{ route('order_page') }}?lang=az"><i class="fa fa-arrows-alt fa-fw"></i> {{trans('locale.order')}}</a></li>
            @endif
          </ul>
        </li>

        <li {{ activeUrl(route('article.index')) }}>
          <a href="{{ route ('article.index') }}"><i class="fa fa-newspaper-o fa-fw"></i> Xəbərlər</a>
        </li>

        <li>
          <a href="#"><i class="fa fa-file fa-fw"></i> Məhsullar<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li {{ route ('product.index') }}><a href="{{ route ('product.index') }}"><i class="fa fa-list fa-fw"></i> Məhsullar</a></li>
            <li {{ activeUrl(route('product_info.index')) }}><a href="{{ route ('product_info.index') }}"><i class="fa fa-tag fa-fw"></i>Məlumat</a></li>
          </ul>
        </li>



        <li {{ activeUrl(route('partners.index')) }}>
          <a href="{{ route ('partners.index') }}"><i class="fa fa-bandcamp fa-fw"></i> Tərəfdaşlar</a>
        </li>

        <li {{ activeUrl(route('dictionary.index')) }}>
          <a href="{{ route ('dictionary.index') }}"><i class="fa fa-text-height fa-fw"></i> Lüğət</a>
        </li>


        @if(auth()->user()->role == 1)
        <li {{ activeUrl(route('user.index')) }}>
          <a href="{{ route ('user.index') }}"><i class="fa fa-user fa-fw"></i> İstifadəçilər</a>
        </li>

        <li  {{ activeUrl(url('manager/filemanager')) }}>
          <a href="{{ url ('manager/filemanager') }}"><i class="fa fa-folder fa-fw"></i> File Manager</a>
        </li>

        <li>
          <a href="#"><i class="fa fa-list fa-fw"></i> Tənzimləmələr<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
              <li  {{ activeUrl(route('config.show', 1)) }}>
                <a href="{{ route ('config.show', 1) }}"><i class="fa fa-cogs fa-fw"></i> Konfiqurasiya</a>
              </li>
              <li  {{ activeUrl(route('sitemap.index')) }}>
                <a href="{{ route ('sitemap.index') }}"><i class="fa fa-map fa-fw"></i> Sitemap</a>
              </li>
          </ul>
        </li>

        @endif

      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
  <!-- /.navbar-static-side -->
</nav>
