@if($deleted == false)

    <a href="{{ route("$route.edit", $id) }}" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> </a>

    @if(auth()->user()->role == 1)

        @if(isset($trash) && $trash == false)
            <a href="#" class="btn btn-xs btn-danger" data-action="{{route("$route.destroy", $id)}}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-remove"></i></a>
        @else
            <a href="#" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('soft-delete-{{$id}}').submit();">
                <i class="fa fa-trash"></i>
                {!! Form::open(['method' => 'PATCH', 'url' => route("trash_$route", $id), 'id' => "soft-delete-{$id}", 'style' => 'display:none']) !!}
                {!! Form::close() !!}
            </a>
        @endif

    @endif

@else
    <a href="#" class="btn btn-xs btn-default" onclick="event.preventDefault(); document.getElementById('restore-delete-{{$id}}').submit();">
        <i class="fa fa-repeat"></i>

        {!! Form::open(['method' => 'PATCH', 'url' => route("restore_$route", $id), 'id' => "restore-delete-{$id}", 'style' => 'display:none']) !!}
        {!! Form::close() !!}
    </a>

    @if(auth()->user()->role == 1)
        <a href="#" class="btn btn-xs btn-danger" data-action="{{route("$route.destroy", $id)}}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-remove"></i></a>
    @endif
@endif


