<div class="modal fade" id="modal-confirm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        {!! Form::open(array('method'=> isset($trash) ? 'PATCH' : 'DELETE', 'class'=>'warning-modal','accept-charset'=>'UTF-8')) !!}
        <div class="modal-content">

            <div class="modal-header">
                {!! Form::button('&times;', ['class' => 'close', 'data-dismiss' => 'modal', 'aria-hidden' => "true"]) !!}
                <h4 class="modal-title" id="myModalLabel">Əməliyyatı təsdiq et</h4>
            </div>

            <div class="modal-body">
                @if(isset($trash))
                  <p>Silmək istədiyinizə əminsinizmi? Silinmiş səhifəni bərpa etmək mümkün OLACAQ.</p>
                @else
                   <p>Silmək istədiyinizə əminsinizmi? Silinmiş səhifəni bərpa etmək mümkün olmayacaq.</p>
                @endif
            </div>

            <div class="modal-footer">
                {!! Form::button('İmtina', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) !!}
                {!! Form::button('Sil', ['class' => 'btn btn-danger', 'type' => 'submit']) !!}
            </div>

        </div>

        {!! Form::close() !!}

    </div>
</div>