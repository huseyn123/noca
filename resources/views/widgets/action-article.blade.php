@if(is_null($deleted))
    <a href="{{ url($page->lang_id.'/'.$page->category_slug.'/'.$page->slug) }}" class="btn btn-xs btn-default" target="_blank"><i class="fa fa-eye"></i></a>

    <a href="{{ route("$route.edit", $page->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>

    @if(auth()->user()->role == 1)
        <a href="#" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('soft-delete-{{ $page->id }}').submit();">
            <i class="fa fa-trash"></i>
        </a>

        {!! Form::open(['method' => 'PATCH', 'url' => route("trash_$route", $page->id), 'id' => "soft-delete-$page->id", 'style' => 'display:none']) !!}
        {!! Form::close() !!}

    @endif
@else
    <a href="#" class="btn btn-xs btn-default" onclick="event.preventDefault(); document.getElementById('restore-delete-{{ $page->id }}').submit();">
        <i class="fa fa-repeat"></i>
    </a>

    {!! Form::open(['method' => 'PATCH', 'url' => route("restore_$route", $page->id), 'id' => "restore-delete-$page->id", 'style' => 'display:none']) !!}
    {!! Form::close() !!}

    @if(auth()->user()->role == 1)
        <a href="#" class="btn btn-xs btn-danger" data-action="{{route("$route.destroy", $page->id)}}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-remove"></i></a>
    @endif
@endif