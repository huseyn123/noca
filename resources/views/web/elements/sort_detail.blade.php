<div class="product_ingridient row">
    <div class="tab_block">
        <div class="col-xs-12">
            <h4>TƏRKİBİ:</h4>
            {!! $sort->content !!}
        </div>
    </div>
</div>

@if($sort->galleries->count())
    <div class="product_gallery">
        <div class="row">
            <h2 class="col-xs-12">Foto Qalereya</h2>

            @foreach($sort->galleries as $gallery)
                <div class="col-md-3 col-sm-3 col-xs-4 col-mob-6">
                    <figure>
                        <a href="{{ asset("storage/$gallery->filename") }}" title="{{ $sort->name }}" class="zoom">
                            <img src="{{ asset("storage/thumb/$gallery->filename") }}" alt="{{ $sort->name }}">
                        </a>
                    </figure>
                </div>
            @endforeach
        </div>
    </div>
@endif

@if(request()->ajax())
    <script src="{{asset(mix('js/script.js'))}}"></script>
@endif