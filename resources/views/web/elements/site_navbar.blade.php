    <nav class="sidemenu">
        <ul class="list-unstyled">

            @foreach($cat->children as $submenu)

                <li @if($page->slug == $submenu->slug) class="active" @endif><a href="{{ route('showPage', $submenu->slug) }}" title="{{ $page->name }}">{{ $submenu->name }}</a></li>

            @endforeach

        </ul>
    </nav>
    <!-- SideMenu End -->

    @if( isset($form) )
        @include('web.elements.application-form')
    @endif
    <!-- SideForm Begin -->
