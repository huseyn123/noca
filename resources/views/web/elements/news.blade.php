<!-- MainMedia Begin -->
<section class="main_media">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                {{--{!! $dictionary['media'] !!}--}}
                <div class="title_block bdr">
                    <h1> {!! $dictionary['media'] or '<strong>Media</strong> mərkəzi' !!} </h1>
                    <a href="{{ route('showPage' , $newsPage->slug) }}" title="">daha çox</a>
                </div>

                @foreach($news as $articles)

                <article>
                    <span class="date">
                        {!! blogDate($articles->published_at) !!}
                    </span>
                    <div class="body">
                        <h2><a href="{{ route('showArticle', [$newsPage->slug, $articles->slug]) }}" title="">{{ $articles->title }}</a></h2>
                        <p> {{ str_limit( $articles->summary , 150 ) }} </p>
                    </div>
                </article>

                @endforeach

            </div>
        </div>
    </div>
    <div class="news_carousel">
        <div id="news_carousel" class="owl-carousel">

            @foreach($news as $articles)

                <figure>
                    <img src="{{ asset('storage/'.$articles->image->filename) }}">
                </figure>

            @endforeach

        </div>
    </div>
</section>
<!-- MainMedia End -->
