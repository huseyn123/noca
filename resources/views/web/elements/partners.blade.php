<!-- Partners Begin -->
<section class="partners">
    <div class="container">
        <div class="row">

            <div class="col-xs-12">
                <div id="partners" class="owl-carousel">

                    @foreach($partners as $partner)

                        <figure>
                            <a href="@if(!is_null($partner->site_url))//{{$partner->site_url}}@else javascript:void(0) @endif"  target="_blank">
                                <img src="{{ asset("storage/$partner->image") }}" alt="{{ $partner->name }}">
                            </a>
                        </figure>

                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Partners End -->