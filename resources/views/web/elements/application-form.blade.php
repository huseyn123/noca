<div class="sideform">

    <h4>{{ $dictionary['application_form'] ?? 'Müraciət forması' }}</h4>
    {!! Form::open(['url'=>route('contact'), 'method'=>'POST', 'id' => 'form']) !!}

        <div class="item">
			<span class="span   @if($errors->has('fullname')) error-item @endif">

            {!! Form::text('fullname', null, ["class" => "ipt_style", "placeholder" => '*'.$dictionary['full_name'] ]) !!}

            </span>
        </div>
        <div class="item">
            <span class="span   @if($errors->has('phone')) error-item @endif">

            {!! Form::text('phone', null, ["class" => "ipt_style", "placeholder" => '*'.$dictionary['phone'],'onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57' ]) !!}

            </span>
        </div>
        <div class="item">
            <span class="span   @if($errors->has('message')) error-item @endif">

            {!! Form::textarea('message', null, ["class" => "ipt_style","placeholder" => '*'.$dictionary['text']  ]) !!}

            </span>
        </div>
        <div class="item">

            {!! Form::button($dictionary['send'], ['id' => 'loadingButton', 'data-loading-text' => g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}

        </div>

    {!! Form::close() !!}
    <div id="msgBox"></div>


</div>


    @push('scripts')
        @include('web.elements.form',['type' => 'application_form'])
    @endpush