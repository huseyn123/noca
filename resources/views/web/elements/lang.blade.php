@foreach(config("app.locales") as $key => $locale)
    @if($current == true || $key != $lang)
        <a rel="alternate" hreflang="{{ $key }}" href="@if(isset($menuWithoutSlug)) {{ url($key) }} @else {{ LaravelLocalization::getLocalizedURL($key, $rel[$page->relation_page][$key] ?? $lang, [], true) }} @endif">
            {{ $dictionary[$key] }}
        </a>
    @endif
@endforeach