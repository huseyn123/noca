<section class="main_product">
    <div class="container">
        <div class="row">
            <!-- TitleBlock Begin -->
            <div class="title_block">
                <div class="col-xs-12">
                    <h1>{{ $dictionary['products'] or 'Məhsullarımız' }}</h1>
                </div>
            </div>
            <!-- TitleBlock End -->
        </div>
        <div class="row mt40">

            @foreach($products as $product)

             <div class="col-sm-4">
                    <article>

                        @if($product->image)

                        <figure>
                            <img src="{{ asset("storage/".$product->image->filename) }}" alt="">
                        </figure>

                        @endif

                        <div class="body">
                            <h2>{{ $product->name }}</h2>
                            <a href="{{ $product->forward_url ? $product->forward_url : route('showArticle', [$product->parent_slug, $product->slug]) }}" title="" @if($product->target == 0) target="_blank" @endif">{{ $dictionary['more'] ?? 'DAVAMI' }} >> </a>
                        </div>
                    </article>
                </div>

            @endforeach

        </div>
    </div>
</section>
