@if($cat->galleries->count() > 0 )

<!-- PageGallery Begin -->
<div class="page_gallery">
    <div class="row">
        <h2 class="col-xs-12">{{ $dictionary['photo_gallery'] or 'Foto Qalereya' }}</h2>

        @foreach($cat->galleries as $gallery)

            <div class="col-xs-4 col-mob-6">
                <figure>
                    <a href="{{ asset("storage/$gallery->filename") }}" title="" class="zoom">
                        <img src="{{ asset("storage/thumb/$gallery->filename") }}" alt="">
                        <i class="fa fa-search-plus" aria-hidden="true"></i>
                    </a>
                </figure>
            </div>

        @endforeach

    </div>
</div>
<!-- PageGallery End -->

@endif
