@if(isset($page->image))

<figure>
    <img src="{{asset("storage/".$page->image->filename)}}" alt="{{ $page->name }}">
</figure>

@endif
