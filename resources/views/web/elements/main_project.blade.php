<!-- MainProject Begin -->
<section class="main_project">
    <div class="container">
        <div class="row">
            <!-- TitleBlock Begin -->
            <div class="title_block">
                <div class="col-xs-12">
                    <h1>{{ $dictionary['projects'] or 'Layihələrimiz' }}</h1>
                </div>
            </div>
            <!-- TitleBlock End -->
        </div>
        <div class="row mt60">

            @foreach($projects as $project)

                <div class="col-xs-6 col-mob-12">
                    <article>

                        @if($project->image)
                        <figure>
                            <img src="{{asset("storage/thumb/".$project->image->filename)}}" alt="">
                        </figure>
                        @endif

                        <h2>{{ $project->name }}</h2>
                        <a href="{{ route('showPage',$project->slug) }}" title="">{{ $dictionary['more'] ?? 'DAVAMI' }} >></a>
                    </article>
                </div>

            @endforeach

        </div>
    </div>
</section>
<!-- MainProject End -->