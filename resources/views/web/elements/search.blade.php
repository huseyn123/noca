<div class="search">

<button type="button" class="btn_search"><i class="fa fa-search" aria-hidden=""></i></button>

{!! Form::open(['url'=>route('showPage', 'search'), 'method'=>'GET']) !!}

    {{ Form::text('keyword', null, ['placeholder' => $dictionary['search'] ]) }}

    {!! Form::button('<i class="fa fa-search" aria-hidden=""></i>', ['type'=>'submit']) !!}

{!! Form::close() !!}
</div>
