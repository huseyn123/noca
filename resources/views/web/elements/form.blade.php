<script type="text/javascript">
    function contactForm(action){

        if (action.data('submitted') === true) {
            return false;
        }
        else
        {
            $.ajax({
                url:  action.attr('action'),
                type: action.attr('method'),
                //data: action.serialize(),
                data: new FormData(action[0]),
                dataType: "json",
                processData: false,
                contentType: false,
                beforeSend: function(response) {
                    $("#loadingButton").button('loading');
                },

                success: function(response){

                    $("#loadingButton").button('reset');

                    if(response.success == 1) {

                        $('button[type=submit]').attr('disabled',true);

                        action.data('submitted', true);

                        //$("#divForm").toggle();
                        //$("#successBox").show();
                        $("#errorBox").remove();
                        $("#msgBox").html(response.msg);

                        setTimeout(function () { $(".perspective_inner").stop(true, true).removeClass("show"); }, 4000);

                    }
                    else{
                        $("#errorBox").show();
                        $("#msgBox").html(response.msg);
                    }

                },

                error: function(res){
                    $("#loadingButton").button('reset');
                    $("#errorBox").show();
                    $("#msgBox").html('Unexpected Error!');
                }
            });
        }
    }

$(function(){

    $("#form").validate({
            rules: {
                fullname: {
                    required: true,
                        lettersonly: true,
                        minlength: 5,
                        maxlength: 40
                },
                @if($type == 'contact')
                email: {
                    required: true,
                    email: true
                },
                @endif
                phone: {
                    required: true,
                        minlength:9,
                        maxlength:15
                },
                @if($type == 'contact')
                subject: {
                    required: true,
                    lettersonly: true,
                    minlength: 3,
                    maxlength: 40
                },
                @endif
                message: {
                    required: true,
                    lettersonly: true,
                    minlength: 5,
                    maxlength: 100
                }
        },

    success: function(label) {
            label.html('').removeClass('error').addClass('ok');
        },
    errorPlacement: function(error, element) {
            @if(!isset($withoutMsg))
                    $(element).parents('.span').addClass( "error-item" );
            @endif
            },
    highlight: function ( element, errorClass, validClass ) {
            $(element).parents('.span').addClass( "error-item" ).removeClass( "valid-item" );
        },
    unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.span').addClass( "valid-item" ).removeClass( "error-item" );
        },
        submitHandler: function (form) {
            contactForm($(form));
            return false;
        }
});

    jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

});

</script>