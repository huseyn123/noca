@if($type == 'header' || $type == 'mobile')
    <ul class="list-unstyled">
        @foreach($menu as $m)

            @if($m->parent_id == $parent && !in_array($m->visible, $hidden) && $m->lang_id == $lang)
                <li @if(isset($page)) @if(($m->parent_id == null && $m->slug == $page->slug) ||($m->id == $page->parent_id))  class="active" @endif @endif>
                    <a href="@if($m->parent_id == null && $m->template_id == 6) # @else {{ route('showPage', $m->slug) }} @endif" @if($m->target == 0) target="_blank" @endif>{{ $m->name }} @if($m->parent_id == null && $type == 'mobile' && $m->template_id == 6) <i class="fa fa-plus" aria-hidden="true"></i> @endif</a>
                    @if($sub == true && $m->template_id == 6)
                        @include('web.elements.menu', ['parent' => $m->id, 'lang' => $lang, 'hidden' => [0], 'sub' => false, 'type' => $type])
                    @endif
                </li>
            @endif
        @endforeach
    </ul>
@else
    @foreach($menu as $m)
        @if($m->parent_id == $parent && !in_array($m->visible, $hidden) && $m->lang_id == $lang)
            <div class="col-md-2 col-sm-3 hidden-xs ">
                <!-- FooterMenu -->
                <nav class="footer_menu">
                    <h4>{{ $m->name }}</h4>
                    @if($sub == true)
                        <ul class="list-unstyled">
                            @include('web.elements.menu', ['parent' => $m->id, 'lang' => $lang, 'hidden' => [0], 'sub' => false, 'type' => 'header'])
                        </ul>
                    @endif
                </nav>
            </div>
        @endif
    @endforeach
@endif

@push('scripts')
<script>

    $('.mobile_menu > ul > li > ul > li').each(function() {

        var sub = $(this).parent().parent();
        sub.addClass('sub');
    });


</script>
@endpush
