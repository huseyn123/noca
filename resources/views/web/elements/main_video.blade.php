<!-- MainVideo Begin -->
<section class="main_video">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-6 col-xs-12">
                <!-- Caption -->
                <div class="caption">
                    <h2>{{ $dictionary['main_video_title'] or 'Qaz Turbin İstilik Elektrik stansiyası' }}</h2>
                    <p>{{ $dictionary['main_video_desc'] or 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco' }} </p>
                </div>
            </div>

            @if($webConfig->main_video)

            <div class="col-md-4 col-md-offset-2 col-sm-5 col-sm-offset-0 col-xs-8 col-xs-offset-2">
                <figure>
                    <a href="https://www.youtube.com/watch?v={{$webConfig->main_video}}" title="" class="zoom"></a>
                        <img src="{{ asset("storage/$webConfig->main_video_cover") }}" alt="">
                </figure>
            </div>

            @endif

        </div>
    </div>
</section>
<!-- MainVideo End -->