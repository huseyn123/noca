<!-- MainServices Begin -->
<section class="main_services">
    <div class="container">
        <div class="row">
            <!-- TitleBlock Begin -->
            <div class="title_block">
                <div class="col-xs-12">
                    <h1>{{ $dictionary['services'] or 'Xidmətlərimiz' }}</h1>
                </div>
            </div>
            <!-- TitleBlock End -->
        </div>
        <div class="row mt60">

            @foreach($services as  $service)

            <div class="col-md-3 col-sm-6 col-xs-12">
                    <article>
                        <a href="{{ route('showPage', $service->slug) }}" title="">
                            <figure>
                                <img src="{{ url("images/delete/$service->icon.svg") }}" alt="" class="svg">
                            </figure>
                            <h2>{{ $service->summary }}</h2>
                        </a>
                    </article>
            </div>

            @endforeach

        </div>
    </div>
</section>
<!-- MainServices End -->