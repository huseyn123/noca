<!-- Header Begin -->
<header id="header">
    <!-- HeaderInner Begin -->
    <div class="header_inner">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-xs-12">
                    <!-- Logo -->
                    <div class="logo">
                        <a href="{{ route('home') }}"><img src="{{ asset('images/new_logo.png') }}"></a>
                        <span>{{ $dictionary['title'] or 'Nəzarət Ölçü Cihazları və Avtomatika' }}</span>
                    </div>
                    <!-- MobileShow -->
                    <div class="mobile_show">
                        <!-- BtnSearch -->

                        <div class="search">
                            @include('web.elements.search')
                        </div>

                        <!-- Lang -->
                        <nav class="lang">
                            @include('web.elements.lang', ['current' => false])
                        </nav>
                        <!-- BtnMenu -->
                        <button type="button" class="btn_menu"><i class="fa fa-bars" aria-hidden=""></i></button>
                    </div>
                </div>
                <div class="col-md-7 hidden-sm hidden-xs">
                    <!-- HeaderContact -->
                    <div class="header_contact">
                        <span><i class="fa fa-envelope" aria-hidden=""></i>{{ $webConfig->email }}</span>
                        <span><strong><i class="fa fa-phone" aria-hidden=""></i>{{ $webConfig->contact_phone }}</strong></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- HeaderInner End -->
    <!-- MobileHeaderContact Begin -->
    <div class="mh_contact">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <span class="pull-left"><i class="fa fa-envelope" aria-hidden=""></i>{{ $webConfig->contact_form_email }}</span>
                    <span class="pull-right"><strong><i class="fa fa-phone" aria-hidden=""></i>{{ $webConfig->contact_phone }}</strong></span>
                </div>
            </div>
        </div>
    </div>
    <!-- MobileHeaderContact End -->
    <!-- NavBar Begin -->
    <div class="nav_bar">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Menu -->
                    <nav class="menu">

                        @include('web.elements.menu', ['parent' => null, 'lang' => $lang, 'hidden' => [0,3], 'sub' => true, 'type' => 'header'])

                    </nav>



                    <div class="pull-right">
                    @include('web.elements.search')
                        <!-- Lang -->
                        <nav class="lang">
                            @include('web.elements.lang', ['current' => false])
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- NavBar End -->
</header>
<!-- Header End -->
