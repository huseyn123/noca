<!-- Breadcrumbs Begin -->
<section class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="pull-right">
                    {!! Breadcrumbs::render('web', $page, $dictionary['home_page'], isset($articleSingle) ? $articleSingle : false) !!}
                </div>

            </div>
        </div>
    </div>
</section>
<!-- Breadcrumbs End -->
