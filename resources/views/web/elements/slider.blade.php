<!-- Slider Begin -->
<section id="slider" class="owl-carousel">

    @foreach($sliders as $slider)

    <article>
        <img src="{{ asset("storage/$slider->image") }}" alt="">
        <div class="caption">
            <div class="vertical-center">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>{{ $slider->title }}</h2>
                            <p>{{ $slider->summary }}</p>
                            @if(!is_null($slider->link))
                                <a href="//{{$slider->link}}" title="{{ $slider->title }}" target="_blank">{{ $dictionary['read_more'] }}</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>

    @endforeach

</section>
<!-- Slider End -->
