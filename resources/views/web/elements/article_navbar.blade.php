@if($page->otherArticles($article->id)->count() )

<div class="block">
    <div class="head">
        <h1>{!! $dictionary['similar_news'] or 'Digər xəbərlər' !!}</h1>
    </div>
    <!-- Other News -->
    <div class="other_news">

        @foreach($page->otherArticles($article->id)->get() as $other)

            <article>
                <span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> {{  blogDate($article->published_at) }}</span>
                <h2><a href="{{ route('showArticle', [$page->slug, $other->article_slug]) }}" title="{{ $other->title  }}"> {{ $other->title  }} </a></h2>
                <p> {{ $other->summary }} </p>
            </article>

        @endforeach

    </div>
</div>

@endif


@if($projects)

    <div class="block">
        <div class="head">
            <h1>Layihələr</h1>
        </div>
        <!-- SideProject -->
        <div class="side_project clearfix">

            @foreach($projects as $project)

                <article>

                    <div class="body">
                        <h2><a href="{{ route('showPage',$project->slug) }}" title="">{{ $project->name }} </a></h2>
                        {!! $project->summary !!}

                    </div>

                </article>

            @endforeach


        </div>

    </div>

@endif