<!-- Footer Begin -->
<footer id="footer">
    <div class="container">
        <div class="row">


            {{--<div class="col-md-2 col-sm-3 hidden-xs">--}}
                {{--<!-- FooterMenu -->--}}
                {{--<nav class="footer_menu">--}}
                    {{--<h4>BÖLMƏLƏR</h4>--}}
                    {{--<ul class="list-unstyled">--}}

                        {{--@foreach( $menu as $cat )--}}

                            {{--@if($cat->parent_id == null && $cat->slug != 'index')--}}
                                {{--<li><a href="{{ route('showPage', $cat->slug) }}" title="{{ $cat->name }}">{{ $cat->name }}</a></li>--}}
                            {{--@endif--}}

                        {{--@endforeach--}}

                    {{--</ul>--}}
                {{--</nav>--}}
            {{--</div>--}}

            @include('web.elements.menu', ['parent' => null, 'lang' => $lang, 'hidden' => [0,2], 'sub' => true, 'type' => 'footer'])

            <div class="col-md-3 col-xs-12 pull-right">
                <!-- FooterContact -->
                <div class="footer_menu pull-right">
                    <h4>{{ $dictionary['contact_tools']}}</h4>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-envelope fa-fw"></i>{{ $webConfig->email }}</li>
                        <li><i class="fa fa-envelope fa-fw"></i>{{ $webConfig->hr_form_email }}</li>
                        <li><i class="fa fa-phone fa-fw"></i>{{ $webConfig->contact_phone }}</li>
                        <li><i class="fa fa-map-marker fa-fw"></i>{{ $dictionary['address'] }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-3">
                    <!-- FooterLogo -->
                    <div class="footer_logo">
                        <a href="{{ route('home') }}"><img src="{{ asset('images/noca-white.png') }}"></a>
                        <span>Nəzarət Ölçü Cihazları və Avtomatika</span>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-9">
                    <p>{!! trans('locale.site_by', ['site' => '<a href="http://marcom.az" target="_blank">Marcom</a>']) !!}</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->