<!-- MobileMenu Begin -->
<nav class="mobile_menu">

    @include('web.elements.menu', ['parent' => null, 'lang' => $lang, 'hidden' => [0], 'sub' => true, 'type' => 'mobile'])

</nav>
<!-- MobileMenu End -->