@extends ('layouts.web', [ 'page_heading' => $pageTitle ] )

@section ('content')

    @include('web.elements.breadcrumbs')

    <!-- Contact Begin -->
    <section class="contact">
        <div id="contactmap"></div>
        <div class="container">
            <div class="row">
                {{--<div class="col-sm-1 col-xs-12">--}}
                    {{--<div class="contact_info">--}}
                        {{--<div class="block">--}}
                            {{--<h1>{!!   categoryName($dictionary['contact_tools']) !!}</h1>--}}
                            {{--<ul class="list-unstyled">--}}
                                {{--<li><i class="fa fa-phone fa-fw"></i>{{ $webConfig->contact_phone }}</li>--}}
                                {{--<li><i class="fa fa-envelope fa-fw"></i>{{ $webConfig->email }}</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--<div class="block">--}}
                            {{--<h1> <strong>{{ $dictionary['address_title'] or 'Ünvan' }} </strong></h1>--}}
                            {{--<ul class="list-unstyled">--}}
                                {{--<li><i class="fa fa-map-marker fa-fw"></i>{{ $dictionary['address'] }}</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="col-sm-12 col-xs-12">
                    <div class="contact_form row">
                        {!! Form::open(['url'=>route('contact'), 'method'=>'POST', 'id' => 'form']) !!}
                            <h1 class="col-xs-12">{!!  categoryName($dictionary['contact_us']) !!}</h1>
                            <div class="col-xs-6 col-mob-12">
							<span class="span @if($errors->has('fullname')) error-item @endif">
                                {!! Form::text('fullname', null, ["placeholder" => $dictionary['full_name'],'class' => 'ipt_style alphaonly'  ]) !!}
							</span>
                            </div>
                            <div class="col-xs-6 col-mob-12">
							<span class="span  @if($errors->has('email')) error-item @endif">
                                {!! Form::text('email', null, ["placeholder" => $dictionary['email'],'class' => 'ipt_style']) !!}
							</span>
                            </div>
                            <div class="col-xs-6 col-mob-12">
							<span class="span  @if($errors->has('phone')) error-item @endif">
                                {!! Form::text('phone', null, ["placeholder" => $dictionary['phone'],'class' => 'ipt_style','onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57']) !!}
							</span>
                            </div>

                            <div class="col-xs-6 col-mob-12">
							<span class="span  @if($errors->has('subject')) error-item @endif">
                                {!! Form::text('subject', null, ["placeholder" => $dictionary['subject'],'class' => 'ipt_style']) !!}
							</span>
                            </div>

                            <div class="col-xs-12">
							<span class="span  @if($errors->has('message')) error-item @endif">
                                {!! Form::textarea('message', null, ["placeholder" => $dictionary['text'],'class' => 'ipt_style']) !!}
							</span>
                            </div>

                            <div class="col-xs-12 text-center">
                                {!! Form::button($dictionary['send'], ['id' => 'loadingButton', 'data-loading-text' => g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <div id="msgBox"></div>

                </div>

            </div>
        </div>
    </section>
    <!-- Contact End -->

@endsection

@push('scripts')

    @include('web.elements.form',['type' => 'contact'])

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ $webConfig->google_api_key }}&language={{ $lang }}"></script>

    @if(!is_null($webConfig->location ))
        <script type="text/javascript">
            google.maps.event.addDomListener(window, 'load', init);
            function init() {
                var markericon = {
                    url: 'images/icon/map-marker.png',
                    //state your size parameters in terms of pixels
                    size: new google.maps.Size(80, 80),
                    scaledSize: new google.maps.Size(80, 80),
                    origin: new google.maps.Point(0,0)
                }

                var mapOptions = {
                    zoom: 12,
                    center: new google.maps.LatLng({{ $webConfig->location }}),
                    styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#f7f7f7"},{"visibility":"on"}]}]
                };

                var mapElement = document.getElementById('contactmap');
                var map = new google.maps.Map(mapElement, mapOptions);

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng({{ $webConfig->location }}),
                    map: map,
                    icon: markericon
                });

                var myoverlay = new google.maps.OverlayView();
                myoverlay.draw = function () {
                    this.getPanes().markerLayer.id='markerLayer';
                };
                myoverlay.setMap(map);
            }
        </script>
    @endif

@endpush