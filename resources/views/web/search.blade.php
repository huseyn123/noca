@extends ('layouts.web', ['page_heading' => $dictionary['search_page'] ?? 'Search results'])

@section ('content')

	<!-- Page Begin -->
	<section class="news">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-xs-12">
					<!-- SearchPage Begin -->
					<div class="search_page">
						<span class="keyword"><em>{{ $keyword }}</em> sözü üzrə <em> {{ $results->count() }} </em> nəticə tapıldı</span>


						<div class="result_list">
							@if($results->count() > 0)

								@foreach($results as $result)

									@if( $result->type == 2)

										<article class="row">
											<figure class="col-md-4 col-sm-5 col-xs-12">
												<img src="{{ asset('storage/thumb/'.$result->image) }}" alt="">
											</figure>
											<div class="col-md-8 col-sm-7 col-xs-12">
												<h2><a href="{{route('showArticle', [$result->category, $result->slug])}}" title="">{{ $result->summary }}</a></h2>
												<span>{{ mediaFullDate($result->published_at) }}</span>
												{!!  str_limit( $result->content , 197 ) !!}
											</div>
										</article>

									@else


										<article class="row">

											@if($result->image)
												<figure class="col-md-4 col-sm-5 col-xs-12">
													<img src="{{ asset('storage/'.$result->image) }}" alt="">
												</figure>
											@endif

											<div class="col-md-8 col-sm-7 col-xs-12">
												@if($result->template_id == 2)
													<h2><a href="{{route('showArticle', [$result->category,$result->slug])}}" title="">{{ $result->name }}</a></h2>
												@else
													<h2><a href="{{route('showPage', [$result->slug])}}" title="">{{ $result->name }}</a></h2>
												@endif

													{!!  str_limit( $result->content , 200 ) !!}
											</div>
										</article>


									@endif

								@endforeach

							@endif

						</div>
					</div>
					<!-- SearchPage End -->
				</div>
			</div>
		</div>
	</section>
	<!-- Page End -->

@endsection