@extends ('layouts.web', [ 'page_heading' => $page->title ] )

@section ('content')

    @include('web.elements.breadcrumbs', ['articleSingle' => $product])

    <!-- Page Begin -->
    <section class="page">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">

                @include('web.elements.site_navbar', ['cat' => $cat,'form' => true ])

                <!-- Download -->
                    @if($product->image_file)

                        <a href="{{ asset("storage/pdf/$product->image_file") }}" title="" class="adownload" download><i></i> <span>{{$dictionary['information_book'] or 'Məhsul məlumat kitabını yüklə'}}</span></a>

                    @endif
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <!-- PageDetail Begin -->
                    <div class="page_detail">
                        <h2 class="title">{{ $product->name }}</h2>
                        <div class="text">

                            {!! $product->content !!}

                        </div>
                    </div>
                    <!-- PageDetail End -->
                    <!-- PageFaq Begin -->
                    <div class="page_faq">
                        <div class="section">
                            <div class="faq_list">

                                @foreach($product->children as $info)

                                    <div class="block">
                                        <div class="head">
                                            <h2>{{ $info->name }}</h2>
                                        </div>
                                        <div class="body">

                                         {!! $info->content !!}

                                        </div>
                                    </div>

                                @endforeach

                            </div>
                        </div>
                    </div>
                    <!-- PageFaq End -->

                    <!-- PageGallery Begin -->
                     @include('web.elements.page-gallery',['cat' => $product] )
                    <!-- PageGallery End -->

                </div>
            </div>
        </div>
    </section>
    <!-- Page End -->


@endsection

