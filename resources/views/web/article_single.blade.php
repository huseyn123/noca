@extends ('layouts.web', [ 'page_heading' => $article->title ] )

@section ('content')

    @include('web.elements.breadcrumbs', ['articleSingle' => $article])


    <!-- News Begin -->
    <section class="news">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8">
                    <!-- NewsDetail Begin -->
                    <div class="news_detail">
                        <figure>
                            <img src="{{ asset("storage/{$article->image->filename}")}}" alt="{{ $article->title }}">
                        </figure>
                        <span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> {{  blogDate($article->published_at) }}</span>
                        <h2 class="title">{{ $article->title }}</h2>
                        <div class="text">
                            {!! $article->content !!}
                        </div>
                    </div>
                    <!-- NewsDetail End -->
                </div>
                <div class="col-md-3 col-sm-4">
                    <!-- SideNews Begin -->
                    <aside class="side_news">

                        @include('web.elements.article_navbar')

                    </aside>
                    <!-- SideNews End -->

                </div>
            </div>
        </div>
    </section>
    <!-- News End -->


@endsection

