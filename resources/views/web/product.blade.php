@extends ('layouts.web', [ 'page_heading' => $pageTitle ] )

@section ('content')

    @include('web.elements.breadcrumbs')

    <!-- Page Begin -->
    <section class="page">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">

                    @include('web.elements.site_navbar', ['cat' => $cat,'form' => true ])

                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <!-- ProductList Begin -->
                    <div class="product_list row">

                        @foreach($page->children as $product)

                            <div class="col-sm-4 col-xs-6 col-mob-12">
                                <article>
                                    <figure>
                                        @if($product->image)

                                            <img src="{{ asset("storage/".$product->image->filename) }}" alt="{{ $product->name }}">

                                        @endif
                                    </figure>
                                    <h2><a href="{{ route('showArticle', [$page->slug, $product->slug]) }}" title="">{{ $product->name }}</a></h2>
                                </article>
                            </div>

                        @endforeach

                    </div>
                    <!-- ProductList End -->
                </div>
            </div>
        </div>
    </section>
    <!-- Page End -->

@endsection
