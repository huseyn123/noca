@extends ('layouts.web', [ 'page_heading' => null,'body' => 'main' ] )

@section ('content')

    @if($sliders->count() > 0)
        @include('web.elements.slider')
    @endif

    @if($services->count() > 0)
    @include('web.elements.main_services')
    @endif

    @if($products->count() > 0)
    @include('web.elements.main_product')
    @endif

    @if($projects->count() > 0))
    @include('web.elements.main_project')
    @endif

    @include('web.elements.main_video')

    @if($partners->count() > 0)
    @include('web.elements.partners')
    @endif

@endsection
