@extends ('layouts.web', [ 'page_heading' => $pageTitle ] )

@section ('content')

    @include('web.elements.breadcrumbs')

    <!-- News Begin -->
    <section class="news">
        <div class="container">
            <div class="row">
                <!-- NewsList Begin -->

                <div class="news_list clearfix">

                    @foreach($page->articles(9) as $news)

                    <div class="col-sm-4 col-xs-6 col-mob-12">
                        <article>
                            <figure>
                                <img src="{{ asset('storage/thumb/'.$news->image->filename) }}" alt="">
                            </figure>
                            <h2><a href="{{ route('showArticle', [$page->slug, $news->slug]) }}" title="">{{ $news->title }}</a></h2>
                            <p>{{ str_limit( $news->summary , 150 ) }}</p>
                        </article>
                    </div>

                    @endforeach

                </div>
                <!-- NewsList End -->

                {{--<!-- Pagination Begin -->--}}
                <nav class="pag col-xs-12">
                    {{ $page->articles(9)->links() }}
                </nav>
                {{--<!-- Pagination End -->--}}

            </div>
        </div>
    </section>
    <!-- News End -->

@endsection

