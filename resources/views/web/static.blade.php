@extends ('layouts.web', [ 'page_heading' => $pageTitle ] )

@section ('content')

    @include('web.elements.breadcrumbs')

    <!-- Page Begin -->
    <section class="page">
        <div class="container">
            <div class="row">


                <div class="col-md-3 col-sm-4 col-xs-12">

                @include('web.elements.site_navbar', ['cat' => $cat,'form' => true ])

                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <!-- PageDetail Begin -->
                    <div class="page_detail">

                       @include('web.elements.page-cover')

                        <h2 class="title">@if($page->template_id == 11){{ $page->name }} @else {{ $page->summary }} @endif</h2>
                        <div class="text">

                         {!! $page->content  !!}

                        </div>
                    </div>
                    <!-- PageDetail End -->


                    <!-- HistoryBlock Begin -->
                    <div class="history_block row">

                        @if($page->children())

                            @foreach($page->children as $block)

                            <div class="col-sm-6 col-xs-12">
                                <article>

                                    @if($block->image)

                                    <figure>
                                        <img src="{{ asset('storage/'.$block->image->filename) }}" alt="">
                                    </figure>

                                    @endif

                                    <div class="body">
                                        <h2>{{ $block->name }}</h2>
                                    </div>
                                </article>
                            </div>

                            @endforeach

                        @endif

                    </div>
                    <!-- HistoryBlock End -->

                    @if($page->galleries->count() >0)

                        @include('web.elements.page-gallery',['cat' => $page])

                    @endif

                </div>

            </div>
        </div>
    </section>
    <!-- Page End -->

@endsection
