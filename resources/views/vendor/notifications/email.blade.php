@component('mail::message')
    {{-- Greeting --}}
    @if (! empty($greeting))
        # {{ $greeting }}
    @else
        @if ($level == 'error')
            # Whoops!
        @else
            # Salam!
        @endif
    @endif

    {{-- Intro Lines --}}
    @foreach ($introLines as $line)
        {{-- {{ $line }} --}}
        Şifrəni dəyişdirmək üçün, aşağıdakı göstərilən linki tıklayın.

    @endforeach

    {{-- Action Button --}}
    @isset($actionText)
        <?php
        switch ($level) {
            case 'success':
                $color = 'green';
                break;
            case 'error':
                $color = 'red';
                break;
            default:
                $color = 'blue';
        }
        ?>
        @component('mail::button', ['url' => $actionUrl, 'color' => $color])
            {{-- {{ $actionText }} --}}
            Şifrəni dəyiş
        @endcomponent
    @endisset

    {{-- Outro Lines --}}
    @foreach ($outroLines as $line)
        {{-- {{ $line }} --}}
        Əgər, siz şifrəni dəyişmək üçün müraciət etməmisinizsə, bu linkə əhəmiyyət verməyin.
    @endforeach

    <!-- Salutation -->
    @if (! empty($salutation))
        {{ $salutation }}
    @else
        Hörmətlə,<br>{{ config('app.name') }}
    @endif

    <!-- Subcopy -->
    @isset($actionText)
        @component('mail::subcopy')
            Əgər yuxarıdakı linkə daxil olmaqda probleminiz olsa, bu linkdən istifadə edə bilərsiz: [{{ $actionUrl }}]({{ $actionUrl }})
        @endcomponent
    @endisset
@endcomponent
