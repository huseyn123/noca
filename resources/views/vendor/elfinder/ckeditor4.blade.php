@extends ('planes.app')
@section('main-title', 'File Manager')
@section ('layout')
    <div id="wrapper">
        <div class="row">
            <div id="elfinder"></div>
            <br>
        </div>
    </div>
@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/elfinder/css/elfinder.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/elfinder/css/theme.css') }}">
@endpush

@push('scripts')
<script src="{{asset('vendor/elfinder/js/elfinder.min.js') }}"></script>

<script src="{{asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script>

<script type="text/javascript" charset="utf-8">
    // Helper function to get parameters from the query string.
    function getUrlParam(paramName) {
        var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
        var match = window.location.search.match(reParam) ;

        return (match && match.length > 1) ? match[1] : '' ;
    }

    $().ready(function() {
        var funcNum = getUrlParam('CKEditorFuncNum');

        var elf = $('#elfinder').elfinder({
            onlyMimes: ['image'], // restricts to images
            customData: {
                _token: '<?= csrf_token() ?>'
            },
            url: '<?= route("elfinder.connector") ?>',  // connector URL
            getFileCallback : function(file) {
                window.opener.CKEDITOR.tools.callFunction(funcNum, file.url);
                window.close();
            }
        }).elfinder('instance');
    });


</script>
@endpush