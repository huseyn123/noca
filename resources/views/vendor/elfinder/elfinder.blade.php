@extends ('layouts.app')
@section ('page_heading', 'File Manager')
@section ('content')

    <div class="col-md-12">
        <div id="elfinder"></div>
        <br>
    </div>

@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/elfinder/css/elfinder.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/elfinder/css/theme.css') }}">
@endpush

@push('scripts')
    <script src="{{asset('vendor/elfinder/js/elfinder.min.js') }}"></script>

    <script src="{{asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script>

    <script type="text/javascript" charset="utf-8">
        $().ready(function() {
            $('#elfinder').elfinder({
                customData: {
                    _token: '{{ csrf_token() }}'
                },
                url : '{{ route("elfinder.connector") }}'  // connector URL
            });
        });
    </script>
@endpush