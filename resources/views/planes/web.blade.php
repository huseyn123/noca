<!DOCTYPE html>
<html lang="{{$lang}}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="theme-color" content="#ffffff">
    <meta property="author" content="{{ $webConfig->company_name }}">
    <meta property="og:title" content="{{ $page_heading }}">
    <meta property="og:description" content="{{ isset($page_description) ? strip_tags($page_description) : @$page->meta_description }}">
    <meta property="og:url" content="{{request()->url()}}">
    <meta property="og:image" content="{{isset($page->image) ? asset($page->image->filename):  asset('images/logo.png')}}">
    @if(isset($page))
        <meta name="keywords" content="{{ $page->meta_keywords }}">
        <meta name="description" content="{{ $page->meta_description }}">
    @endif
    <title>@if(!is_null($page_heading)){{ $page_heading }} | @endif{{ $webConfig->company_name }}</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('images/favicon/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('images/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="theme-color" content="#ffffff">



    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700&text=%21%22%23%24%25%26%27%28%29%30+,-./0123456789:;%3C=%3E%3F@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`%E2%82%AC„‘’“”™©®°µ±÷abcdefghijklmnopqrstuvwxyz{|}~%C3%9C%C3%96%C4%9E%C4%B0%C6%8F%C3%87%C5%9E%C3%BC%C3%B6%C4%9F%C4%B1%C9%99%C3%A7%C5%9F" rel="stylesheet">
    <link href="{{ asset(mix('css/style.css')) }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @include('widgets.analytic', ['code' => $webConfig->analytic_id ])

</head>
<body class="{{ $body or 'inside' }}">

@yield('layout')

<script src="{{asset(mix('js/script.js'))}}"></script>

@stack('scripts')
</body>
</html>
