@extends ('layouts.app')
@section ('page_heading', $title)

@section ('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-1">

            @section('settings')
                <table class="table table-bordered table-striped">
                    @foreach($smtp as $value)
                        <tr>
                            <td>{{ $value->key }}</td>
                            <td>@if($value->key == 'password') ****** @else {{ $value->value  }} @endif</td>
                            <td>
                                @if(auth()->user()->role == 1)
                                    <a href="#" class="btn btn-xs btn-primary open-modal-dialog" data-action="{{route('smtp.edit', $value->id)}}">
                                        {!! icon('edit') !!}
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            @endsection

            @include('widgets.settings-panel')
        </div>
    </div>

@endsection