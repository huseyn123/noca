@extends(request()->ajax() ? 'layouts.modal' : 'layouts.app', ['form' => true, 'script' => true] )
@section('title', $smtp->key)
@section('class', 'col-md-6 col-md-offset-3')
@section('form', Form::open(array('route'=>['smtp.update', $smtp->id], 'method'=>'PATCH', 'class'=>'form-horizontal')))

@section('button', Form::button(trans('locale.save'), ['class' => 'btn btn-success', 'type' => 'submit']))

@section('content')

    <div class="form-group">
        <label for="type" class="col-md-4 control-label">{{ $smtp->key }}</label>
        <div class="col-md-6">
            @if($type == 'password')
                <input type="password" name="value" class="form-control">
            @else
                <input type="text" name="value" class="form-control" value="{{ $smtp->value }}">
            @endif
        </div>
    </div>

@endsection