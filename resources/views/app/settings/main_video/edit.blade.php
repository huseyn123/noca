@extends(request()->ajax() ? 'layouts.modal' : 'layouts.app', ['form' => true, 'script' => true] )
@section('title', $title)
@section('class', 'col-md-6 col-md-offset-3')
@section('form', Form::open(array('route'=>['main_video.update', $id], 'method'=>'PATCH', 'files' => true, 'class'=>'form-horizontal')))

@section('button', Form::button(icon('save', trans('locale.save')), ['class' => 'btn btn-success', 'type' => 'submit']))

@section('content')

    {!! $fields !!}

@endsection
@push('script')
<script>

    $('#main_video_cover').on('fileselect', function(event, numFiles, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        }

    });

    $('#main_video_cover').on('change',function(e){

        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);

    });


</script>
@endpudh