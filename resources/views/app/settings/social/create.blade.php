@extends(request()->ajax() ? 'layouts.modal' : 'layouts.app', ['form' => true, 'script' => true] )
@section('title', 'Sosial şəbəkə')
@section('class', 'col-md-6 col-md-offset-3')
@section('form', Form::open(array('route'=>'social.store', 'method'=>'POST', 'class'=>'form-horizontal')))

@section('button', Form::button(trans('locale.create'), ['class' => 'btn btn-primary', 'type' => 'submit']))

@section('content')

    {!! $fields !!}

@endsection