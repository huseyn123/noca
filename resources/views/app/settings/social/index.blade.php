@extends ('layouts.app')
@section ('page_heading', $title)

@section ('content')

    @include('widgets.modal-confirm')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            @section('settings')
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Şəbəkə</th>
                        <th>Ünvan</th>
                        <th>Əməliyyat</th>
                    </tr>

                    @forelse($social as $links)
                    <tr>
                        <td>{!! icon($links->type) !!} {{ config("config.social-network.".$links->type) }}</td>
                        <td><a href="{!! $links->forward_url !!}" target="_blank">{!! $links->forward_url !!}</a></td>
                        <td>
                            @if(auth()->user()->role == 1)

                            <a href="{{route('social.edit', $links->id)}}" class="btn btn-sm btn-primary open-modal-dialog" data-action="{{route('social.edit', $links->id)}}">
                                {!! icon('edit') !!}
                            </a>

                            <a href="#" class="btn btn-sm btn-danger"  data-action="{{route('social.destroy', $links->id)}}" data-toggle="modal" data-target="#modal-confirm">
                                {!! icon('trash') !!}
                            </a>

                            @endif
                        </td>
                    @empty
                        <th colspan="3">Nəticə tapılmadı</th>
                    </tr>
                    @endforelse

                    @if(auth()->user()->role == 1)
                    <tr>
                        <td colspan="3" align="center">
                            <a href="{{route('social.create')}}" data-action="{{route('social.create')}}" class="btn btn-primary btn-lg open-modal-dialog">{!! icon('plus', trans('locale.create')) !!}</a>
                        </td>
                    </tr>
                    @endif
                </table>
            @endsection

            @include('widgets.settings-panel')
        </div>
    </div>

@endsection