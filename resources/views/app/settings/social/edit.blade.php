@extends(request()->ajax() ? 'layouts.modal' : 'layouts.app', ['form' => true, 'script' => true] )
@section('title', $title)
@section('class', 'col-md-6 col-md-offset-3')
@section('form', Form::open(array('route'=>['social.update', $id], 'method'=>'PATCH', 'class'=>'form-horizontal')))

@section('button', Form::button('Update', ['class' => 'btn btn-primary', 'type' => 'submit']))

@section('content')

    {!! $fields !!}

@endsection