@extends(request()->ajax() ? 'layouts.modal' : 'layouts.app', ['form' => true, 'script' => true] )
@section('title', $title)
@section('class', 'col-md-6 col-md-offset-3')
@section('form', Form::open(array('route'=>['config.update', $id], 'method'=>'PATCH', 'class'=>'form-horizontal')))

@section('button', Form::button(icon('save', trans('locale.save')), ['class' => 'btn btn-success', 'type' => 'submit']))

@section('content')

    {!! $fields !!}

@endsection