@extends ('layouts.app', ['script' => true])
@section ('page_heading', $info->name)

@section('content')

    @include('widgets.modal-confirm', ['trash' => true])
    @include('widgets.modal-confirm-image')


    <ul class="nav nav-tabs">
        <li class="active"><a href="#pages" data-toggle="tab"><i class="fa fa-file"></i> Məzmunlar</a></li>
    </ul>

    <div class="tab-content">
        <div id="pages" class="tab-pane in active"><br>
            <div class="col-lg-10 col-lg-offset-1">

                <ul class="nav nav-tabs">
                    @foreach(config('app.locales') as $key => $locale)
                        <li @if($key == $info->lang_id) class="active" @endif><a href="#{{$key}}" data-toggle="tab"> {{ $key }}</a></li>
                    @endforeach
                </ul>

                <div class="tab-content">

                    @foreach($relatedPage as $relPage)

                        <div id="{{ $relPage->lang_id }}" class="tab-pane @if($relPage->lang_id == $info->lang_id) in active @endif">

                            {!! Form::open(['url'=>route("$route.update", $relPage->id), 'method'=>'PATCH', 'class' => 'actionPage']) !!}

                            {!! $fields[$relPage->lang_id] !!}

                            {!! Form::hidden('lang_id', $relPage->lang_id) !!}

                            @include('widgets.form-submit', ['id' => $relPage->id, 'text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save', 'del' => true, 'route' => $route])

                            {!! Form::close() !!}

                        </div>

                    @endforeach

                    @foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale)

                        <div id="{{$key}}" class="tab-pane">

                            {!! Form::open(['url'=>route("$route.store"), 'method'=>'POST', 'class' => 'actionPage']) !!}

                            {!! $fields[$key] !!}

                            {!! Form::hidden('lang_id', $key) !!}

                            {!! Form::hidden('relation_page', $info->id) !!}

                            @include('widgets.form-submit', ['id' => 0, 'text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save', 'del' => true, 'route' => 'page'])

                            {!! Form::close() !!}

                        </div>

                    @endforeach

                </div>

            </div>

        </div>
    </div>

@endsection

@push('scripts')
<script src="{{ asset('vendor/ckeditor/ckeditor.js')}}"></script>

<script>

    @foreach(config('app.locales') as $key => $locale)

CKEDITOR.replace('editor-{{ $key }}');

    $('#meta_keywords_{{$key}}').select2({
        data: {!! json_encode($keys[$key]) !!},
        tags: true,
        maximumSelectionLength: 10

    }).on("change", function(e) {
        var isNew = $(this).find('[data-select2-tag="true"]');
        if(isNew.length){
            isNew.replaceWith('<option selected value="'+isNew.val()+'">'+isNew.val()+'</option>');
        }
    });

    $('#meta_keywords_{{$key}}').val({!! json_encode($keys[$key]) !!}).trigger('change');

    @endforeach


</script>

@endpush