@extends ('layouts.app', ['panel' => true])
@section ('page_heading', $title)
@section ('content')

    <div class="col-lg-10 col-lg-offset-1">

        @if(File::exists(public_path("storage/catalog/$config->brandbook")))
            <div class="alert alert-success">
                <b>Baxmaq üçün link<br><a href="{{asset("storage/catalog/$config->brandbook")}}" target="_blank">{{$config->brandbook}}</a></b>
            </div>
        @else
            <div class="alert alert-warning">
                Kataloq yüklənməyib!
            </div>
        @endif

        @if(auth()->user()->role == 1)

        {!! Form::open(['url'=>route('catalog.store'), 'method'=>'POST', 'files' => true, 'class' => 'text-center']) !!}

        <div class="form-group">
            {!! Form::file('brandbook', ['class' => 'form-control']) !!}
        </div>

         @if(File::exists(public_path("storage/catalog/$config->brandbook")))
            @include('widgets.form-submit', ['text' => 'Yenilə', 'class' => 'success', 'icon' => 'upload'])
         @else
          @include('widgets.form-submit', ['text' => 'Yüklə', 'class' => 'success', 'icon' => 'upload'])
        @endif

        {!! Form::close() !!}

        @endif
    </div>

@endsection
