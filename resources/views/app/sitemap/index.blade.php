@extends ('layouts.app', ['panel' => true])
@section ('page_heading', $title)
@section ('content')

    @if(!is_null($config->sitemap))
        <div class="alert alert-success">
            Son yenilənmə: {{$config->sitemap}}
            <a class="alert-link pull-right" href="{{ url('sitemap.xml') }}" target="_blank">Fayla bax</a>
        </div>
    @endif


    {!! Form::open(['url'=>route('sitemap.store'), 'method'=>'POST', 'class' => 'text-center']) !!}

    @include('widgets.form-submit', ['text' => 'Yenilə', 'class' => 'success', 'icon' => 'refresh'])

    {!! Form::close() !!}

@endsection
