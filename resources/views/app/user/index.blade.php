@extends ('layouts.app', ['panel' => true, 'create' => 'user.create', 'title' => trans('locale.create'), 'modal' => true])
@section ('page_heading', $title)

@section ('content')
    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route('user.index')) }}><a href="{{route('user.index')}}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route('blocked_user')) }}><a href="{{route('blocked_user')}}">{{trans('locale.blocked')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'users', 'width' => '100%']) !!}
        </div>
    </div>
@endsection

@push('scripts')

{!! $dataTable->scripts() !!}

@endpush
