@extends(request()->ajax() ? 'layouts.modal' : 'layouts.app', ['form' => true, 'script' => true] )
@section('title', $title)
@section('class', 'col-md-6 col-md-offset-3')
@section('form', Form::open(array('route'=>['user.update', $id], 'method'=>'PATCH', 'class'=>'form-horizontal', 'id' => 'dtForm')))

@section('button', Form::button(trans('locale.edit'), ['class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'loadingButton', 'data-loading-text' => g_icon('refresh', null, 'spinning')]))

@section('content')

    {!! $fields !!}

    <div id="msgBox"></div>
@endsection

@push('scripts')
    @include('widgets.dtForm')
@endpush