@extends ('layouts.app')
@section ('page_heading', 'Profil')
@section ('content')

    <div class="col-md-6 col-md-offset-3">

        {{--@if (Auth::user()->verified == 0)--}}
            {{--@include('widgets.alert', array('class'=>'danger', 'message'=>"Please check your email and confirm your account."))--}}
        {{--@endif--}}

        {!! Form::open(['url'=>route('user.update', $info->id), 'method'=>'PATCH', 'class'=>'form-horizontal' ]) !!}

            {!! $fields !!}

            <div class="form-group col-md-12">
                {!! Form::button("Şifrəni dəyiş", ['class'=>'btn btn-sm btn-danger btn-block open-modal-dialog','data-action'=>route('edit_pass')]) !!}
            </div>

            @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}
    </div>

@endsection