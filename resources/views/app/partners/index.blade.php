@extends ('layouts.app', ['panel' => true, 'create' => 'partners.create', 'title' => trans('locale.create')])
@section ('page_heading', $title)

@section ('content')
    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route('partners.index')) }}><a href="{{route('partners.index')}}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route('trashed_partners')) }}><a href="{{route('trashed_partners')}}">{{trans('locale.trash')}}</a></li>
        <li role="presentation" {{ activeUrl(route('order_partners')) }}><a href="{{route('order_partners')}}">{{trans('locale.order')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'partners', 'width' => '100%']) !!}
        </div>
    </div>
@endsection

@push('scripts')

    {!! $dataTable->scripts() !!}

@endpush