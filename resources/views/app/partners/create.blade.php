@extends ('layouts.app', ['script' => true])
@section ('page_heading', 'Yeni tərəfdaş')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route('partners.store'), 'method'=>'POST', 'files' => true ]) !!}

        {!! $fields !!}

        @include('widgets.form-submit', ['text' => trans('locale.create'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}

    </div>

@endsection