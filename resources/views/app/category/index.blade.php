@extends ('layouts.app', ['panel' => true, 'create' => 'category.create', 'title' => trans('locale.create')])
@section ('page_heading', $title)

@section ('content')
    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route('category.index')) }}><a href="{{route('category.index')}}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route('trashed_category')) }}><a href="{{route('trashed_category')}}">{{trans('locale.trash')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'categories', 'width' => '100%']) !!}
        </div>
    </div>
@endsection

@push('scripts')

{!! $dataTable->scripts() !!}

@endpush
