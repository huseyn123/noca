@extends ('layouts.app', ['panel' => true, 'create' => 'slider.create', 'title' => trans('locale.create')])
@section ('page_heading', $title)

@section ('content')
    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route('slider.index')) }}><a href="{{route('slider.index')}}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route('trashed_slider')) }}><a href="{{route('trashed_slider')}}">{{trans('locale.trash')}}</a></li>
        <li role="presentation" {{ activeUrl(route('order_slider', 'az')) }}><a href="{{route('order_slider', 'az')}}">{{trans('locale.order')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'slider', 'width' => '100%']) !!}
        </div>
    </div>
@endsection

@push('scripts')

    {!! $dataTable->scripts() !!}

@endpush