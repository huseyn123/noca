@extends ('layouts.app', ['script' => true])
@section ('page_heading', 'Yeni söz')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route('dictionary.store'), 'method'=>'POST' ]) !!}

            {!! $fields !!}

            @include('widgets.lang-tab', ['input' => 'textarea', 'name' => 'content', 'multiRow' => true])

            @include('widgets.form-submit', ['text' => trans('locale.create'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}

    </div>

@endsection
