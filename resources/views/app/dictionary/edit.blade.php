@extends(request()->ajax() ? 'layouts.modal' : 'layouts.app', ['form' => true, 'script' => true] )
@section('title', $title)
@section('class', 'col-md-6 col-md-offset-3')
@section('form', Form::open(array('route'=>['dictionary.update', $info['id']], 'method'=>'PATCH', 'class'=>'form-horizontal', 'id' => 'dtForm')))

@section('button', Form::button(icon('save').trans('locale.save'), ['class' => 'btn btn-success', 'type' => 'submit', 'id' => 'loadingButton', 'data-loading-text' => g_icon('refresh', null, 'spinning')]))

@section('content')

    <div class="col-md-12">
        @include('widgets.lang-tab', ['input' => 'textarea', 'editor' => $info->editor, 'info' => $words, 'multiRow' => true, 'name' => 'content'])

        <div id="msgBox"></div>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('vendor/ckeditor/ckeditor.js')}}"></script>

    <script>
        @foreach(config('app.locales') as $key => $locale)
         CKEDITOR.replace('editor-{{ $key }}', { customConfig: '{{ asset('vendor/ckeditor/config-mini.js?v=1') }}' });
        @endforeach
    </script>

    @include('widgets.dtForm', ['editor' => true])
@endpush