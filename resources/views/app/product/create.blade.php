@extends ('layouts.app', ['script' => true])
@section ('page_heading', 'Yeni Məhsul')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        {!! Form::open(['url'=>route('product.store'), 'method'=>'POST', 'files' => true ]) !!}

        {!! $fields !!}

        @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}

    </div>

@endsection


@push('scripts')

    <script src="{{ asset('vendor/ckeditor/ckeditor.js')}}"></script>

    <script>
        @foreach(config('app.locales') as $key => $locale)
        CKEDITOR.replace('editor-{{ $key }}', { customConfig: '{{ asset('vendor/ckeditor/config-mini.js?v=1') }}' });
        @endforeach
    </script>


@endpush