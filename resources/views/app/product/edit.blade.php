@extends ('layouts.app', ['script' => true])
@section('page_heading', 'Düzəliş')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route('product.update', $info->id), 'method'=>'PATCH', 'files' => true]) !!}

        @include('widgets.lang-tab', ['input' => 'text', 'name' => 'title','tab_title'=>'Ad'])

        {!! $fields !!}

        @include('widgets.lang-tab', ['input' => 'textarea', 'name' => 'description','editor' => 1 ,'tab_title'=>'Məhsul üçün məlumat'])

        @include('widgets.lang-tab', ['input' => 'textarea', 'name' => 'product_sort','editor' => 1 ,'tab_title'=>'Çeşid üçün məlumat'])

        @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}
    </div>

@endsection

@push('scripts')

    <script src="{{ asset('vendor/ckeditor/ckeditor.js')}}"></script>

    <script>
        @foreach(config('app.locales') as $key => $locale)
    CKEDITOR.replace('editor-{{ $key }}', { customConfig: '{{ asset('vendor/ckeditor/config-mini.js?v=1') }}' });
        @endforeach
    </script>


@endpush