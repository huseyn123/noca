@extends ('layouts.app', ['script' => true])
@section ('page_heading', $info->title)

@section('content')

    @include('widgets.modal-confirm', ['trash' => true])

    <ul class="nav nav-tabs">
        <li class="active"><a href="#pages" data-toggle="tab"><i class="fa fa-file"></i> {{ trans('locale.page') }}</a></li>
        <li><a href="#image" data-toggle="tab"><i class="fa fa-image"></i> Şəkil</a></li>
        {{--<li><a href="#gallery" data-toggle="tab"><i class="fa fa-image"></i> Qalereya</a></li>--}}
    </ul>


    <div class="tab-content">
        <div id="pages" class="tab-pane in active"><br>
            <div class="col-lg-10 col-lg-offset-1">

                <ul class="nav nav-tabs">
                    @foreach(config('app.locales') as $key => $locale)
                        <li @if($key == $lang) class="active" @endif><a href="#{{$key}}" data-toggle="tab"> {{ $key }}</a></li>
                    @endforeach
                </ul>


                <div class="tab-content">

                    @foreach($relatedArticle as $relArticle)

                        <div id="{{ $relArticle->category->lang_id }}" class="tab-pane @if($relArticle->category->lang_id == $lang) in active @endif">

                            {!! Form::open(['url'=>route('article.update', $relArticle->id), 'method'=>'PATCH', 'class' => 'actionPage']) !!}

                            {!! $fields[$relArticle->category->lang_id] !!}

                            {!! Form::hidden('lang', $relArticle->category->lang_id) !!}

                            @include('widgets.form-submit', ['id' => $relArticle->id, 'text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save', 'del' => true, 'route' => 'article'])

                            {!! Form::close() !!}

                        </div>

                    @endforeach

                    @foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale)

                        <div id="{{ $key}}" class="tab-pane">

                            {!! Form::open(['url'=>route('article.store'), 'method'=>'POST', 'class' => 'actionPage']) !!}

                            {!! $fields[$key] !!}

                            {!! Form::hidden('relation_page', $info->id) !!}

                            {!! Form::hidden('lang', $key) !!}

                            @include('widgets.form-submit', ['id' => 0, 'text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save', 'del' => true, 'route' => 'article'])

                            {!! Form::close() !!}

                        </div>

                    @endforeach

                </div>

            </div>

        </div>

        <div id="image" class="tab-pane"><br>

            <div class="form-group col-lg-10 col-lg-offset-1">

                @if($info->image)
                    <img src="{{ asset("storage/{$info->image->filename}") }}" class="center-block img-responsive">
                @endif

                <br>

                {!! Form::open(['url'=>route('update_img_article', $info->id), 'method'=>'PATCH', 'files' => true]) !!}

                    <div class="input-group">
                        <label class="input-group-btn">
                            <span class="btn btn-primary">
                                <i class="fa fa-cloud-upload"></i> Şəkli dəyiş
                                {!! Form::file('file', ['id' => 'file', 'class' => 'form-control image', 'style' => 'display:none']) !!}
                            </span>
                        </label>
                        <input type="text" class="form-control" readonly="">
                    </div>

                    @include('widgets.form-submit', ['id' => $info->id, 'text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save', 'route' => 'article'])

                {!! Form::close() !!}
            </div>

        </div>

        <div id="gallery" class="tab-pane"><br>
            <div class="form-group col-lg-10 col-lg-offset-1">

                @if($info->galleries->count())
                    @include('app.article.gallery.edit', ['id' => $info->id, 'maxFile' => 30, 'routeName' => 'article']);
                @else
                    @include('app.article.gallery.create', ['id' => $info->id, 'maxFile' => 30, 'routeName' => 'article']);
                @endif

            </div>
        </div>
    </div>

@endsection


@push('scripts')
    <script src="{{ asset('vendor/ckeditor/ckeditor.js')}}"></script>

    <script>
        CKEDITOR.replace('ckeditor');
    </script>
@endpush