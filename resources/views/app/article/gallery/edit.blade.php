@include('app.article.gallery.dropzone')

@push('scripts')

    <script src="{{ asset("js/dropzone.js") }}"></script>

    <script>

        var photo_counter = 0;
        Dropzone.options.realDropzone = {

            uploadMultiple: false,
            parallelUploads: 3,
            maxFiles: '{{ $maxFile }}',
            maxFilesize: 10,
            previewsContainer: '#dropzonePreview',
            previewTemplate: document.querySelector('#preview-template').innerHTML,
            addRemoveLinks: true,
            dictRemoveFile: 'Sil',
            dictFileTooBig: 'Image is bigger than 10 MB',

            // The setting up of the dropzone
            init:function() {

                // Add server images
                var myDropzone = this;

                $.get('{{ route('gallery.show', $id) }}?route={{ $routeName }}', function(data) {


                    $.each(data.images, function (key, value) {

                        var file = {name: value.original, size: value.size, id: value.id};
                        myDropzone.options.addedfile.call(myDropzone, file);
                        myDropzone.options.thumbnail.call(myDropzone, file, '{!! asset('storage/thumb') !!}/' + value.server);
                        myDropzone.emit("complete", file);
                        photo_counter++;
                        $("#photoCounter").text( "(" + photo_counter + ")");
                    });
                });

                this.on("removedfile", function(file) {

                    $.ajax({
                        type: 'POST',
                        url: '{!! route('gallery.destroy', $id) !!}',
                        data: {id: file.id, _token: '{{csrf_token()}}', _method: 'DELETE'},
                        dataType: 'html',
                        success: function(data){
                            var rep = JSON.parse(data);
                            if(rep.code == 200)
                            {
                                photo_counter--;
                                $("#photoCounter").text( "(" + photo_counter + ")");
                            }

                        }
                    });

                } );
            },
            error: function(file, response) {
                if($.type(response) === "string")
                    var message = response; //dropzone sends it's own error messages in string
                else
                    var message = response.message;
                file.previewElement.classList.add("dz-error");
                _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                _results = [];
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i];
                    _results.push(node.textContent = message);
                }
                return _results;
            },
            success: function(file,response) {
                file.id = response.id;
                photo_counter++;
                $("#photoCounter").text( "(" + photo_counter + ")");
            }
        }


    </script>
@endpush