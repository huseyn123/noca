{!! Form::open(['url'=>route('gallery.store'), 'method'=>'POST', 'class' => 'dropzone', 'files'=>true, 'id'=>'real-dropzone']) !!}

<div class="dz-message">

</div>

<div class="fallback">
    <input name="file" type="file" multiple />
</div>

<div class="dropzone-previews" id="dropzonePreview"></div>

<h4 style="text-align: center;color:#428bca;">Şəkilləri bura sürüşdürün  <span class="glyphicon glyphicon-hand-down"></span></h4>

@include('widgets.dropzone-template')

{!! Form::hidden('id', $info->id) !!}

{!! Form::hidden('route', $routeName) !!}

{!! Form::hidden('csrf-token', csrf_token(), ['id' => 'csrf-token']) !!}

{!! Form::close() !!}