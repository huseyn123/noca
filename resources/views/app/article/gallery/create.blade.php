@include('app.article.gallery.dropzone')

@push('scripts')
<script src="{{ asset("js/dropzone.js") }}"></script>

@include('widgets.dropzone', ['maxFile' => 30, 'id' => $id ])
@endpush