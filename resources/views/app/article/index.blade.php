@extends ('layouts.app', ['panel' => true, 'create' => 'article.create', 'title' => trans('locale.create')])
@section ('page_heading', $title)
@section ('content')

    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route('article.index')) }}><a href="{{route('article.index')}}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route('trashed_article')) }}><a href="{{route('trashed_article')}}">{{trans('locale.trash')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>

            <div class="dataTables_wrapper">
                <div class="pull-left hidden-xs col-md-3" id="dataTables_length_box">
                </div>

                <div class="pull-right">
                    @include('widgets.article-search')
                </div>
            </div>

            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'articles', 'width' => '100%']) !!}
        </div>
    </div>

@endsection

@push('scripts')

    {!! $dataTable->scripts() !!}

    <script>
        $('#search-form').on('submit', function(e) {
            $('#articles').DataTable().draw(false);
            e.preventDefault();
        });

        $("#articles").on('preXhr.dt', function(e, settings, data) {
            data.title = $("#title").val();
            data.categoryId = $("#category_id").val();
        });


    </script>
@endpush
