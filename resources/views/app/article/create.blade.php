@extends ('layouts.app', ['script' => true])
@section ('page_heading', isset($title) ? $title : 'Yeni xəbər')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route('article.store'), 'method'=>'POST', 'files'=>true ]) !!}

        {!! $fields !!}

        @include('widgets.form-submit', ['text' => trans('locale.next'), 'class' => 'success', 'icon' => 'arrow-right', 'return' => true])

        {!! Form::close() !!}

    </div>

@endsection

@push('scripts')
    <script src="{{ asset('vendor/ckeditor/ckeditor.js')}}"></script>

    <script>
        CKEDITOR.replace('ck-editor', { customConfig: '{{ asset('vendor/ckeditor/config.js?v=6') }}' });
    </script>
@endpush