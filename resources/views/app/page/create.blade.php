@extends ('layouts.app', ['script' => true])
@section ('page_heading', 'Yeni səhifə')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route("$route.store"), 'method'=>'POST' , 'files' => true]) !!}

            {!! $fields !!}

            @include('widgets.form-submit', ['text' => trans('locale.next'), 'class' => 'success', 'icon' => 'arrow-right', 'return' => true])

        {!! Form::close() !!}
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('vendor/ckeditor/ckeditor.js')}}"></script>

    <script>

        $('#meta_keywords_').select2({
            tags: true,
            maximumSelectionLength: 10

        }).on("change", function(e) {
            var isNew = $(this).find('[data-select2-tag="true"]');
            if(isNew.length){
                isNew.replaceWith('<option selected value="'+isNew.val()+'">'+isNew.val()+'</option>');
            }
        });

    </script>
@endpush