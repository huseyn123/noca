@extends ('layouts.app', ['panel' => true, 'create' => 'page.create', 'title' => trans('locale.create')])
@section ('page_heading', $title)

@section ('content')
    @include('widgets.modal-confirm')

    <div class="col-md-12">
        <div class="alert alert-info">
            <b>QEYD: Ana səhifənin slug ünvanı bütün dillərdə "index" kimi göstərilməlidir.</b>
        </div>
    </div>

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route('page.index')) }}><a href="{{route('page.index')}}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route('trashed_page')) }}><a href="{{route('trashed_page')}}">{{trans('locale.trash')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'pages', 'width' => '100%']) !!}
        </div>
    </div>
@endsection

@push('scripts')

{!! $dataTable->scripts() !!}

@endpush
