@extends ('layouts.app')
@section ('page_heading', 'Ardıcıllıq')

@section ('content')

    <div class="row">
        <div class="col-md-12">

            @if($lang != false)
                <ul class="nav nav-tabs">
                    @foreach(config('app.locales') as $key => $lg)
                        <li role="presentation" @if($key == $lang) class="active" @endif>
                            <a href="{{ route($route) }}?lang={{ $key }}">{{ $key }}</a>
                        </li>
                    @endforeach
                </ul>
            @endif

            <div class="tab-content">
                <div class="tab-pane active"><br>
                    <div class="well">
                        <div class="dd" id="nestable">
                            {!! $html !!}
                        </div>
                        <p id="success-indicator" style="display:none; margin-right: 10px;">
                            <span class="glyphicon glyphicon-ok"></span> Dəyişikliklər qeydə alındı.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('css')
<link rel="stylesheet" href="{{asset('vendor/nestable/nestable.css')}}">
@endpush

@push('scripts')
<script src="{{asset('vendor/nestable/nestable.js')}}"></script>

<script type="text/javascript">


    $('.dd').nestable({
        maxDepth: {{$depth}},
        dropCallback: function(details) {

            var order = new Array();
            $("li[data-id='"+details.destId +"']").find('ol:first').children().each(function(index,elem) {
                order[index] = $(elem).attr('data-id');
                //console.log(details);
            });
            if (order.length === 0){
                var rootOrder = new Array();
                $("#nestable > ol > li").each(function(index,elem) {
                    rootOrder[index] = $(elem).attr('data-id');
                });
            }
            $.post( '{{route("post_$route")}}',
                {
                    source : details.sourceId,
                    destination: details.destId,
                    order:JSON.stringify(order),
                    rootOrder:JSON.stringify(rootOrder)
                },
                function(data) {
                    // console.log('data '+data);
                })
                .done(function() {
                    $( "#success-indicator" ).fadeIn(100).delay(1000).fadeOut();
                })
                .fail(function() {  })
                .always(function() {  });
        }
    });

    $("button[data-action='collapse']").remove();

</script>

@endpush