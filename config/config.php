<?php

return [
    "form_category" => [1 => "Kateqoriyalar", "Ölkə"],
    "model" => ['article' => 'App\Models\Article', 'page' => 'App\Models\Page', 'slider' => 'App\Models\Slider', 'partners' => 'App\Models\Partner',],
    "slider-type" => [1 => "Slider", "Kompaniya"],
    "article-status" => [0 => "Qaralama", "Dərc olunub"],
    "label" => ["0" => "danger", "1" => "success"],
    "menu-target" => ["1" => "Self", "0" => "Blank"],
    "menu-visibility" => [0 => "Hidden", "Header and Footer", "Header", "Footer"],
    "menu-visibility-boolean" => [0 => "Gizli", "Aktiv"],
    "menu-visibility-label" => [0 => "danger", "success", "warning", "warning", "info"],
    "clients" => [1 => 'Tərəfdaşlar', 'Media tərəfdaşları'],
    "lang" => ["az" => "Azərbaycanca", "en" => "İngiliscə", "ru" => "Rusca"],
    "lang-dictionary" => ["az" => "az", "en" => "en", "ru" => "ru"],
    "role" => [1 => "Administrator", "Editor"],
    "blog_page" => [0 => "No", "Yes"],
    "social-network" => ['facebook' => 'Facebook', 'instagram' => 'Instagram', 'youtube' => 'Youtube', 'twitter' => 'Twitter', 'linkeidn' => 'Linkedin'],
    "subscriber-label" => ["0" => "warning", "1" => "success"],
    "subscriber-status" => ["0" => "pending", "1" => "active"],
    "yes-no" => [0 => "minus", "plus"],
    'prefix' => [12 => 12, 50 => 50, 51 => 51, 55 => 55, 70 => 70, 77 => 77],
    'slug_replacement' => ['az' => 'azerbaijani', 'en' => 'azerbaijani', 'ru' => 'russian', 'tr' => 'turkish'],
    "sex" => [1 => "Kişi", 2 => "Qadın"],
    "services-icon" => ['' => "---",'icon-texnik' => 'Texniki diaqnostika', 'icon-sazlama' => 'Quraşdırma və Sazlama','icon-it' => 'İnformasiya Texnologiya və Avtomatika', 'icon-servis' => 'Texniki Xidmət'],

    "template" => [1 => 'Xəbərlər', 4 => 'Əlaqə', 6 => 'Static'],

    "template2" => [
        0 => "Static",
        7 => "Xidmətlər",
        8 => "Layihələr",
        10 => "Blok",
        13 => 'Məhsullar',
    ],
];