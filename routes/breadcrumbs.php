<?php
use App\Logic\Breadcrumb;


Breadcrumbs::register('web', function($breadcrumbs, $page, $homePageTitle, $article)
{
    $breadcrumbs->push($homePageTitle, route('home'));



    $breadcrumb = new Breadcrumb;


    $bc = $breadcrumb->get($page);

    foreach ( $bc as $key => $item) {

        $breadcrumbs->push(normalTitle($item['name']), route("showPage", $item['slug']));
    }

    if($article == true) {
        if($article->title){
            $breadcrumbs->push(normalTitle($article->title));
        }else{
            $breadcrumbs->push(normalTitle($article->name));
        }
    }

});

