<?php

Route::pattern('id', '[0-9]+');
Route::pattern('type', '[0-9]+');

Auth::routes();


if(!request()->is('manager/*') && !request()->is('manager') && !request()->is('_debugbar/*'))
{

    Route::group(
        [
            'prefix' => LaravelLocalization::setLocale(),
            'middleware' => [ 'web', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
        ],
        function()
        {
            Route::get('/',                     ['as' => 'home', 'uses' => 'Web\MainController@index']);

            Route::get('/search',               ['as' => 'search', 'uses' => 'Web\MainController@search']);

            Route::get('/condition/{slug}',     ['as' => 'condition', 'uses' => 'PdfController@show']);

            Route::get('{page}',                ['as' => 'showPage', 'uses' => 'Web\MainController@showPage']);

            Route::get('{page}/{article}',      ['as' => 'showArticle', 'uses' => 'Web\MainController@showArticle']);

            Route::get('/get/{id}/{name}',      ['as' => 'download', 'uses' => 'GalleryController@getDownload']);

            Route::post('/contact',             ['as' => 'contact', 'uses' => 'ContactController@contact']);



        });
}
