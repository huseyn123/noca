<?php

Route::pattern('id', '[0-9]+');
Route::pattern('type', '[0-9]+');


    Route::get('/', function (){

        return redirect()->route('dashboard');
    });

    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

    Route::resource('catalog', 'BrandController', ['only' => ['index', 'create', 'store', 'destroy']]);

    Route::resource('slider', 'SliderController', ['except' => ['show']]);

    Route::group(['prefix'=>'slider'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_slider', 'uses' => 'SliderController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_slider', 'uses' => 'SliderController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_slider', 'uses' => 'SliderController@restore']);
        Route::get('order',             ['as' => 'order_slider', 'uses' => 'SliderController@order']);
        Route::post('postOrder',        ['as' => 'post_order_slider', 'uses' => 'SliderController@postOrder']);
    });

    Route::resource('page', 'PageController', ['except' => ['show']]);

    Route::group(['prefix'=>'page'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_page', 'uses' => 'PageController@trashed']);
        Route::patch('{id}/updateImg',  ['as' => 'update_page_image', 'uses' => 'PageController@updateImage']);
        Route::patch('{id}/trash',      ['as' => 'trash_page', 'uses' => 'PageController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_page', 'uses' => 'PageController@restore']);
        Route::get('order',             ['as' => 'order_page', 'uses' => 'PageController@order']);
        Route::post('postOrder',        ['as' => 'post_order_page', 'uses' => 'PageController@postOrder']);
        Route::delete('{id}/image',     ['as' => 'del_image', 'uses' => 'PageController@delImage']);
        Route::delete('{id}/MainImage', ['as' => 'del_main_image', 'uses' => 'PageController@delMainImage']);
    });


    Route::resource('category', 'CategoryController', ['except' => ['show']]);

    Route::group(['prefix'=>'category'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_category', 'uses' => 'CategoryController@trashed']);
        Route::get('order/{lang}',      ['as' => 'order_category', 'uses' => 'PageController@orderCategory']);
        Route::patch('{id}/trash',      ['as' => 'trash_category', 'uses' => 'CategoryController@trash']);
        Route::patch('{id}/updateImg',  ['as' => 'update_category_image', 'uses' => 'CategoryController@updateImage']);
    });

    Route::resource('product', 'ProductController', ['except' => ['show']]);

    Route::group(['prefix'=>'product'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_product', 'uses' => 'ProductController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_product', 'uses' => 'ProductController@trash']);
        Route::patch('{id}/updateImg',  ['as' => 'update_product_image', 'uses' => 'ProductController@updateImage']);
    });

    Route::resource('product_info', 'ProductInfoController', ['except' => ['show']]);

    Route::group(['prefix'=>'product_info'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_product_info', 'uses' => 'ProductInfoController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_product_info', 'uses' => 'ProductInfoController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_product_info', 'uses' => 'ProductInfoController@restore']);
        Route::delete('destroy',        ['as' => 'destroy_product_info',  'uses' => 'ProductInfoController@destroy']);
    });






    Route::group(['prefix'=>'{id}/gallery'], function()
    {
        Route::get('/',            ['as' => 'gallery_index',    'uses' => 'GalleryController@index']);
        Route::post('store',       ['as' => 'gallery_store',    'uses' => 'GalleryController@store']);
        Route::get('getImages',    ['as' => 'gallery_get',      'uses' => 'GalleryController@getImages']);
        Route::get('image',        ['as' => 'gallery_image',    'uses' => 'GalleryController@image']);
        Route::delete('destroy',   ['as' => 'gallery_destroy',  'uses' => 'GalleryController@destroy']);
    });


    Route::resource('gallery', 'GalleryController', ['except' => ['index', 'update']]);

    Route::resource('article', 'ArticleController', ['except' => ['show']]);

    Route::group(['prefix'=>'article'], function()
    {
        Route::get('category',          ['as' => 'cat_article', 'uses' => 'ArticleController@categories']);
        Route::get('featured',          ['as' => 'featured_article', 'uses' => 'ArticleController@featured']);
        Route::get('trashed',           ['as' => 'trashed_article', 'uses' => 'ArticleController@trashed']);
        Route::patch('{id}/updateImg',  ['as' => 'update_img_article', 'uses' => 'ArticleController@updateImage']);
        Route::patch('{id}/trash',      ['as' => 'trash_article', 'uses' => 'ArticleController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_article', 'uses' => 'ArticleController@restore']);
        Route::post('{id}/{lang}/copy', ['as' => 'copy_article', 'uses' => 'ArticleController@copy']);
    });

    Route::resource('dictionary', 'DictionaryController',['only' => ['index', 'edit', 'update', 'create', 'store']]);


    Route::resource('partners', 'PartnerController', ['except' => ['show']]);

    Route::group(['prefix'=>'partners'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_partners', 'uses' => 'PartnerController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_partners', 'uses' => 'PartnerController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_partners', 'uses' => 'PartnerController@restore']);
        Route::get('order',             ['as' => 'order_partners', 'uses' => 'PartnerController@order']);
        Route::post('postOrder',        ['as' => 'post_order_partners', 'uses' => 'PartnerController@postOrder']);
    });


    Route::resource('user', 'UserController',['except' => ['show']]);

    Route::group(['prefix'=>'user'], function()
    {
        Route::get('show',              ['as' => 'show_user', 'uses' => 'UserController@show']); //+
        Route::get('edit-password',     ['as' => 'edit_pass', 'uses' => 'UserController@editPassword']); //+
        Route::get('blocked',           ['as' => 'blocked_user', 'uses' => 'UserController@blocked']);  //+
        Route::post('updatePass',       ['as' => 'update_user_pass', 'uses' => 'UserController@updatePassword']);  //+
        Route::patch('{id}/trash',      ['as' => 'trash_user', 'uses' => 'UserController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_user', 'uses' => 'UserController@restore']);
    });



    Route::group(['prefix'=>'settings'], function()
    {
        Route::resource('config', 'ConfigController',['only' => ['edit', 'update', 'show']]);

        Route::resource('main_video', 'MainVideoController',['only' => ['edit', 'update', 'show']]);

        Route::resource('social', 'SocialController',['except' => ['show']]);

        Route::resource('sitemap', 'SiteMapController', ['only' => ['index', 'store']]);

        Route::resource('smtp', 'SmtpController', ['except' => ['show', 'create', 'store']]);

    });






