﻿CKEDITOR.editorConfig = function( config ) {

    config.toolbar = 'miniVersion';
// The toolbar groups arrangement, optimized for two toolbar rows.
    /*config.toolbarGroups = [
        { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
        { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
        { name: 'links' },
        { name: 'insert' },
        { name: 'forms' },
        { name: 'tools' },
        { name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'others' },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
        { name: 'styles' },
        { name: 'colors' },
        { name: 'about' },
        { name: 'Blockquote', attributes: { "class": "style2" } }
    ]; */

// Remove some buttons provided by the standard plugins, which are
// not needed in the Standard(s) toolbar.
    //config.removeButtons = 'Underline,Subscript,Superscript';

// Set the most common block elements.
    //config.format_tags = 'p;h1;h2;h3;pre';

// Simplify the dialog windows.
    //config.removeDialogTabs = 'image:advanced;link:advanced';

    //config.extraPlugins = 'justify,oembed,colorbutton';
    config.removePlugins = 'about,font,forms';

    config.filebrowserBrowseUrl = '/manager/filemanager/ckeditor';
    config.filebrowserWindowWidth = "70%";
    config.filebrowserWindowHeight = "50%";
    //config.stylesSet = 'my_styles';
    config.allowedContent = true;
    config.language = 'az';
};